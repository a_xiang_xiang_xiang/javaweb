package com.xiang.lesson01;
//线程开启不一定立即执行，由cpu 高度执行

//创建方式一，继承Thread类，重写run方法，调用start开启线程
public class TestThread1  extends  Thread{
    @Override
    public void run() {
//        run 方法 线程体
        for (int i = 0; i < 5; i++) {
            System.out.println("线程-------------- "+i);
        }
    }

    public static void main(String[] args) {
//        创建一个线程对象
        TestThread1 thread1 = new TestThread1();
//        调用start（） 方法开启线程
        thread1.run();
        thread1.start();
//main 线程 ；主线程
        for (int i = 0; i < 10; i++) {
            System.out.println("多线程---------"+i);
        }
    }
}

//交替执行；