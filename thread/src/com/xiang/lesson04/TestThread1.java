package com.xiang.lesson04;
//多个线程操作同一个对象

//买火车票
public class TestThread1 implements  Runnable{

//    票数量
    private  int tick = 10;

    @Override
    public void run() {
while (true){
    if (tick<=0){
        break;
    }
//    延时
    try {
        Thread.sleep(200);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
    System.out.println(Thread.currentThread().getName()+"-->拿到了第几"+tick--+"票");
}
    }

    public static void main(String[] args) {
        TestThread1 thread1 = new TestThread1();

        new  Thread(thread1,"小小").start();
        new  Thread(thread1,"小二").start();
        new  Thread(thread1,"小九").start();
        new  Thread(thread1,"小四").start();
    }
}
//多个线程操作同一个资源的情况下，线程不安全，数据紊乱

/**
 * 小九-->拿到了第几10票
 * 小四-->拿到了第几10票
 * 小二-->拿到了第几9票
 * 小小-->拿到了第几8票
 * 小四-->拿到了第几7票
 * 小二-->拿到了第几6票
 * 小小-->拿到了第几7票
 * 小九-->拿到了第几7票
 * 小二-->拿到了第几5票
 * 小九-->拿到了第几3票
 * 小四-->拿到了第几4票
 * 小小-->拿到了第几2票
 * 小四-->拿到了第几1票
 * 小九-->拿到了第几1票
 * 小小-->拿到了第几0票
 * 小二-->拿到了第几-1票
 */