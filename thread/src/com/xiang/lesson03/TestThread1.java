package com.xiang.lesson03;

//创建线程方式2， 实现Runnable 接口，重写run 方法，
public class TestThread1 implements  Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("循环---+"+i);
        }
    }
//这是一个交替执行的过程
    public static void main(String[] args) {
//        创建Runnable 接口 的实现类
        TestThread1 thread1 = new TestThread1();
        //        创建线程对象，通过线程对象来开启我们的线程，代理；
//        Thread thread = new Thread(thread1);
//        thread.start();

        new  Thread(thread1).start();
        for (int i = 0; i < 10; i++) {
            System.out.println("循环第"+i+"次");
        }
    }

}
