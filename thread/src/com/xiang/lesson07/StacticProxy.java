package com.xiang.lesson07;

public class StacticProxy {
    public static void main(String[] args) {
//        Wedding wedding = new Wedding();
//        wedding.Wedding(new You());
//        wedding.HappyMarry();

        You you = new You();
        Wedding wedding = new Wedding();
        wedding.Wedding(you);
        wedding.HappyMarry();


    }
}

interface Marry {
    void HappyMarry();
}

//真实角色
class You implements Marry {
    @Override
    public void HappyMarry() {
        System.out.println("marry...........");
    }
}

//代理角色
class Wedding implements Marry {
    private Marry target;

    public Wedding() {

    }

    public void Wedding(Marry target) {
            this.target = target;
    }

    @Override
    public void HappyMarry() {
        before();
        this.target.HappyMarry();
        after();
    }

    private void before() {
        System.out.println("before...........");
    }

    private void after() {
        System.out.println("after..............");
    }
}