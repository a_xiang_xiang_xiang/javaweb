create
database webapp1901 charset utf8mb4;

create
user 'webapp1901'@'localhost' identified by 'webapp1901';
grant all
on webapp1901.* to 'webapp1901'@'localhost';



-- # mysql -uwebapp1901 -pwebapp1901 webapp1901 -h127.0.0.1


create table teacher
(
    id    int auto_increment
        primary key,
    tid   varchar(20) not null,
    tname varchar(20) not null
);


create table student
(
    id       int AUTO_INCREMENT not null PRIMARY KEY,
    #编号
             studentId varchar(8) not null,
    #学号
             pwd varchar(100),
    #密码
             name varchar(20),
    #姓名
             gender int not null,--性别（1：男，0：女）
    birthday date not null,
    #出生日期
             mobile varchar(11) not null,
    #电话
             address varchar(100) #地址
);
create
unique index student_index1 on student(studentId);