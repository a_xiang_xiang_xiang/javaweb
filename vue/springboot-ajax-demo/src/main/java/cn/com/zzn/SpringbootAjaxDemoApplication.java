package cn.com.zzn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootAjaxDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootAjaxDemoApplication.class, args);
    }

}
