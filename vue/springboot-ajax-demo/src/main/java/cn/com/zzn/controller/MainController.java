package cn.com.zzn.controller;

import cn.com.zzn.model.Teacher;
import cn.com.zzn.service.TeacherMapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@RequestMapping("/")
public class MainController {
    @Autowired
    TeacherMapperService teacherMapperService;

    //获取
    @GetMapping("/teacher")
    public List<Teacher> list(){
        List<Teacher> all = teacherMapperService.findAll();
        return  all;
    }

    //获取
    @GetMapping("/teacher/{id}")
    public Teacher geTeacher(@PathVariable("id") Integer id){
        Teacher byId = teacherMapperService.findById(id);
        return byId;
    }
//    删除
//    @DeleteMapping("/teacher/{id}")
}
