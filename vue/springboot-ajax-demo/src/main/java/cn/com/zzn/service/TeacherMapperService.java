package cn.com.zzn.service;

import cn.com.zzn.model.Teacher;
import org.springframework.stereotype.Service;

import java.util.List;

//持久层主要就是映射接口的方法
public interface TeacherMapperService {
        List<Teacher> findAll();
        Integer delete(int id);
        Integer update(Teacher teacher);
        Integer add(Teacher teacher);
        Teacher findById(int id);

}
