package cn.com.zzn.service;

import cn.com.zzn.mapper.TeacherMapper;
import cn.com.zzn.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//业务操作接口
@Service
public class TeacherMapperServiceImpl implements TeacherMapperService{
//    注入接口的方法
    @Autowired
    private TeacherMapper teacherMapper;
    @Override
    public List<Teacher> findAll() {
        return teacherMapper.findAll();
    }

    @Override
    public Integer delete(int id) {
       return teacherMapper.delete(id);
    }

    @Override
    public Integer update(Teacher teacher) {
        return teacherMapper.update(teacher);
    }

    @Override
    public Integer add(Teacher teacher) {
        return  teacherMapper.add(teacher);
    }

    @Override
    public Teacher findById(int id) {
        return findById(id);
    }
}
