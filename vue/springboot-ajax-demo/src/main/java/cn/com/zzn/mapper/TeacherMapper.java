package cn.com.zzn.mapper;

import cn.com.zzn.model.Teacher;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface TeacherMapper {
    List<Teacher> findAll();
    Integer delete(int id);
    Integer update(Teacher teacher);
    Integer add(Teacher teacher);
    Teacher findById(int id);

}
