package com.xiang.chat;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;

//发送方
public class UdpSenderDemo01 {
    public static void main(String[] args) throws Exception {
        DatagramSocket socket = new DatagramSocket(888);
//准备数据 ，从控制台读取；System.in
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        读取
while (true){
    String data = reader.readLine();
    byte[] datas = data.getBytes(StandardCharsets.UTF_8);


    DatagramPacket packet = new DatagramPacket(datas, 0, datas.length, new InetSocketAddress("localhost", 666));
    socket.send(packet);

    if (data.equals("bye")){
        break;
    }
}
        socket.close();
    }
}
