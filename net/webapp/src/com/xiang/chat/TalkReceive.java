package com.xiang.chat;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class TalkReceive implements Runnable {
    DatagramSocket socket = null;
    private int port;
    private  String mesFrom;

    public TalkReceive(int port,String mesFrom) {
        this.port = port;
        this.mesFrom=mesFrom;
        try {
            socket = new DatagramSocket(port);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {

        while (true) {
            try {
//        准备接收包裹
                byte[] container = new byte[1024];

                DatagramPacket packet = new DatagramPacket(container, 0, container.length);

                socket.receive(packet);//阻塞式接收包裹

//            断开连接 bye
                byte[] data = packet.getData();
                String resData = new String(data, 0, packet.getLength());
                System.out.println(mesFrom+":"+resData);

                if (resData.equals("bye")) {
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            socket.close();
        }
    }
}
