package com.xiang.chat;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

//接收方
public class UdpReceiveDemo01 {
    public static void main(String[] args) throws Exception {
        DatagramSocket socket = new DatagramSocket(666);
        while (true) {
//        准备接收包裹
            byte[] container = new byte[1024];

            DatagramPacket packet = new DatagramPacket(container, 0, container.length);

            socket.receive(packet);//阻塞式接收包裹

//            断开连接 bye
            byte[] data = packet.getData();
            String resData = new String(data, 0, packet.getLength());
            System.out.println(resData);

            if (resData.equals("bye")) {
                break;
            }
            socket.close();
        }
    }
}
