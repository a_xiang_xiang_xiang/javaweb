package com.xiang.chat;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;

public class TalkSend  implements  Runnable{
    DatagramSocket socket = null;
    BufferedReader reader = null;

    private  int fromPort;
    private  String toIP;
    private  int toPort;

    public TalkSend(int fromPort, String toIP, int toPort) {
        this.fromPort = fromPort;
        this.toIP = toIP;
        this.toPort = toPort;

        try {
            DatagramSocket socket = new DatagramSocket(fromPort);
            //准备数据 ，从控制台读取；System.in
            reader = new BufferedReader(new InputStreamReader(System.in));
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {

//        读取
        while (true){
            try {
                String data = reader.readLine();
                byte[] datas = data.getBytes(StandardCharsets.UTF_8);


                DatagramPacket packet = new DatagramPacket(datas, 0, datas.length, new InetSocketAddress(this.toIP, this.toPort));
                socket.send(packet);

                if (data.equals("bye")){
                    break;
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }
        socket.close();
    }
}
