package com.xiang.lesson02;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

//客户端
public class TcpClientDemo01 {
    public static void main(String[] args) {
        Socket socket = null;
        OutputStream outputStream = null;

//        1,要知道服务器的地址
//        连这个地址 localhost 999
        try {
            InetAddress serverIP = InetAddress.getByName("127.0.0.1");

//          2  端口号
            int port = 999;

//            3,创建一个 socket 连接
            socket = new Socket(serverIP, port);

//            4,发送消息 io 流
//            输出 流
            outputStream = socket.getOutputStream();
//            写
            outputStream.write("你好，欢迎学习到了网络编程".getBytes(StandardCharsets.UTF_8));


        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
