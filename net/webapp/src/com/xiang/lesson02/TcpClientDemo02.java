package com.xiang.lesson02;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
//客户端读取文件；
public class TcpClientDemo02 {
    public static void main(String[] args) throws Exception {
        //1,  创建一个socket 连接
        Socket socket = new Socket(InetAddress.getByName("127.0.0.1"), 9000);
//        2,创建一个输出注
        OutputStream os = socket.getOutputStream();
//        3,文件流 读取文件
        FileInputStream fis = new FileInputStream(new File("javaee.txt"));
//        4,写出文件
        byte[] buffer = new byte[1024];
        int len;
        while ((len = fis.read(buffer)) != -1) {
            os.write(buffer, 0, len);
        }

//        通知服务器，我已经结束 了
        socket.shutdownOutput(); //我已传输完了

//        确定服务器接收完毕，才断开连接
        InputStream inputStream = socket.getInputStream();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        byte[] buffer2 = new byte[1024];
        int len2;
        while ((len2=inputStream.read(buffer2))!=-1){
            baos.write(buffer2,0,len2);
        }

        System.out.println(baos.toString());

//       5,关闭资源
        baos.close();
        inputStream.close();

        fis.close();
        os.close();
        socket.close();
    }


}
