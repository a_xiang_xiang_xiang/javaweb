package com.xiang.lesson02;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

//服务端接收文件
public class TcpServerDemo02 {
    public static void main(String[] args) throws Exception {
//        1,创建一个端口  创建一个服务
        ServerSocket socket = new ServerSocket(9000);
//    2,监听客户端的连接
        Socket accept = socket.accept();
//        3,获取输入流
        InputStream is = accept.getInputStream();

//        4，文件输出
        FileOutputStream fos = new FileOutputStream(new File("res"));

        byte[] buffer = new byte[1024];
        int len;
        while ((len=is.read(buffer))!=-1){
            fos.write(buffer,0,len);
        }

//        通知客户端 ，我接收完成
        OutputStream os = accept.getOutputStream();
        os.write("我这边接收完毕了，你可以断开了".getBytes(StandardCharsets.UTF_8));

//        关闭资源
        fos.close();
        is.close();
        accept.close();
        socket.close();
    }
}
