package com.xiang.lesson01;

import java.net.InetSocketAddress;

public class TestInetSockAddress {
    public static void main(String[] args) {
        InetSocketAddress inetSocketAddress = new InetSocketAddress("127.0.0.1", 8080);
        System.out.println(inetSocketAddress);//127.0.0.1:8080
        InetSocketAddress inetSocketAddress2 = new InetSocketAddress("localhost", 8080);
        System.out.println(inetSocketAddress2);//localhost/127.0.0.1:8080

        System.out.println(inetSocketAddress.getAddress());///127.0.0.1
        System.out.println(inetSocketAddress.getHostName());//127.0.0.1
        System.out.println(inetSocketAddress.getPort());//8080
        System.out.println(inetSocketAddress.getHostString());//127.0.0.1
        String s = inetSocketAddress.toString();
        System.out.println(s);//127.0.0.1/127.0.0.1:8080
    }
}
