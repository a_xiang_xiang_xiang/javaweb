package com.xiang.lesson01;

import java.net.InetAddress;
import java.net.UnknownHostException;
//测试ip
public class TestInetAddress {
    public static void main(String[] args) {
        try {
            InetAddress[] inetAddresses = InetAddress.getAllByName("127.0.0.1");
            System.out.println(inetAddresses); //[Ljava.net.InetAddress;@1b6d3586

//            查询本机地址
            InetAddress name1 = InetAddress.getByName("127.0.0.1");
            System.out.println(name1);//127.0.0.1
            InetAddress name = InetAddress.getByName("localhost");
            System.out.println(name);//localhost/127.0.0.1/127.0.0.1
            InetAddress name0 = InetAddress.getLocalHost();
            System.out.println(name0);//DESKTOP-6K7C1EJ/192.168.42.22


//            查询网站ip地址
            InetAddress name2 = InetAddress.getByName("www.baidu.com");
            System.out.println(name2);//www.baidu.com/103.235.46.39

//            常用方法
            System.out.println(name2.getAddress());//[B@4554617c
            System.out.println(name2.getHostAddress());//103.235.46.39 --------规范的名字
            System.out.println(name2.getCanonicalHostName());//103.235.46.39 ---------ip
            System.out.println(name2.getHostName());//www.baidu.com ---------- 域名、或者自己电脑名



        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
