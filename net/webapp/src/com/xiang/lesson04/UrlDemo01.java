package com.xiang.lesson04;

import java.net.MalformedURLException;
import java.net.URL;

public class UrlDemo01 {
    public static void main(String[] args) throws Exception {
        URL url = new URL("http://loclhost:8080/xiang/index.jsp?username=xiang&password=123456");
        System.out.println(url.getProtocol());//http
        System.out.println(url.getHost());//loclhost
        System.out.println(url.getPath());///xiang/index.jsp
        System.out.println(url.getQuery());//username=xiang&password=123456
        System.out.println(url.getPort());//8080
        System.out.println(url.getFile());///xiang/index.jsp?username=xiang&password=123456
    }
}
