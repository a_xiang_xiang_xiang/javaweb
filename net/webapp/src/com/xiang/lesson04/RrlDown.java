package com.xiang.lesson04;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

//url 下载网络资源 ；
public class RrlDown {
    public static void main(String[] args) throws Exception {
//        1,下载地址
        URL url = new URL("http://localhost:8080/xiang/index.jsp");
//    2，连接到这个资源 ，http
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        InputStream stream = connection.getInputStream();

        FileOutputStream fos = new FileOutputStream("test.txt");

        byte[] buffer = new byte[1024];
        int len;
        while ((len=stream.read(buffer))!=-1){
            fos.write(buffer,0, len); //写出这外数据

        }
        fos.close();
        stream.close();
        connection.disconnect(); //断开连接
    }
}
