package com.xiang.lesson03;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class UdpServerDemo01 {
    public static void main(String[] args) throws Exception {
//        开放端口 ；
        DatagramSocket socket = new DatagramSocket(9999);
//        接收数据 包
        byte[] buffer = new byte[1024];
//        接收
        DatagramPacket packet = new DatagramPacket(buffer, 0, buffer.length);
//        阻塞/接收
        socket.receive(packet);

//        输出
        System.out.println(packet.getSocketAddress());
        System.out.println(packet.getAddress().getCanonicalHostName());
        System.out.println(new String(packet.getData(),0,packet.getLength()));

//        关闭连接
        socket.close();

/*
先启动服务器，
要启动客户端
 */

    }
}
