# create database 数据库名 charset utf8mb4;

create  table  student(
    id int AUTO_INCREMENT not null  PRIMARY KEY,#编号
    studentId varchar(8) not null ,#学号
    pwd varchar(100),#密码
    name  varchar(20),#姓名
    gender int not null ,--性别（1：男，0：女）
    birthday date not null ,#出生日期
    mobile varchar(11) not null ,#电话
    address varchar(100)#地址
);

INSERT INTO `student`(`studentId`,`pwd`,`name`,`gender`,`birthday`,`mobile`,`address`)
VALUES ( '19301043', '123456', '周圳南', 1, '2000-01-10', '13419171453', '四川泸州'),

**StudentDao**(dao 层)
@Repository //仓库；
public interface StudentDao extends CrudRepository<StudentEntity,Integer> {

}

**StudentEntity**类名（model 层）
@Entity
@Table(name = "student", schema = "today_the_campus", catalog = "")
public class StudentEntity {
    private int id;
    private String studentId;
    private String pwd;
    private String name;
    private int gender;
    private Date birthday;
    private String mobile;
    private String address;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "studentId", nullable = false, length = 8)
    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    @Basic
    @Column(name = "pwd", nullable = true, length = 100)
    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 20)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "gender", nullable = false)
    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    @Basic
    @Column(name = "birthday", nullable = false)
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Basic
    @Column(name = "mobile", nullable = false, length = 11)
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Basic
    @Column(name = "address", nullable = true, length = 100)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentEntity that = (StudentEntity) o;
        return id == that.id && gender == that.gender && Objects.equals(studentId, that.studentId) && Objects.equals(pwd, that.pwd) && Objects.equals(name, that.name) && Objects.equals(birthday, that.birthday) && Objects.equals(mobile, that.mobile) && Objects.equals(address, that.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, studentId, pwd, name, gender, birthday, mobile, address);
    }


    @Override
    public String toString() {
        return "StudentEntity{" +
                "id=" + id +
                ", studentId='" + studentId + '\'' +
                ", pwd='" + pwd + '\'' +
                ", name='" + name + '\'' +
                ", gender=" + gender +
                ", birthday=" + birthday +
                ", mobile='" + mobile + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}

**StudentDaoTest**（测试类）
@SpringBootTest
class StudentDaoTest {
    @Autowired
    private  StudentDao studentDao;


    @Test
    public  void  findById(){
        StudentEntity studentEntity = studentDao.findById(14).get();
        System.out.println("findById(14):"+studentEntity);
    }

    @Test
    public void  findAll(){
        Iterable<StudentEntity> list = studentDao.findAll();
        for (StudentEntity stu : list) {
            System.out.println(stu.getId() +"\t"+stu.getName());
            System.out.println("===================");
            System.out.println(stu);
        }
    }

    @Test
    public  void  update(){
        StudentEntity entity1 = studentDao.findById(14).get();
        entity1.setPwd("******");
        studentDao.save(entity1);

        StudentEntity entity2 = studentDao.findById(14).get();
        Assertions.assertEquals(entity1.getPwd(),entity2.getPwd());

        StudentEntity findById = studentDao.findById(14).get();
        System.out.println(findById);
    }

    @Test
    public  void  insert(){
        try {
        StudentEntity studentEntity = new StudentEntity();
        studentEntity.setId(20);
        studentEntity.setStudentId("19301930");
        studentEntity.setPwd("*********");
        studentEntity.setName("小向");
        studentEntity.setGender(0);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date parse =  dateFormat.parse("2000-11-11");
        studentEntity.setBirthday(parse);
        studentEntity.setMobile("19987920701");
        studentEntity.setAddress("四川成都");

        studentDao.save(studentEntity);


        } catch (Exception e) {
            e.printStackTrace();
            return;
        }


        StudentEntity entityList = studentDao.findById(20).get();
        System.out.println(entityList);

    }


    @Test
    public  void  delete(){
        StudentEntity entity = studentDao.findById(20).get();
        System.out.println(entity);
        System.out.println("============");
        studentDao.delete(entity);
    }


    @Test
    public  void  deleteById(){
        studentDao.deleteById(15);
        System.out.println("删除成功");

    }

    @Test
    public  void  deleteById2(){
        List<Integer> list = new ArrayList<>();
        list.add(12);
        list.add(13);

        studentDao.deleteAllById(list);
        System.out.println("删除成功");
    }

    @Test
    public  void  deleteAll(){
        List<StudentEntity> delete = new ArrayList<>();
        StudentEntity entity = new StudentEntity();
        entity.setId(1);
        delete.add(entity);

        delete.add(studentDao.findById(3).get());
        studentDao.deleteAll(delete);

        System.out.println("删除成功");
    }


}



**ManagerController**（controller 层）

@Controller

@RequestMapping("/manager")
public class ManagerController {

    @Autowired
    private ManagerDao managerDao;
    private Logger logger = LoggerFactory.getLogger(getClass());

    @RequestMapping(value = {"", "/", "/list"})
    public String list(Model model) {
        List<Manager> list = managerDao.findAll();
        model.addAttribute("list", list);
        return "manager/list";
    }

    @RequestMapping(value = {"/new"})
    public String nwePage(Model model) {
        return "manager/new";
    }

//    @ResponseBody
    @PostMapping("/create")
    public String create(Manager manager, RedirectAttributes redirectAttributes) {
        logger.info(manager.getLoginid());
        logger.info(manager.getRealname());
        System.out.println(manager.getRealname());

        manager.setLogincount(0);
        manager.setPwd("");
        boolean save = managerDao.save(manager);
        if (save){

            logger.info("insert success");

            redirectAttributes.addFlashAttribute("msg","新增成功");
            return  "redirect:manager/list";
        }
        else {
            logger.info("inert fail");
            redirectAttributes.addFlashAttribute("msg","新增成功");
            return "redirect:manager/new";
        }


    }

}


**前端  首页**
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>首页</title>
  <head th:include="bootstrap/t :: bs" th:remove="tag"></head>

</head>
<body>
<div th:replace="bootstrap/t :: nav" th:remove="tag"></div>
<!--<button class="btn btn-primary">update</button>-->


</body>
</html>



**表格**
  <table class="table  table-hover">
        <tr>
            <th>ID</th>
            <th>登录名</th>
            <th>密码</th>
            <th>姓名</th>
            <th>登录次数</th>
            <th>登录时间</th>
            <th>操作</th>
        </tr>
        <tr th:each="manager :${list}">
            <td th:text="${manager.id}"></td>
            <td th:text="${manager.loginid}"></td>
            <td th:text="${manager.pwd}"></td>
            <td th:text="${manager.realname}"></td>
            <td th:text="${manager.logincount}"></td>
            <td th:text="${manager.lastlogindt}"></td>
            <td></td>
        </tr>
    </table>


**新建表单页面**
 <form method="post" th:action="@{/manager/create}">
        <div class="form-group">
            <label for="loginid">登录名</label>
            <input type="text" class="form-control" id="loginid" name="loginid" aria-describedby="loginIdHelp" placeholder="loginId">
            <small id="loginIdHelp" class="form-text text-muted">loginId 维一.</small>
        </div>
        <div class="form-group">
            <label for="realname">登录名</label>
            <input type="text" class="form-control" id="realname" name="realname" aria-describedby="realNameHelp" placeholder="realName">
            <small id="realNameHelp" class="form-text text-muted">realName 姓名.</small>
        </div>
        <button type="submit" class="btn btn-primary">提交</button>
    </form>




