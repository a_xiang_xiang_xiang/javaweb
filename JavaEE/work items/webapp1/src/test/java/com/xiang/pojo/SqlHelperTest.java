package com.xiang.pojo;

import com.xiang.mapper.UserMapper;
import com.xiang.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;



@SpringBootTest
class SqlHelperTest {
    @Autowired
    private UserMapper userMapper;



    @Test
    public void testSelect() {
        userMapper.findAll();
    }

    @Test
    public void findById() {
        String[] parameters = {"5"};
        userMapper.findById(parameters);
    }

    @Test
    public void insert() {
        final User user = new User(7, "小向", "四川广元", "13419171453");
        userMapper.insertUser(user);
    }

    @Test
    public void update() {
        userMapper.update(7, "向向");
    }

}