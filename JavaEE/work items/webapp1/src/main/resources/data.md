阅读Java Mysql版的SqlHelper代码。（https://blog.csdn.net/weixin_30586257/article/details/94991063）

准备mysql数据库，表(User)。

将上面的SqlHelp代码移植到自己的项目中

创建UserDao，在该类中使用Spring的注入方式完成UserDao的findAll, findById, insert, update等方法。

给UserDao的每个方法创建一个测试用例，完成测试。



不录视频，提交最后运行结果和核心代码。