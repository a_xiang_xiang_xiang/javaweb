create database javaee charset utf8mb4;

create user 'javaee'@'localhost' identified by 'javaee';
grant all on javaee.* to 'javaee'@'localhost';


-- # mysql -ujavaee -pjavaee javaee -h127.0.0.1

SET NAMES utf8mb4;
SET
    FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`
(
    `id`          int(11)                                                       NOT NULL,
    `name`        varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci  NULL DEFAULT NULL,
    `address`     varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
    `phoneNumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci  NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci
  ROW_FORMAT = Dynamic;


INSERT INTO `user`
VALUES (1, '向贵敏', '四川广元', '13419171453');
INSERT INTO `user`
VALUES (2, '向贵敏', '四川广元', '13419171453');
INSERT INTO `user`
VALUES (3, '向贵敏', '四川广元', '13419171453');
INSERT INTO `user`
VALUES (4, '向贵敏', '四川广元', '13419171453');
INSERT INTO `user`
VALUES (5, '向贵敏', '四川广元', '13419171453');
INSERT INTO `user`
VALUES (6, '向贵敏', '四川广元', '13419171453');

SET
    FOREIGN_KEY_CHECKS = 1;
