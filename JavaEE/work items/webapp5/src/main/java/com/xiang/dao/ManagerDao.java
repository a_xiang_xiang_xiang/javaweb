package com.xiang.dao;

import com.xiang.model.ManagerEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;
import java.util.List;
import java.util.Map;

@Repository
public interface ManagerDao extends CrudRepository<ManagerEntity,Integer> {

    public ManagerEntity findByLoginId(String loginId);

    @Query(value = "select * from  manager order by loginCount desc  limit 1", nativeQuery = true)
    public  ManagerEntity findByMaxLoginCount();


    @Query(value = "select loginCount from  manager order by loginCount desc  limit 1", nativeQuery = true)
    public  Integer findByManagerMaxLoginCount();


//    查询一条
    @Query(value = "select loginId,realName  from  manager where id = :id ",nativeQuery = true)
    public Map<String,String> findLoginIdRealNameById(Integer id);


//    查询多条
    @Query(value = "select loginId,realName  from  manager where id > :min and  id < :max",nativeQuery = true)
    public List<Map<String,String>> findLoginIdRealNameByIdRang(Integer min, Integer max);


}