package com.xiang.dao;

import com.xiang.model.Class;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassDao  extends CrudRepository<Class,Integer> {
}
