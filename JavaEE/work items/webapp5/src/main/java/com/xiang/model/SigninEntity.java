package com.xiang.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "signin", schema = "today_the_campus", catalog = "")
public class SigninEntity {
    private int id;
    private String studentId;
    private Timestamp signDatetime;
    private Date signDate;
    private BigDecimal temperature;
    private int working;
    private int hadTravel;
    private String address;
    private int jkt;
    private String jktColor;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "studentId", nullable = false, length = 8)
    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    @Basic
    @Column(name = "signDatetime", nullable = false)
    public Timestamp getSignDatetime() {
        return signDatetime;
    }

    public void setSignDatetime(Timestamp signDatetime) {
        this.signDatetime = signDatetime;
    }

    @Basic
    @Column(name = "signDate", nullable = false)
    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    @Basic
    @Column(name = "temperature", nullable = true, precision = 1)
    public BigDecimal getTemperature() {
        return temperature;
    }

    public void setTemperature(BigDecimal temperature) {
        this.temperature = temperature;
    }

    @Basic
    @Column(name = "working", nullable = false)
    public int getWorking() {
        return working;
    }

    public void setWorking(int working) {
        this.working = working;
    }

    @Basic
    @Column(name = "hadTravel", nullable = false)
    public int getHadTravel() {
        return hadTravel;
    }

    public void setHadTravel(int hadTravel) {
        this.hadTravel = hadTravel;
    }

    @Basic
    @Column(name = "address", nullable = true, length = 100)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "jkt", nullable = false)
    public int getJkt() {
        return jkt;
    }

    public void setJkt(int jkt) {
        this.jkt = jkt;
    }

    @Basic
    @Column(name = "jktColor", nullable = true, length = 20)
    public String getJktColor() {
        return jktColor;
    }

    public void setJktColor(String jktColor) {
        this.jktColor = jktColor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SigninEntity that = (SigninEntity) o;
        return id == that.id && working == that.working && hadTravel == that.hadTravel && jkt == that.jkt && Objects.equals(studentId, that.studentId) && Objects.equals(signDatetime, that.signDatetime) && Objects.equals(signDate, that.signDate) && Objects.equals(temperature, that.temperature) && Objects.equals(address, that.address) && Objects.equals(jktColor, that.jktColor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, studentId, signDatetime, signDate, temperature, working, hadTravel, address, jkt, jktColor);
    }
}
