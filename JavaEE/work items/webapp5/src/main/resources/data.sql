/*
 Navicat Premium Data Transfer

 Source Server         : today_the_campus
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : today_the_campus

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 06/07/2021 09:30:25
*/


# create database today_the_campus charset utf8mb4;

# create user 'campus'@'localhost' identified by 'campus';
# grant all on today_the_campus.* to 'campus'@'localhost';


-- # mysql -ucampus -pcampus today_the_campus -h127.0.0.1

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for manager
-- ----------------------------
DROP TABLE IF EXISTS `manager`;
CREATE TABLE `manager`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loginId` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `realName` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `pwd` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `loginCount` int(11) NULL DEFAULT NULL,
  `lastLoginDt` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `signIn_index1`(`loginId`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 47 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of manager
-- ----------------------------
INSERT INTO `manager` VALUES (1, 'xiang', '向向向', 'c59545a69f963aeb17674a103de705ff', 111, '2021-05-26 16:17:45');
INSERT INTO `manager` VALUES (5, 'yyyy', '向向向', '****', 111, '2000-11-11 00:00:00');
INSERT INTO `manager` VALUES (42, 'dada', '向向向', '*******3*', 111, '2021-05-24 00:00:00');

-- ----------------------------
-- Table structure for signin
-- ----------------------------
DROP TABLE IF EXISTS `signin`;
CREATE TABLE `signin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studentId` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `signDatetime` datetime(0) NOT NULL,
  `signDate` date NOT NULL,
  `temperature` decimal(18, 1) NULL DEFAULT NULL,
  `working` int(11) NOT NULL,
  `hadTravel` int(11) NOT NULL,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `jkt` int(11) NOT NULL,
  `jktColor` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `signIn_index1`(`studentId`, `signDate`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of signin
-- ----------------------------
INSERT INTO `signin` VALUES (1, '19301043', '2021-04-13 07:33:33', '2021-04-13', 36.5, 1, 0, '四川广元', 1, '绿');
INSERT INTO `signin` VALUES (2, '19301044', '2021-04-13 07:25:33', '2021-04-13', 36.5, 1, 0, '四川广元', 1, '绿');
INSERT INTO `signin` VALUES (3, '19301045', '2021-04-13 07:35:33', '2021-04-13', 36.5, 1, 0, '四川广元', 1, '绿');
INSERT INTO `signin` VALUES (4, '19301046', '2021-04-13 07:37:33', '2021-04-13', 36.5, 1, 0, '四川广元', 1, '绿');
INSERT INTO `signin` VALUES (5, '19301047', '2021-04-13 07:32:33', '2021-04-13', 36.5, 1, 0, '四川广元', 1, '绿');
INSERT INTO `signin` VALUES (6, '19301048', '2021-04-13 07:23:33', '2021-04-13', 36.5, 1, 0, '四川广元', 1, '绿');
INSERT INTO `signin` VALUES (7, '19301049', '2021-04-13 07:13:33', '2021-04-13', 36.5, 1, 0, '四川广元', 1, '绿');
INSERT INTO `signin` VALUES (8, '19301050', '2021-04-13 07:43:33', '2021-04-13', 36.5, 1, 0, '四川广元', 1, '绿');
INSERT INTO `signin` VALUES (9, '19301051', '2021-04-13 07:36:33', '2021-04-13', 36.5, 1, 0, '四川广元', 1, '绿');
INSERT INTO `signin` VALUES (10, '19301052', '2021-04-13 07:14:33', '2021-04-13', 36.5, 1, 0, '四川广元', 1, '绿');
INSERT INTO `signin` VALUES (11, '19301053', '2021-04-13 07:32:33', '2021-04-13', 36.5, 1, 0, '四川广元', 1, '绿');
INSERT INTO `signin` VALUES (12, '19301058', '2021-04-13 07:11:33', '2021-04-13', 36.5, 1, 0, '四川广元', 1, '绿');
INSERT INTO `signin` VALUES (13, '19301054', '2021-04-13 07:02:33', '2021-04-13', 36.5, 1, 0, '四川广元', 1, '绿');

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studentId` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `pwd` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `gender` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `mobile` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `student_index1`(`studentId`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (2, '19301044', '123456', '廖章涛', 1, '2000-01-10', '13419171453', '四川泸州');
INSERT INTO `student` VALUES (4, '19301046', '123456', '徐代番', 1, '2000-01-10', '13419171453', '四川泸州');
INSERT INTO `student` VALUES (5, '19301047', '123456', '廖希', 1, '2000-01-10', '13419171453', '四川泸州');
INSERT INTO `student` VALUES (6, '19301048', '123456', '马昆', 1, '2000-01-10', '13419171453', '四川泸州');
INSERT INTO `student` VALUES (7, '19301049', '123456', '李家志', 1, '2000-01-10', '13419171453', '四川泸州');
INSERT INTO `student` VALUES (8, '19301050', '123456', '江哼龙', 1, '2000-01-10', '13419171453', '四川泸州');
INSERT INTO `student` VALUES (9, '19301052', '123456', '秦海伦', 1, '2000-01-10', '13419171453', '四川泸州');
INSERT INTO `student` VALUES (10, '19301051', '123456', '袁扬', 1, '2000-01-10', '13419171453', '四川泸州');
INSERT INTO `student` VALUES (11, '19301053', '123456', '陶勇', 1, '2000-01-10', '13419171453', '四川泸州');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `uname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `upawd` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('xiang', '123456');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `gender` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `mobile` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '周圳南', 1, '2000-01-10', '13419171453', '四川泸州');
INSERT INTO `users` VALUES (2, '向贵敏', 1, '2000-01-10', '13419171453', '四川泸州');
INSERT INTO `users` VALUES (3, '廖贤龙', 1, '2000-01-10', '13419171453', '四川泸州');
INSERT INTO `users` VALUES (4, '廖希', 1, '2000-01-10', '13419171453', '四川泸州');
INSERT INTO `users` VALUES (5, '廖章涛', 1, '2000-01-10', '13419171453', '四川泸州');
INSERT INTO `users` VALUES (6, '刘芮豪', 1, '2000-01-10', '13419171453', '四川泸州');
INSERT INTO `users` VALUES (7, '徐代藩', 1, '2000-01-10', '13419171453', '四川泸州');

SET FOREIGN_KEY_CHECKS = 1;






create database today_the_campus charset utf8mb4;

create user 'campus'@'localhost' identified by 'campus';
grant all on today_the_campus.* to 'campus'@'localhost';



-- # mysql -ucampus -pcampus today_the_campus -h127.0.0.1


create  table  student(
                          id int AUTO_INCREMENT not null  PRIMARY KEY,#编号
                          studentId varchar(8) not null ,#学号
                          pwd varchar(100),#密码
                          name  varchar(20),#姓名
                          gender int not null ,--性别（1：男，0：女）
                          birthday date not null ,#出生日期
                          mobile varchar(11) not null ,#电话
                          address varchar(100)#地址
);
create unique index student_index1 on student(studentId);



create  table  signIn (
                          id int AUTO_INCREMENT not null  PRIMARY KEY,#编号
                          studentId varchar(8) not null ,#学号
                          signDatetime datetime not null ,#签到时间
                          signDate date not null ,#签到日期
                          temperature decimal(18,1),#温度
                          working int not null ,--是否在岗（1：在，0：不在）
                          hadTravel int not null ,--是否出行（1：出行，0：不出行）
                          address varchar(100),#签到地址
    jkt int not null ,--是否申请健康通（1：是，0：没有）
    jktColor varchar(20)--健康码颜色（绿，红，黄）
);
create unique index signIn_index1 on signIn(studentId, signDate);
ALTER TABLE signIn ADD FOREIGN KEY (studentId) REFERENCES student(studentId);


# 管理员帐号表
create  table  manager(
                          id int AUTO_INCREMENT not null  PRIMARY KEY,#编号
                          loginId varchar(20) not null ,#登录名
                          realName varchar(10),#真实姓名
                          pwd varchar(200),#密码
                          loginCount int,#登录次数
                          lastLoginDt datetime#最后登录时间

);
create unique index signIn_index1 on manager(loginId);

INSERT INTO `signIn`(`studentId`,`signDatetime`,`signDate`,`temperature`,`working`,`hadTravel`,`address`,`jkt`,`jktColor`)
VALUES( '19301043', '2021-04-13 07:33:33','2021-04-13 07:33:33', '36.5', 1, '0', '四川广元', '1','绿'),
      ( '19301044', '2021-04-13 07:25:33','2021-04-13 07:25:33', '36.5', 1, '0', '四川广元', '1','绿'),
      ( '19301045', '2021-04-13 07:35:33','2021-04-13 07:35:33', '36.5', 1, '0', '四川广元', '1','绿'),
      ( '19301046', '2021-04-13 07:37:33','2021-04-13 07:37:33', '36.5', 1, '0', '四川广元', '1','绿'),
      ( '19301047', '2021-04-13 07:32:33','2021-04-13 07:32:33', '36.5', 1, '0', '四川广元', '1','绿'),
      ( '19301048', '2021-04-13 07:23:33','2021-04-13 07:23:33', '36.5', 1, '0', '四川广元', '1','绿'),
      ( '19301049', '2021-04-13 07:13:33','2021-04-13 07:13:33', '36.5', 1, '0', '四川广元', '1','绿'),
      ( '19301050', '2021-04-13 07:43:33','2021-04-13 07:43:33', '36.5', 1, '0', '四川广元', '1','绿'),
      ( '19301051', '2021-04-13 07:36:33', '2021-04-13 07:36:33', '36.5', 1, '0', '四川广元', '1','绿'),
      ( '19301052', '2021-04-13 07:14:33','2021-04-13 07:14:33', '36.5', 1, '0', '四川广元', '1','绿'),
      ( '19301053', '2021-04-13 07:32:33', '2021-04-13 07:32:33', '36.5', 1, '0', '四川广元', '1','绿'),
      ( '19301058', '2021-04-13 07:11:33','2021-04-13 07:11:33', '36.5', 1, '0', '四川广元', '1','绿'),
      ( '19301054', '2021-04-13 07:02:33','2021-04-13 07:02:33', '36.5', 1, '0', '四川广元', '1','绿');


INSERT INTO `student`(`studentId`,`pwd`,`name`,`gender`,`birthday`,`mobile`,`address`)
VALUES ( '19301043', '123456', '周圳南', 1, '2000-01-10', '13419171453', '四川泸州'),
       ( '19301044', '123456', '廖章涛', 1, '2000-01-10', '13419171453', '四川泸州'),
       ( '19301045', '123456', '余磊', 1, '2000-01-10', '13419171453', '四川泸州'),
       ( '19301046', '123456', '徐代番', 1, '2000-01-10', '13419171453', '四川泸州'),
       ( '19301047', '123456', '廖希', 1, '2000-01-10', '13419171453', '四川泸州'),
       ( '19301048', '123456', '马昆', 1, '2000-01-10', '13419171453', '四川泸州'),
       ( '19301049', '123456', '李家志', 1, '2000-01-10', '13419171453', '四川泸州'),
       ( '19301050', '123456', '江哼龙', 1, '2000-01-10', '13419171453', '四川泸州'),
       ( '19301052', '123456', '秦海伦', 1, '2000-01-10', '13419171453', '四川泸州'),
       ( '19301051', '123456', '袁扬', 1, '2000-01-10', '13419171453', '四川泸州'),
       ( '19301053', '123456', '陶勇', 1, '2000-01-10', '13419171453', '四川泸州'),
       ( '19301054', '123456', '王红', 1, '2000-01-10', '13419171453', '四川泸州'),
       ( '19301058', '123456', '向贵敏', 1, '2000-01-10', '13419171453', '四川泸州');


insert into  manager (loginId, realName, pwd, loginCount, lastLoginDt)
values ('xiang','小向',null,0,null);
-- 密码为 123456
values ('xiang','小向','c59545a69f963aeb17674a103de705ff',0,null);

update  manager set  pwd = 'c59545a69f963aeb17674a103de705ff'
where  loginId  = 'xiang';







