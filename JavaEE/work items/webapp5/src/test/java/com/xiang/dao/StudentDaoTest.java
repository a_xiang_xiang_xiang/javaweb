package com.xiang.dao;

import com.xiang.model.StudentEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class StudentDaoTest {
    @Autowired
    private  StudentDao studentDao;


    @Test
    public  void  findById(){
        StudentEntity studentEntity = studentDao.findById(14).get();
        System.out.println("findById(14):"+studentEntity);
    }

    @Test
    public void  findAll(){
        Iterable<StudentEntity> list = studentDao.findAll();
        for (StudentEntity stu : list) {
            System.out.println(stu.getId() +"\t"+stu.getName());
            System.out.println("===================");
            System.out.println(stu);
        }
    }

    @Test
    public  void  update(){
        StudentEntity entity1 = studentDao.findById(14).get();
        entity1.setPwd("******");
        studentDao.save(entity1);

        StudentEntity entity2 = studentDao.findById(14).get();
        Assertions.assertEquals(entity1.getPwd(),entity2.getPwd());

        StudentEntity findById = studentDao.findById(14).get();
        System.out.println(findById);
    }

    @Test
    public  void  insert(){
        try {
        StudentEntity studentEntity = new StudentEntity();
        studentEntity.setId(20);
        studentEntity.setStudentId("19301930");
        studentEntity.setPwd("*********");
        studentEntity.setName("小向");
        studentEntity.setGender(0);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date parse =  dateFormat.parse("2000-11-11");
        studentEntity.setBirthday(parse);
        studentEntity.setMobile("19987920701");
        studentEntity.setAddress("四川成都");

        studentDao.save(studentEntity);


        } catch (Exception e) {
            e.printStackTrace();
            return;
        }


        StudentEntity entityList = studentDao.findById(20).get();
        System.out.println(entityList);

    }


    @Test
    public  void  delete(){
        StudentEntity entity = studentDao.findById(20).get();
        System.out.println(entity);
        System.out.println("============");
        studentDao.delete(entity);
    }


    @Test
    public  void  deleteById(){
        studentDao.deleteById(15);
        System.out.println("删除成功");

    }

    @Test
    public  void  deleteById2(){
        List<Integer> list = new ArrayList<>();
        list.add(12);
        list.add(13);

        studentDao.deleteAllById(list);
        System.out.println("删除成功");
    }

    @Test
    public  void  deleteAll(){
        List<StudentEntity> delete = new ArrayList<>();
        StudentEntity entity = new StudentEntity();
        entity.setId(1);
        delete.add(entity);

        delete.add(studentDao.findById(3).get());
        studentDao.deleteAll(delete);

        System.out.println("删除成功");
    }


}