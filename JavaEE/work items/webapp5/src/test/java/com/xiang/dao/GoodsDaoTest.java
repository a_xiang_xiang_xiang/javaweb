package com.xiang.dao;

import com.xiang.model.Goods;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class GoodsDaoTest {
    @Autowired
    private GoodsDao goodsDao;

    @Test
    public void insert() {
        try {
            Goods into = new Goods();
            into.setName("棒棒爊");
            BigDecimal decimal = new BigDecimal("5.00");
            into.setPrice(decimal);
            into.setUnit("四川成都");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date = format.parse("2021-01-08");
            into.setDateProduction(date);

            goodsDao.save(into);
            System.out.println("插入数据为：" + into);
        } catch (ParseException e) {
            e.printStackTrace();
            return;
        }
    }

    @Test
    public  void  into(){
        Goods into = new Goods();
        into.setUnit("广元可口可乐有限公司");
        BigDecimal decimal = new BigDecimal("3.00");
        into.setPrice(decimal);
        into.setName("可口可乐");
        Date date = new Date();
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:ss:mm");
        into.setDateProduction(date);
        goodsDao.save(into);
        System.out.println(into);
    }


    @Test
    public void BigDecimal() {
        BigDecimal a = new BigDecimal(0.1);
        System.out.println("a values is:" + a);
        System.out.println("=====================");
        BigDecimal b = new BigDecimal("0.1");
        System.out.println("b values is:" + b);
    }

    @Test
    public void date() {
        //获取任务发布的时间
        Date  date =new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd  hh:mm:ss");
        String date1 = sdf.format(date);
        System.out.println(date1);
    }


    @Test
    public  void  findById(){
        Goods findById = goodsDao.findById(1).get();
        System.out.println(findById);
    }
}