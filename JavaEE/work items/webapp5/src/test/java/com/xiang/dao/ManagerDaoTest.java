package com.xiang.dao;

import com.xiang.model.ManagerEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ManagerDaoTest {
    @Autowired
    private ManagerDao managerDao;

//    private Logger logger = LoggerFactory.getLogger(getClass());
//    private  Logger logger = LoggerFactory.getLogger(ManagerDaoTest.class);

    private  Logger logger = LoggerFactory.getLogger(getClass());


    @Test
    public void deleteFindAll() {
        List<Integer> list = new ArrayList<>();
        list.add(2);
        list.add(3);
        managerDao.deleteAllById(list);

        System.out.println(list);
        System.out.println("删除成功");
    }

    @Test
    public void findById() {
        ManagerEntity entity = managerDao.findById(1).get();
        System.out.println(entity);
    }

    @Test
    public void insert() {
        try {
            ManagerEntity entity = new ManagerEntity();
//            entity.setId(5);
            entity.setPwd("****");
            entity.setRealName("xccc");
            entity.setLoginCount(10);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date parse = format.parse("2000-11-11");
            entity.setLoginId("yyyy12312");
            entity.setLastLoginDt(parse);

            managerDao.save(entity);


            System.out.println(entity);
            System.out.println("insert suss");
        } catch (ParseException e) {
            e.printStackTrace();
            return;
        }

    }



    @Test
    public void insertInto() {
        try {
            ManagerEntity into = new ManagerEntity();
            into.setLoginId("789-123");
            into.setLoginCount(123);
            into.setRealName("刘德华");
            into.setPwd("***");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date = format.parse("2000-01-12");
            into.setLastLoginDt(date);

            ManagerEntity save = managerDao.save(into);
            System.out.println("插入的数据为："+save);

            System.out.println("插入的数据为："+into);
        } catch (ParseException e) {
            e.printStackTrace();
            return;
        }
    }

    @Test
    public  void  deleteByIdAll(){
        List<Integer> deleteList = new ArrayList<>();
        deleteList.add(45);
        deleteList.add(46);
        managerDao.deleteAllById(deleteList);
        System.out.println("删除的数据ID为："+deleteList);
    }

    @Test
    public  void  finAll(){
        for (ManagerEntity list : managerDao.findAll()) {
            System.out.println("修改前的数据："+list);

            list.setLoginCount(111);
            list.setRealName("向向向");
            ManagerEntity save = managerDao.save(list);
            System.out.println("修改后的数据："+save);
        }
    }


    @Test
    public  void  findById2(){
        ManagerEntity find = managerDao.findById(1).get();
        System.out.println("查询数据为："+find);
    }


    @Test
    public  void findByIdAll(){
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(5);
        System.out.print(managerDao.findAllById(list));

    }


    @Test
    public  void  findByLoginId(){
        System.out.println(managerDao.findByLoginId("xiang"));
        System.out.println("============");
        ManagerEntity id = managerDao.findByLoginId("xiang");
        Assertions.assertEquals("向向向",id.getRealName());
        System.out.println(id);
    }

    @Test
    public  void  findByMaxLoginCount(){
        final ManagerEntity byMaxLoginCount = managerDao.findByMaxLoginCount();
        Assertions.assertEquals("向向向",byMaxLoginCount.getRealName());
        System.out.println(byMaxLoginCount);
    }


    @Test
    public  void  findByManagerMaxLoginCount(){
        Integer count = managerDao.findByManagerMaxLoginCount();
        Assertions.assertEquals(111,count);
        System.out.println(count);

    }

    @Test
    public  void  findLoginIdRealNameById(){
        System.out.println("开始执行；findLoginIdRealNameById");
        Map<String, String> realNameById = managerDao.findLoginIdRealNameById(1);
        if (realNameById == null){
            System.out.println("realNameById == null");
        }
        else {
            System.out.println("realNameById != null");
        }
        Assertions.assertEquals("向向向",realNameById.get("realName"));
        System.out.println(realNameById.get("realName"));

    }

    @Test
    public  void  findLoginIdRealNameByIdRang(){
        List<Map<String, String>> idRang = managerDao.findLoginIdRealNameByIdRang(1,10);
        for (Map<String,String> map : idRang){
            String loginId = map.get("loginId");
            String realName = map.get("loginId");

            System.out.println("loginId:"+loginId+"\t"+"realName:"+realName);
            System.out.println("====================");

            //    BannerLogger
            logger.info("loginId:"+loginId+"\t"+"realName:"+realName);

            System.out.println(idRang);
        }
    }






}