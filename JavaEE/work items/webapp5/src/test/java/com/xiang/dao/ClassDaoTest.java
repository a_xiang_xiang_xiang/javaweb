package com.xiang.dao;

import com.xiang.model.Class;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ClassDaoTest {
    @Autowired
    private ClassDao classDao;

    @Test
    public void insertInto() {
        try {
            Class into = new Class();
            into.setName("软件19-9班");
            into.setUnit("软件学院");
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//            Date parse = format.parse("2021-11-11");
            Date date = new Date();
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//            Date date1 = format.parse(String.valueOf(date));
            into.setSetTime(date);
            BigDecimal decimal = new BigDecimal("500.00");
            into.setClassExpense(decimal);
            classDao.save(into);
            System.out.println(into);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

    }



    @Test
    public void  findById(){
        Class findById = classDao.findById(1).get();
        System.out.println(findById);
    }
}