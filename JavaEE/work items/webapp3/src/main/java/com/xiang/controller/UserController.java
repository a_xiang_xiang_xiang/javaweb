package com.xiang.controller;

import com.xiang.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/xiang")
public class UserController {
    /*
    创建控制器UserController。

    增加方法edit,关联地址/user/edit，邦定到edit.html，该页面包含form。

    增加方法update,关联地址/user/update，在该方法中获取user的每个字段的值，在页面输出。
    */
    @GetMapping("/user/edit")
    public String edit() {
        return "edit";
    }

    /*
    *  private  int id;
    private  String name;
    private Date birthday;
    private  Double salary;*/
    @PostMapping("/user/update")
    public  String update(HttpServletRequest request, Model model){
        User user = new User();

        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String birthday = request.getParameter("birthday");
        String salary = request.getParameter("salary");


        try {
            user.setId(Integer.parseInt(id));
            user.setName(name);
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            Date parse = dateFormat.parse(birthday);
//            user.setBirthday(parse);
//            user.setSalary(Double.parseDouble(salary));
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date parse = simpleDateFormat.parse(birthday);
            user.setBirthday(parse);
            user.setSalary(Double.parseDouble(salary));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        model.addAttribute("user",user);

        System.out.println("id:"+id+"\nname:"+name+"\nbirthday:"+birthday+"\nsalary:"+salary);

        return  "update";
    }
}
