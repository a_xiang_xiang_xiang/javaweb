package com.xiang.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class User {
    //包含：id, name, birthday(日期), salary(浮点类型 工资)等字段
    private  int id;
    private  String name;
    private Date birthday;
    private  Double salary;

}
