package com.xiang.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyHash {
    /*
    字符串
    找出重复的数字 ；
     */

    private String data;

    public MyHash(String data) {
        this.data = data;
    }

    public MyHash() {

    }

    public List<Integer> findRepeatNum() {
        List<Integer> list = new ArrayList<>();

        HashMap<Object, Object> hashMap = new HashMap<>();
        String[] split = data.split(",");
        for (String item : split) {
            item = item.trim();

            Integer num = Integer.parseInt(item);
            if (hashMap.containsKey(num)) {
                list.add(num);
            } else {
                System.out.println("将要添加：" + item);
                hashMap.put(num, null);
            }
        }

        //    把不重复的输出
        for (Map.Entry<Object, Object> entry : hashMap.entrySet()) {
            System.out.println( "key"+ entry.getKey());
        }
        return list;
    }
}
