package com.xiang.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MyHashTest {

    @Test
    void findRepeatNum() {
        String data = "1,2,3,5,2,2,1";
        final MyHash myHash = new MyHash(data);
        List<Integer> list = myHash.findRepeatNum();
        Assertions.assertEquals(3,list.size());

    }
}