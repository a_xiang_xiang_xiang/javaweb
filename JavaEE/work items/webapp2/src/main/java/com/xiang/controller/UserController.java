package com.xiang.controller;

import com.xiang.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/xiang")
public class UserController {
    /*
    创建控制器UserController。

    增加方法edit,关联地址/user/edit，邦定到edit.html，该页面包含form。

    增加方法update,关联地址/user/update，在该方法中获取user的每个字段的值，在页面输出。
    */


    @GetMapping("/user/edit")
    public String edit() {
        return "edit";
    }

    @PostMapping("/user/update")
    public String update(HttpServletRequest request, Model model) {
        User user = new User();
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String birthday = request.getParameter("birthday");
        String salary = request.getParameter("salary");


        try {
            user.setId(Integer.parseInt(id));
            user.setName(name);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date parse = simpleDateFormat.parse(birthday);
            user.setBirthday(parse);
            user.setSalary(Double.parseDouble(salary));
        } catch (Exception e) {
            e.printStackTrace();
        }


        model.addAttribute("user", user);

        System.out.println("学号：" + id + "\n姓名：" + name + "\n生日：" + birthday + "\n工资：" + salary);


        return "update";
    }

    /*
    thymeleaf 的使用
    */

    @GetMapping("/user/thymeleaf")
    public String thymeleaf(Model model) {
//        传参
        model.addAttribute("name","小不点");
        model.addAttribute("male",false);
        return "thymeleaf";
    }

    @GetMapping("/user/userList")
    public  String userList(Model model){
        List<User> list = new ArrayList<>();


        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            User user = new User();
            user.setId(001);
            user.setName("小向");
            user.setBirthday(format.parse("2000-06-23"));
            list.add(user);

            user = new User();
            user.setId(002);
            user.setName("小小");
            user.setBirthday(format.parse("2021-11-11"));
            list.add(user);

            user = new User();
            user.setId(003);
            user.setName("小小");
            user.setBirthday(format.parse("2021-11-11"));
            list.add(user);

            user = new User();
            user.setId(004);
            user.setName("小小");
            user.setBirthday(format.parse("2021-11-11"));
            list.add(user);


            model.addAttribute("users",list);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "userList";
    }

}
