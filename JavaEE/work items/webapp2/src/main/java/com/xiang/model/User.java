package com.xiang.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class User {
//包含：id, name, birthday(日期), salary(浮点类型 工资)等字段
    private Integer id;
    private  String name;
    @DateTimeFormat(pattern ="yyyy-MM-dd")
    private Date birthday;
    private  Double salary;
}
