package com.xiang.controller;

import com.xiang.model.Student;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

@Controller
@RequestMapping("/xiang")
public class StudentController {
    @GetMapping("/student/edit")
    public String edit() {
        return "studentEdit";
    }

    @PostMapping("/student/update")
    @ResponseBody
//    public  String update(HttpServletRequest request){
//        String id = request.getParameter("id");
//        String name = request.getParameter("name");
//        String gender = request.getParameter("gender");
//        String age = request.getParameter("age");
//        String birthday = request.getParameter("birthday");
//        System.out.println("学号："+id+"\n姓名：" +name+"\n性别：" + gender+"\n年龄：" + age+"\n生日：" + birthday);
//        return "studentUpdate";
//    }
    public Object update(Student student) {

        System.out.println(student);
        return student;

    }

}
