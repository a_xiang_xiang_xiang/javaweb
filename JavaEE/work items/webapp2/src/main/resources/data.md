
## 在控制器中获取参数 ##

新建springweb项目：

创建model/User.java类，包含：id, name, birthday(日期), salary(浮点)等字段

创建控制器UserController。

增加方法edit,关联地址/user/edit，邦定到edit.html，该页面包含form。

增加方法update,关联地址/user/update，在该方法中获取user的每个字段的值，在页面输出。