create database today_the_campus charset utf8mb4;

create user 'campus'@'localhost' identified by 'campus';
grant all on today_the_campus.* to 'campus'@'localhost';


-- # mysql -ucampus -pcampus today_the_campus -h127.0.0.1


create table student
(
    id        int AUTO_INCREMENT not null PRIMARY KEY,#编号
    studentId varchar(8)         not null,#学号
    pwd       varchar(100),#密码
    name      varchar(20),#姓名
    gender    int                not null,#--性别（1：男，0：女）
    birthday  date               not null,#出生日期
    mobile    varchar(11)        not null,#电话
    address   varchar(100)#地址
);
create unique index student_index1 on student (studentId);



create table signIn
(
    id           int AUTO_INCREMENT not null PRIMARY KEY,#编号
    studentId    varchar(8)         not null,#学号
    signDatetime datetime           not null,#签到时间
    signDate     date               not null,#签到日期
    temperature  decimal(18, 1),#温度
    working      int                not null,#--是否在岗（1：在，0：不在）
    hadTravel    int                not null,#--是否出行（1：出行，0：不出行）
    address      varchar(100),#签到地址
    jkt          int                not null,#--是否申请健康通（1：是，0：没有）
    jktColor     varchar(20)#--健康码颜色（绿，红，黄）
);
create unique index signIn_index1 on signIn (studentId, signDate);
ALTER TABLE signIn
    ADD FOREIGN KEY (studentId) REFERENCES student (studentId);


# 管理员帐号表
create table manager
(
    id          int AUTO_INCREMENT not null PRIMARY KEY,#编号
    loginId     varchar(20)        not null,#登录名
    realName    varchar(10),#真实姓名
    pwd         varchar(200),#密码
    loginCount  int,#登录次数
    lastLoginDt datetime#最后登录时间

);
create unique index signIn_index1 on manager (loginId);

INSERT INTO `signIn`(`studentId`, `signDatetime`, `signDate`, `temperature`, `working`, `hadTravel`, `address`, `jkt`,
                     `jktColor`)
VALUES ('19301043', '2021-04-13 07:33:33', '2021-04-13 07:33:33', '36.5', 1, '0', '四川广元', '1', '绿'),
       ('19301044', '2021-04-13 07:25:33', '2021-04-13 07:25:33', '36.5', 1, '0', '四川广元', '1', '绿'),
       ('19301045', '2021-04-13 07:35:33', '2021-04-13 07:35:33', '36.5', 1, '0', '四川广元', '1', '绿'),
       ('19301046', '2021-04-13 07:37:33', '2021-04-13 07:37:33', '36.5', 1, '0', '四川广元', '1', '绿'),
       ('19301047', '2021-04-13 07:32:33', '2021-04-13 07:32:33', '36.5', 1, '0', '四川广元', '1', '绿'),
       ('19301048', '2021-04-13 07:23:33', '2021-04-13 07:23:33', '36.5', 1, '0', '四川广元', '1', '绿'),
       ('19301049', '2021-04-13 07:13:33', '2021-04-13 07:13:33', '36.5', 1, '0', '四川广元', '1', '绿'),
       ('19301050', '2021-04-13 07:43:33', '2021-04-13 07:43:33', '36.5', 1, '0', '四川广元', '1', '绿'),
       ('19301051', '2021-04-13 07:36:33', '2021-04-13 07:36:33', '36.5', 1, '0', '四川广元', '1', '绿'),
       ('19301052', '2021-04-13 07:14:33', '2021-04-13 07:14:33', '36.5', 1, '0', '四川广元', '1', '绿'),
       ('19301053', '2021-04-13 07:32:33', '2021-04-13 07:32:33', '36.5', 1, '0', '四川广元', '1', '绿'),
       ('19301058', '2021-04-13 07:11:33', '2021-04-13 07:11:33', '36.5', 1, '0', '四川广元', '1', '绿'),
       ('19301054', '2021-04-13 07:02:33', '2021-04-13 07:02:33', '36.5', 1, '0', '四川广元', '1', '绿');


INSERT INTO `student`(`studentId`, `pwd`, `name`, `gender`, `birthday`, `mobile`, `address`)
VALUES ('19301043', '123456', '周圳南', 1, '2000-01-10', '13419171453', '四川泸州'),
       ('19301044', '123456', '廖章涛', 1, '2000-01-10', '13419171453', '四川泸州'),
       ('19301045', '123456', '余磊', 1, '2000-01-10', '13419171453', '四川泸州'),
       ('19301046', '123456', '徐代番', 1, '2000-01-10', '13419171453', '四川泸州'),
       ('19301047', '123456', '廖希', 1, '2000-01-10', '13419171453', '四川泸州'),
       ('19301048', '123456', '马昆', 1, '2000-01-10', '13419171453', '四川泸州'),
       ('19301049', '123456', '李家志', 1, '2000-01-10', '13419171453', '四川泸州'),
       ('19301050', '123456', '江哼龙', 1, '2000-01-10', '13419171453', '四川泸州'),
       ('19301052', '123456', '秦海伦', 1, '2000-01-10', '13419171453', '四川泸州'),
       ('19301051', '123456', '袁扬', 1, '2000-01-10', '13419171453', '四川泸州'),
       ('19301053', '123456', '陶勇', 1, '2000-01-10', '13419171453', '四川泸州'),
       ('19301054', '123456', '王红', 1, '2000-01-10', '13419171453', '四川泸州'),
       ('19301058', '123456', '向贵敏', 1, '2000-01-10', '13419171453', '四川泸州');


insert into manager (loginId, realName, pwd, loginCount, lastLoginDt)
values ('xiang', '小向', null, 0, null);
-- 密码为 123456

insert into manager (loginId, realName, pwd, loginCount, lastLoginDt)
values
    ('xiang','小向','c59545a69f963aeb17674a103de705ff',0, null);

update manager
set pwd = 'c59545a69f963aeb17674a103de705ff'
where loginId = 'xiang';






