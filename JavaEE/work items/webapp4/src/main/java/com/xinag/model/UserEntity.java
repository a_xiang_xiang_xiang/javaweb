//package com.xinag.model;
//
//import javax.persistence.Basic;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Table;
//import java.util.Objects;
//
//@Entity
//@Table(name = "user", schema = "today_the_campus", catalog = "")
//public class UserEntity {
//    private String uname;
//    private String upawd;
//
//    @Basic
//    @Column(name = "uname", nullable = false, length = 20)
//    public String getUname() {
//        return uname;
//    }
//
//    public void setUname(String uname) {
//        this.uname = uname;
//    }
//
//    @Basic
//    @Column(name = "upawd", nullable = true, length = 200)
//    public String getUpawd() {
//        return upawd;
//    }
//
//    public void setUpawd(String upawd) {
//        this.upawd = upawd;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        UserEntity that = (UserEntity) o;
//        return Objects.equals(uname, that.uname) && Objects.equals(upawd, that.upawd);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(uname, upawd);
//    }
//}
