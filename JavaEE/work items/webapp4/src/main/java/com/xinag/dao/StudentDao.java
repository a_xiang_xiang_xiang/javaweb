package com.xinag.dao;

import com.xinag.model.StudentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository //仓库


public interface StudentDao  extends CrudRepository<StudentEntity,Integer> {

}
