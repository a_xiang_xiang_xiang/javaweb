package com.xiang.controller;

import com.xiang.dao.ManagerDao;
import com.xiang.model.Manager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller

@RequestMapping("/manager")
public class ManagerController {

    @Autowired
    private ManagerDao managerDao;
    private Logger logger = LoggerFactory.getLogger(getClass());

    @RequestMapping(value = {"", "/", "/list"})
    public String list(Model model) {
        List<Manager> list = managerDao.findAll();
        model.addAttribute("list", list);
        return "manager/list";
    }

    @RequestMapping(value = {"/new"})
    public String nwePage(Model model) {
        return "manager/new";
    }

//    @ResponseBody
    @PostMapping("/create")
    public String create(Manager manager, RedirectAttributes redirectAttributes) {
        logger.info(manager.getLoginid());
        logger.info(manager.getRealname());
        System.out.println(manager.getRealname());

        manager.setLogincount(0);
        manager.setPwd("");
        boolean save = managerDao.save(manager);
        if (save){

            logger.info("insert success");

            redirectAttributes.addFlashAttribute("msg","新增成功");
            return  "redirect:manager/list";
        }
        else {
            logger.info("inert fail");
            redirectAttributes.addFlashAttribute("msg","新增成功");
            return "redirect:manager/new";
        }


    }

}
