package com.xiang.dao;

import com.xiang.mapper.ManagerMapper;
import com.xiang.model.Manager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;

import java.util.List;

@Repository
public class ManagerDao {
     private @Autowired
     ManagerMapper managerMapper;

    public Manager findById(Integer id){
        return managerMapper.selectByPrimaryKey(id);
    }
    public List<Manager> findAll(){
        return managerMapper.selectAll();
    }

    public  boolean save(Manager manager){
        return managerMapper.insert(manager) == 1;
    }
}
