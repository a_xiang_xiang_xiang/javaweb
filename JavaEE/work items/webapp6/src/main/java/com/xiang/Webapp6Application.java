package com.xiang;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.xiang.mapper")
//@MapperScans("com.xinag.mapper")
public class Webapp6Application {

    public static void main(String[] args) {
        SpringApplication.run(Webapp6Application.class, args);
    }

}
