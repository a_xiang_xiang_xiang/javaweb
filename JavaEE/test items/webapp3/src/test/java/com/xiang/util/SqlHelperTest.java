package com.xiang.util;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static org.junit.jupiter.api.Assertions.*;

class SqlHelperTest {

    @Autowired
    private SqlHelper sqlHelper;
    @Test
    void getCs() {
    }

    @Test
    void getConnection() {
        sqlHelper.getConnection();
        System.out.println("数据库连接成功");
    }

    @Test
    void callPro1() {
    }

    @Test
    void callPro2() {
    }

    @Test
    void executeQuery() {
        Connection ct =null;
        PreparedStatement ps=null;
        ResultSet rs=null;

        try {
            //1、获取数据库链接
            ct=SqlHelper.getConnection();
            //2、创建获取rs
            String sql = "select * from users";

            String parameters[]={};
            rs= (ResultSet) sqlHelper.executeQuery(sql,parameters);

            while(rs.next()){
                System.out.println(rs.getString(1)+"\t"+rs.getString(2)+"\t"+rs.getString(3)+"\t"+rs.getString(4)+"\t"+rs.getString(5)+"\t"+rs.getString(6));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            if(rs!=null){
                try {
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                rs=null;
            }
            if(ps!=null){
                try {
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ps=null;
            }
            if(ct!=null){
                try {
                    ct.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ct=null;
            }

        }

    }

    @Test
    void getCt() {
    }

    @Test
    void getPs() {
    }

    @Test
    void getRs() {
    }

    @Test
    void executeUpdate2() {
    }

    @Test
    void executeUpdate() {
    }

    @Test
    void close() {
    }

    @Test
    void resultSetToList() {
    }
}