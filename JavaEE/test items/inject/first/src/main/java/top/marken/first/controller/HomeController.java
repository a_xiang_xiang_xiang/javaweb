package top.marken.first.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/")
public class HomeController {
    @Autowired
    HttpServletRequest request;

    @GetMapping("")
    public String defaultPage(Integer id) {
        System.out.println("id = " + id);
        System.out.println("id2 = " + request.getParameter("id"));
        return "default";
    }
}
