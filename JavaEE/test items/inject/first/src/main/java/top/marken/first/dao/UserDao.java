package top.marken.first.dao;

import org.springframework.stereotype.Component;
import top.marken.first.entity.User;
import top.marken.first.util.SqlHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Component
public class UserDao {
    public List<User> findAll() {
        List<User> users = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "select * from `user`";

        try {
            conn = SqlHelper.getConn();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            users = new ArrayList<>();
            while (rs.next()) {
                users.add(new User(rs.getInt("id"),rs.getString("name"),
                        rs.getInt("gender"),rs.getInt("age"),rs.getDate("birthday")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            SqlHelper.close(conn,ps,rs);
        }
        return users;
    }

    public User findById(int id) {
        User user = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "select * from `user` where id = ?";

        try {
            conn = SqlHelper.getConn();
            ps = conn.prepareStatement(sql);
            ps.setInt(1,id);
            rs = ps.executeQuery();
            while (rs.next()) {
               user = new User(rs.getInt("id"),rs.getString("name"),
                        rs.getInt("gender"),rs.getInt("age"),rs.getDate("birthday"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            SqlHelper.close(conn,ps,rs);
        }
        return user;
    }

    public boolean insert(User user) {
        List<User> users = null;
        Connection conn = null;
        PreparedStatement ps = null;
        String sql = "insert into `user` (name,gender,age,birthday) values (?,?,?,?)";

        try {
            conn = SqlHelper.getConn();
            ps = conn.prepareStatement(sql);
            ps.setString(1,user.getName());
            ps.setInt(2,user.getGender());
            ps.setInt(3,user.getAge());
            ps.setDate(4,user.getBirthday());
            return ps.executeUpdate() == 1;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            SqlHelper.close(conn,ps,null);
        }
        return false;
    }

    public boolean update(User user) {
        List<User> users = null;
        Connection conn = null;
        PreparedStatement ps = null;
        String sql = "update `user` set name = ?,gender = ?,age = ?,birthday = ? where id = ?";

        try {
            conn = SqlHelper.getConn();
            ps = conn.prepareStatement(sql);
            ps.setString(1,user.getName());
            ps.setInt(2,user.getGender());
            ps.setInt(3,user.getAge());
            ps.setDate(4,user.getBirthday());
            ps.setInt(5,user.getId());
            return ps.executeUpdate() == 1;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            SqlHelper.close(conn,ps,null);
        }
        return false;
    }
}
