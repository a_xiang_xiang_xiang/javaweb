create database javaee2 charset utf8mb4;

create user 'javaee2'@'localhost' identified by 'javaee2';
grant all on javaee2.* to 'javaee2'@'localhost';



-- # mysql -ujavaee2 -pjavaee2 javaee2 -h127.0.0.1

# private Integer id;
#     private String name;
#     private Integer gender;
#     private Integer age;
#     private Date birthday;

create table user
(
    id       int  not null PRIMARY KEY,#编号
    name     varchar(20),#姓名
    gender   int,#--性别（1：男，0：女）
    age      int,#年龄
    birthday date not null             #出生日期
);


-- id int AUTO_INCREMENT not null  PRIMARY KEY,#编号


INSERT INTO `user`(`id`, `name`, `gender`, `age`, `birthday`)
VALUES (1, '向贵敏', '1', '18', '2000-06-23'),
       (2, '向贵敏', '1', '18', '2000-06-23'),
       (3, '向贵敏', '1', '19', '2000-06-23'),
       (4, '向贵敏', '1', '20', '2000-06-23'),
       (5, '向贵敏', '1', '21', '2000-06-23');







