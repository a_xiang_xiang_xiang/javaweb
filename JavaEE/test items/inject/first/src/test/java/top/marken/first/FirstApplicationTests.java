package top.marken.first;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import java.text.SimpleDateFormat;

@SpringBootTest
class FirstApplicationTests {

    @Test
    void contextLoads() {
        try {
            String str = "2021-2-02";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            System.out.println(sdf.parse(str));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
