package top.marken.first.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import top.marken.first.entity.User;
import java.sql.Date;
import java.util.List;

@SpringBootTest
class UserDaoTest {
    @Autowired
    private UserDao userDao;

    @Test
    void findAll() {
       List<User> users = userDao.findAll();
        System.out.println(users);
    }

    @Test
    void findById() {
        User user = userDao.findById(1);
        System.out.println(user);
    }

    @Test
    void insert() {
        User user = new User(10,"向向",0,19,new Date(1898989));
        if (userDao.insert(user)) System.out.println("执行成功！");
    }

    @Test
    void update() {
        User user = new User(10,"小向",1,18,new Date(123123123));
        if (userDao.update(user)) System.out.println("执行成功！");
    }
}