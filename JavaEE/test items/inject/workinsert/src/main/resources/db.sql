/*
 Navicat Premium Data Transfer

 Source Server         : javaee
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : javaee

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 08/06/2021 19:53:34
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`
(
    `id`          int(11) NOT NULL,
    `name`        varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
    `address`     varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
    `phoneNumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user`
VALUES (1, '向贵敏', '四川广元', '13419171453');
INSERT INTO `user`
VALUES (2, '向贵敏', '四川广元', '13419171453');
INSERT INTO `user`
VALUES (3, '向贵敏', '四川广元', '13419171453');
INSERT INTO `user`
VALUES (4, '向贵敏', '四川广元', '13419171453');
INSERT INTO `user`
VALUES (5, '向贵敏', '四川广元', '13419171453');
INSERT INTO `user`
VALUES (6, '向贵敏', '四川广元', '13419171453');
INSERT INTO `user`
VALUES (110, '小朋友', '四川广元', '13419171453');
INSERT INTO `user`
VALUES (12, '向向', '四川广元', '13419171453');

SET
FOREIGN_KEY_CHECKS = 1;
