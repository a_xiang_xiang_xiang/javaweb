package cn.com.scitc.workinsert;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkinsertApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorkinsertApplication.class, args);
    }

}
