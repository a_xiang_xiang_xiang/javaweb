package cn.com.scitc.workinsert.helper;

import cn.com.scitc.workinsert.dao.UserDao;
import cn.com.scitc.workinsert.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class SqlHelperTest {
    @Autowired
    private UserDao userDao;
//    @Test
//    public void test1(){
//        SqlHelper.getConnection();
//    }

    @Test
    public void testSelect() {
        userDao.findAll();
    }

    @Test
    public void findById() {
        String[] parameters = {"5"};
        userDao.findById(parameters);
    }

    @Test
    public void insert() {
        final User user = new User(7, "小向", "四川广元", "13419171453");
        userDao.insertUser(user);
    }

    @Test
    public void update() {
        userDao.update(7, "向向");
    }
}