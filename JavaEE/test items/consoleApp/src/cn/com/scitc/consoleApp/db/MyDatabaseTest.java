package cn.com.scitc.consoleApp.db;

import cn.com.scitc.consoleApp.model.User;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.*;

class MyDatabaseTest {

    @org.junit.jupiter.api.Test
    void findById() {
        MyDatabase db = new MyDatabase();
        User user = db.findById(100);
        Assertions.assertEquals("张三", user.getName());

        user = db.findById(101);
        Assertions.assertEquals("李四", user.getName());

        user = db.findById(102);
        Assertions.assertEquals("王五", user.getName());
    }

    @org.junit.jupiter.api.Test
    void inser() {
        MyDatabase db = new MyDatabase();
        User user = new User();
        user.setId(110);
        user.setName("刘德华");
        user.setPwd("123456");

        db.insert(user);

        User findUser = db.findById(110);
        Assertions.assertEquals(user.getName(), findUser.getName());
    }

    @org.junit.jupiter.api.Test
    void update() {
        MyDatabase db = new MyDatabase();
        User user = new User();
        user.setId(110);
        user.setName("刘");
        user.setPwd("123");

//        db.insert(user);
        db.update(user);

        User findUser = db.findById(110);
        System.out.println(findUser);
        Assertions.assertEquals(user.getName(), findUser.getName());
    }


    @org.junit.jupiter.api.Test
    void delete() {
        MyDatabase db = new MyDatabase();
        db.delete(110);
        System.out.println(db.findById(110));
    }

    @org.junit.jupiter.api.Test
    void findAll() {
        MyDatabase db = new MyDatabase();
        System.out.println(db.findAll());
    }
}