package cn.com.scitc.consoleApp.data;

import org.junit.platform.commons.logging.Logger;
import org.junit.platform.commons.logging.LoggerFactory;

import java.io.*;

public class TxtTest {
    private static final Logger logger = LoggerFactory.getLogger(TxtTest.class);

    public static String readTxt(File file) throws IOException {
        String s = "";
        InputStreamReader in = new InputStreamReader(new FileInputStream(file), "UTF-8");
        BufferedReader br = new BufferedReader(in);
        StringBuffer content = new StringBuffer();
        while ((s = br.readLine()) != null) {
            content = content.append(s);
        }
        return content.toString();
    }

//    public static void main(String[] args) {
//        try {
////　　　　　　　　//通过绝对路径获取文件
//            String s1 = TxtTest.readTxt(new File("D:\\JavaWeb\\JavaEE\\test items\\consoleApp\\input\\init.txt"));
////            logger.info(s1);
//            System.out.println(s1);
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
