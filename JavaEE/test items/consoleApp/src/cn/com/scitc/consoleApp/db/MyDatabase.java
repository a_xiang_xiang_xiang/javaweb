package cn.com.scitc.consoleApp.db;

import cn.com.scitc.consoleApp.model.User;

import java.util.ArrayList;
import java.util.List;

public class MyDatabase {
    private List<User> data;

    public MyDatabase() {
        //c/c++ new的时候在分配空间,然后才能存东西，否则它是null, null是不能存东西的
        data = new ArrayList<>();
        loadData();
    }

    //数据写死了的
    private void loadData() {
        User user = new User();
        user.setId(100);
        user.setName("张三");
        user.setPwd("123456");
        data.add(user);

        user = new User();
        user.setId(101);
        user.setName("李四");
        user.setPwd("123456");
        data.add(user);

        user = new User();
        user.setId(102);
        user.setName("王五");
        user.setPwd("123456");
        data.add(user);
    }

    public User findById(Integer id) {
        for (User user : data) {
            if (user.getId().equals(id)) {
                return user;
            }
        }
        return null;
    }

    //
    public boolean insert(User user) {
        System.out.println(user);
        return data.add(user);
    }

    public boolean update(User user) {
        return data.add(user);
    }

    public boolean delete(Integer id) {
        return data.remove(id);
    }

    public List<User> findAll() {
        return data;
    }

}
