package com.xiang;

import com.xiang.dao.StudentDao;
import com.xiang.model.StudentEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class Webapp4ApplicationTest {
    @Autowired private StudentDao studentDao;


    @Test
    public  void  text(){
        Iterable<StudentEntity> all = studentDao.findAll();
        System.out.println(all);
    }


    @Test
    public  void  text2(){
        Optional<StudentEntity> byId = studentDao.findById(1);
        StudentEntity studentEntity = byId.get();
        String name = studentEntity.getName();
        System.out.println(name);
    }

}