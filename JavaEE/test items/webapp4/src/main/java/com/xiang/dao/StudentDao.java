package com.xiang.dao;

import com.xiang.model.StudentEntity;
import org.springframework.data.repository.CrudRepository;

public interface StudentDao extends CrudRepository<StudentEntity,Integer> {
}
