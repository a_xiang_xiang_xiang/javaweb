package com.xiang.pojo;

import com.xiang.dao.UserDao;
import com.xiang.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class SqlHelperTest {
    @Autowired
    private UserDao userDao;

    @Test
    public void testSelect() {
        userDao.findAll();
    }

    @Test
    public void findById() {
        String[] parameters = {"2"};
        userDao.findById(parameters);
    }

    @Test
    public void insert() {
//        final User user = new User("1","123","132","123");
//        userDao.insertUser(user);

    }

    @Test
    public void update() {
        userDao.update(12, "向向");
    }

}