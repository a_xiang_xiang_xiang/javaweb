package com.xiang.dao;



import com.xiang.model.User;
import com.xiang.pojo.SqlHelper;
import org.springframework.stereotype.Component;

@Component
public class UserDao{

    String keys = "";
    String value1 = "";
    String value2 = "";
    String value3 = "";
    String[] parameters = null;

    SqlHelper helper = new SqlHelper();

    public void findAll(){
        String sql = "select * from user";

        for (int i = 0; i < helper.executeQuery(sql,parameters).size(); i++) {
            //转换为object
            Object obj  = (Object) helper.executeQuery(sql,parameters).get(i);

            //将object 转换为 object[] 即可获取对应的值了。

            Object[] objarray = (Object[])obj;

            //获取对应的值

            keys =String.valueOf(objarray[0]);

            value1 = String.valueOf(objarray[1]);
            value2 = String.valueOf(objarray[2]);
            value3 = String.valueOf(objarray[3]);

            System.out.println(keys + "\t" + value1 + "\t" + value2 + "\t" + value3);

        }

    }
    public void findById(String[] parameters){
        String sql = "select * from user where id=?";

        for (int i = 0; i < helper.executeQuery(sql,parameters).size(); i++) {
            //转换为object
            Object obj  = (Object) helper.executeQuery(sql,parameters).get(i);

            //将object 转换为 object[] 即可获取对应的值了。

            Object[] objarray = (Object[])obj;

            //获取对应的值

            keys =String.valueOf(objarray[0]);

            value1 = String.valueOf(objarray[1]);
            value2 = String.valueOf(objarray[2]);
            value3 = String.valueOf(objarray[3]);

            System.out.println(keys + "\t" + value1 + "\t" + value2 + "\t" + value3);

        }
    }

    public void insertUser(User user){
        String sql = "insert into user (id,name,address,phoneNumber) value(?,?,?,?)";

        parameters = new String[]{String.valueOf(user.getId()), user.getName(), user.getAddress(), user.getPhonenumber()};

        helper.callPro1(sql,parameters);
        System.out.println("插入成功！！");
    }

    public void update(int id,String name){
        String sql = "update user set name = ? where id= ?";

        parameters = new String[]{name, String.valueOf(id)};
        helper.executeUpdate(sql,parameters);
        System.out.println("修改成功！！");
    }

}

