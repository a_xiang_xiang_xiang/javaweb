package com.xiang.util;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class MyToolTest {
    @Autowired
    private  MyTool myTool ;
//传统的方法；
    @Test

    public  void  test(){
        MyTool myTool = new MyTool();
        myTool.show();
    }

//    spring 里边的方法；
    @Test
    public  void  test2(){
        myTool.show();
    }

}