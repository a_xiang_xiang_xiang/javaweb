package com.xiang.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

class UserTest {
    @Test
    public  void  test(){
        /*
        * 用字符串构造User
        * "1，小向,2000-11-01"
        * */

        try {
            String str = "1,小向,2000-11-01";
            User user = new User(str);
            Assertions.assertEquals(1,user.getId());
            Assertions.assertEquals("小向",user.getName());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            date = sdf.parse("2000-11-01");
            Assertions.assertEquals(date,user.getBirthday());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

@Test
    public  void  test2(){
        User user = new User();
//        user.show;
    }

}