package com.xiang.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class User {
    private  Integer id;
    private  String name;
    private Date birthday;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }


    public User() {
        this.id = id;
        this.name = name;
        this.birthday = birthday;
    }

    public  User(String str){
//        这里补充代码
//        System.out.println(str);
        String[] split = str.split(",");
//        for (String list : split){
//            System.out.println(list);
//        }

        try {
            this.id = Integer.parseInt(split[0]);
            this.name = split[1];
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            this.birthday = sdf.parse(split[2]);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println(str);

    }

//    public
}
