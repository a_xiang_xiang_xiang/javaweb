package com.xiang.controller;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Component
@RequestMapping("/")
public class HomeController {
    @GetMapping("/xiang")
    public  String xiang(){
        return  "xiang";
    }
}
