package com.xiang.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/")
public class HomeController {
//    传统方法 注入
    @Autowired
    HttpServletRequest req;

//    spring 新方法
    @GetMapping("/xiang")
    public String  defaultPage(Integer id){
        System.out.println("（spring的方法获取）页面传入的对数id为："+id);

        String ID = req.getParameter("id");
        System.out.println("传统的方法获取："+ID);

        return "defaultPage";
    }

}
