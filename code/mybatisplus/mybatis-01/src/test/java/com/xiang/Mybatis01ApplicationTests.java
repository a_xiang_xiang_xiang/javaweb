package com.xiang;

import com.xiang.mapprt.UserMapper;
import com.xiang.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class Mybatis01ApplicationTests {

    @Autowired
    UserMapper userMapper;

    @Test
    void contextLoads() {
        List<User> users= userMapper.selectList(null);
        users.forEach(System.out::println);
    }

}
