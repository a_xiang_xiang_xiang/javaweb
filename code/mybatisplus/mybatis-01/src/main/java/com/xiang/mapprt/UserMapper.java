package com.xiang.mapprt;

import com.xiang.pojo.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper {
    List<User> selectList(Object o);
}
