<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/5/20
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>今日校园---更新用户信息</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<c:url value="/css/manager.css"/>">
    <link rel="stylesheet" href="<c:url value="/webjars/bootstrap/4.6.0/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/webjars/font-awesome/4.7.0/css/font-awesome.min.css"/>">
    <script src="<c:url value="/webjars/jquery/3.6.0/dist/jquery.min.js"/>"></script>
    <script src=" <c:url value="/webjars/bootstrap/4.6.0/js/bootstrap.min.js"/>"></script>
</head>
<body>
<div class="container">
    <h1>更新用户</h1>
    <c:if test="${sessionScope.msg != null}">

        <div class="alert alert-primary" role="alert">
            <span><c:out value="${sessionScope.msg}"/></span>
            <c:remove var="msg" scope="session"/>
        </div>
    </c:if>

    <%--    <form  action="<c:url value="/manager/user/creaate"/ class="form-signin needs-validation" novalidate>--%>
    <form method="post" action="<c:url value="/manager/user/update"/> " class=" needs-validation" novalidate>
        <%--    <form method="post" action="<c:url value="/manager/user/create"/> " class=" needs-validation" novalidate>--%>


        <div class="form-group" style="display: none">
            <label for="loginid">编号</label>
            <input type="hidden" class="form-control" id="id" name="id" value="${requestScope.manager.id}">
        </div>

        <div class="form-group">
            <label for="loginid">登录名</label>
            <input type="text" class="form-control" id="loginid" name="loginId" value="${requestScope.manager.loginid}"
                   aria-describedby="emailHelp" placeholder="请输入登录名" required minlength="4" maxlength="10"
                   pattern="\w{4,10}">
            <small class="form-text text-muted">登录名唯一</small>
        </div>
        <div class="form-group">
            <label for="realname">真实姓名</label>
            <input type="text" class="form-control" id="realname" name="realName"
                   value="${requestScope.manager.realname}" placeholder="真实姓名" required pattern="[\u4e00-\u9fa5]{2,5}">
            <small class="form-text text-muted">真实名唯一</small>
        </div>
        <%--        <div class="form-group">--%>
        <%--            <label for="pwd1">密码</label>--%>
        <%--            <input type="text" class="form-control" id="pwd1" name="pwd1" value="${requestScope.manager.pwd}" placeholder="密码" required minlength="6" maxlength="20">--%>
        <%--        </div>--%>
        <%--        <div class="form-group">--%>
        <%--            <label for="pwd2">确定密码</label>--%>
        <%--            <input type="text" class="form-control" id="pwd2" name="pwd2" value="${requestScope.manager.pwd}" placeholder="确定密码" required minlength="6" maxlength="20">--%>
        <%--        </div>--%>

        <div class="form-group">
            <label for="logincount">登录次数</label>
            <input type="number" class="form-control" id="logincount" value="${requestScope.manager.logincount}"
                   name="loginCount" placeholder="登录次数" value="0">
        </div>
        <div class="form-group">
            <label for="lastlogindt">最后登录时间</label>
            <input type="datetime-local" class="form-control" id="lastlogindt"
                   value="<fmt:formatDate value="${requestScope.manager.lastlogindt}" pattern="yyyy-MM-dd'T'HH:mm"/>"
                   name="lastLoginDt" placeholder="最后登录时间">

        </div>

        <button type="submit" class="btn btn-pr
        imary">提交</button>
    </form>


    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function () {
            'use strict';
            window.addEventListener('load', function () {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>

    <%--            private Integer id;--%>

    <%--            private String loginid;--%>

    <%--            private String realname;--%>

    <%--            private String pwd;--%>

    <%--            private Integer logincount;--%>

    <%--            private Date lastlogindt;&ndash;%&gt;--%>

    <%--    </table>--%>
</div>
</body>
</html>
