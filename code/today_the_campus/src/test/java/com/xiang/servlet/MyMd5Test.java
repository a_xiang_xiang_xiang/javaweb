package com.xiang.servlet;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyMd5Test {

    @Test
    void stringToMD5() {
        String str = "123456";
        final String toMD5 = MyMd5.stringToMD5(str);
        Assertions.assertEquals("e10adc3949ba59abbe56e057f20f883e".toLowerCase(), toMD5);
        System.out.println(toMD5);
//        .toUpperCase() 转大写
//        .toLowerCase() 转小写

    }

    @Test
    void stringToMD5_2() {

        final String toMD5 = MyMd5.signPwd("123456");
        Assertions.assertEquals("c59545a69f963aeb17674a103de705ff",toMD5);
        System.out.println(toMD5);
    }
}