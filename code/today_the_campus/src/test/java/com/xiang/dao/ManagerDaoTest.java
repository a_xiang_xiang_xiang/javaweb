package com.xiang.dao;

import com.xiang.model.Manager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class ManagerDaoTest {

    @Test
    void selectByPrimaryKey() {
    }

    @Test
    void findByLoginId() {
        ManagerDao dao = new ManagerDao();
        Manager byLoginId = dao.findByLoginId("xiang");
        Assertions.assertEquals("小向",byLoginId.getRealname());
        System.out.println(byLoginId.getRealname());
        System.out.println(byLoginId);
    }

    @Test
    void  SimpleDateFormat(){
        try {
        String dt ="2021-05-19T21:27";
        dt=dt.replace("T"," ");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-ddHH:mm");

            Date lastLoginDt = format.parse(dt);
            System.out.println(lastLoginDt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    void  updateByPrimaryKey(){
        try {
        ManagerDao managerDao = new ManagerDao();

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date parse = simpleDateFormat.parse("2020-02-28");
//        managerDao.updateByPrimaryKey(new Manager(42,"admin45","管理","123456",22,parse));
            Manager manager = new Manager();
            manager.setId(42);
            manager.setLoginid("admin46");
            managerDao.updateByPrimaryKey(manager);


        } catch (ParseException e) {
            e.printStackTrace();
        }
//        managerDao.setBirthday(parse);

    }
}