package com.xiang.dao;

import com.xiang.mapper.StudentMapper;
import com.xiang.model.Student;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

class StudentDaoTest {

    @Test
    void selectByPrimaryKey() {
        StudentDao studentDao = new StudentDao();
        Student student = studentDao.selectByPrimaryKey(1);
        System.out.println(student);
    }


    @Test
    void selectAll(){
        SqlSession getsession = MybatisUtil.getsession();
        StudentMapper mapper = getsession.getMapper(StudentMapper.class);
        List<Student> students = mapper.selectAll();
        for (Student student:students){
            System.out.println(student);
        }
    }

    @Test
    void  selectAll2(){
        StudentDao studentDao = new StudentDao();
        ArrayList arrayList = studentDao.selectAll();
        for (Object students:arrayList){
            System.out.println(students);
        }
    }


    @Test
    void  insert() throws ParseException {
        StudentDao studentDao = new StudentDao();
        Student student = new Student();
        student.setStudentid("11111112");
        student.setName("向贵敏");
        student.setAddress("四川广元");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date parse = simpleDateFormat.parse("2020-02-28");
        student.setBirthday(parse);
        student.setMobile("13419171453");
        student.setGender(1);
        student.setPwd("123456");
        int res = studentDao.insert(student);
        if (res >0){
            System.out.println("插入成功");
        }
    }

    @Test
    void  insert2(){
        StudentDao studentDao = new StudentDao();
        Map<String, Object> map = new HashMap<>();
//        map.put("")
        int insert = studentDao.insert((Student) map);
        if (insert>0){
            System.out.println("插入成功");
        }
    }

    @Test
    void  deleteByPrimaryKey(){
        StudentDao studentDao = new StudentDao();
        int delete = studentDao.deleteByPrimaryKey(19);
        if(delete >0){
            System.out.println("删除成功");
        }
    }

    @Test
    void  updateByPrimaryKey() throws ParseException {
        StudentDao studentDao = new StudentDao();
        Student student = new Student();
        student.setStudentid("11111111");
        student.setName("向tmk");
        student.setAddress("四川广元");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date parse = simpleDateFormat.parse("2020-02-28");
        student.setBirthday(parse);
        student.setMobile("13419171453");
        student.setGender(0);
        student.setPwd("123456");
        int res =  studentDao.updateByPrimaryKey(student);
        if(res>0){
            System.out.println("更新成功");
        }
    }


    @Test
    void te(){
            double d = 100.675;
            double f = -8.9;

            System.out.println(Math.floor(d));
            System.out.println(Math.floor(f));

            System.out.println(Math.ceil(d));
            System.out.println(Math.ceil(f));

        String str = "";
        System.out.println(str.split(",").length);

//        Iterator
        }

        @Test
    void  test(){
            System.out.println("hihi");
        }


    }
