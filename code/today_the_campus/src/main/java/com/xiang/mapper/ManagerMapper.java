package com.xiang.mapper;

import com.xiang.model.Manager;
import java.util.List;

public interface ManagerMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Manager record);

    Manager selectByPrimaryKey(Integer id);

    List<Manager> selectAll();

    int updateByPrimaryKey(Manager record);

    Manager findByLoginId(String loginId);

    Manager findById(Integer id);

    int updateByLoginId(Manager manager);

    Integer updateByKey(Integer id);
}