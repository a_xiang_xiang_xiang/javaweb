package com.xiang.mapper;

import com.xiang.model.Signin;
import java.util.List;

public interface SigninMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Signin record);

    Signin selectByPrimaryKey(Integer id);

    List<Signin> selectAll();

    int updateByPrimaryKey(Signin record);
}