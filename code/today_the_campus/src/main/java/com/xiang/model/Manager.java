package com.xiang.model;

import java.io.Serializable;
import java.util.Date;

public class Manager implements Serializable {
    private Integer id;

    private String loginid;

    private String realname;

    private String pwd;

    private Integer logincount;

    private Date lastlogindt;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Integer getLogincount() {
        return logincount;
    }

    public void setLogincount(Integer logincount) {
        this.logincount = logincount;
    }

    public Date getLastlogindt() {
        return lastlogindt;
    }

    public void setLastlogindt(Date lastlogindt) {
        this.lastlogindt = lastlogindt;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", loginid=").append(loginid);
        sb.append(", realname=").append(realname);
        sb.append(", pwd=").append(pwd);
        sb.append(", logincount=").append(logincount);
        sb.append(", lastlogindt=").append(lastlogindt);
        sb.append("]");
        return sb.toString();
    }
}