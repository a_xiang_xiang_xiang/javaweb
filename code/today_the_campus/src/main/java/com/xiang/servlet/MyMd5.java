package com.xiang.servlet;

import com.xiang.dao.ManagerDao;
import com.xiang.model.Manager;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MyMd5 {
    public  static  String preSeed = "@$%#@%";
    public  static  String extSeed = "?><?:";

    public static String stringToMD5(String plainText) {
        byte[] secretBytes = null;
        try {
            secretBytes = MessageDigest.getInstance("md5").digest(
                    plainText.getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("没有这个md5算法！");
        }
        String md5code = new BigInteger(1, secretBytes).toString(16);
        for (int i = 0; i < 32 - md5code.length(); i++) {
            md5code = "0" + md5code;
        }
        return md5code;
    }

//    加密码 前缀  后缀；
    public  static String signPwd(String plainText){
        return  stringToMD5(preSeed + plainText + extSeed);
    }

//    密码认证
    public static  boolean validateManager(String loginId,String plainText){
//        从页面来
        final ManagerDao dao = new ManagerDao();
        final Manager manager = dao.findByLoginId(loginId);
        if (manager !=  null && manager.getPwd() != null && MyMd5.signPwd(plainText).equals(manager.getPwd())){
            return  true;
        }else {
            return  false;
        }
    }
}
