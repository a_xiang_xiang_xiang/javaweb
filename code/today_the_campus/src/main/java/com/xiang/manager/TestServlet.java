package com.xiang.manager;

import com.xiang.dao.ManagerDao;
import com.xiang.model.Manager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/manager/test")
public class TestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ManagerDao dao = new ManagerDao();
        final Manager manager = dao.findByLoginId("xiang");
        req.getSession().setAttribute("currentUser",manager);
        resp.sendRedirect("./home");
    }
}
