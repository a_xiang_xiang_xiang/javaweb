package com.xiang.manager.user;

import com.xiang.dao.ManagerDao;
import com.xiang.model.Manager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/manager/user/edit")
public class UserEditServlet extends HttpServlet {
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        req.getRequestDispatcher("WEB-INF/manager/user/edit.jsp").forward(req, resp);
//
//    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String id = req.getParameter("id");
            System.out.println("id:" + id);
            ManagerDao dao = new ManagerDao();
            final Manager manager = dao.findById(Integer.parseInt(id));
            System.out.println("manager:"+ manager);
            req.setAttribute("manager", manager);
            req.getRequestDispatcher("/WEB-INF/manager/user/edit.jsp").forward(req,resp);
//            resp.sendRedirect("list");
            return;
        }
        catch (Exception e){
            e.printStackTrace();
//            System.out.println("update failure");
//            req.getSession().setAttribute("msg", "更新失败");
            resp.sendRedirect("list");
        }


//        String loginId = req.getParameter("loginid");
//        System.out.println("loginId ："+ loginId);
//        req.setAttribute("manager",new ManagerDao().findByLoginId(loginId));

//        String id = req.getParameter("id");
//        System.out.println("id ："+ id);
//        req.setAttribute("manager",new ManagerDao().updateByKey(Integer.parseInt(id)));

//        Integer id = Integer.valueOf(req.getParameter("id"));
//        System.out.println("id ：" + id);
//        req.setAttribute("manager", new ManagerDao().updateByPrimaryKey(id));

//        int id = Integer.parseInt(req.getParameter("id"));
//        System.out.println("id:"+id);
//        req.setAttribute("manager",new ManagerDao().findById(id));

//        int id = Integer.parseInt(req.getParameter("id"));
//        System.out.println(id);
//        req.setAttribute("manager",new ManagerDao().findById(id));
//
//        req.getRequestDispatcher("/WEB-INF/manager/user/edit.jsp").forward(req,resp);
    }
}
