package com.xiang.manager.user;

import com.xiang.dao.ManagerDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/manager/user/delete")
public class UserDeleteServlet  extends HttpServlet {
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        resp.sendRedirect("list");
//    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {


        String id = req.getParameter("id");
        System.out.println("id:"+id);
        ManagerDao dao = new ManagerDao();
        dao.deleteByPrimaryKey(Integer.parseInt(id));
        System.out.println("delete success");
        req.getSession().setAttribute("msg","删除成功");
        resp.sendRedirect("list");
        return;
        }
        catch (Exception e){
            System.out.println("delete failure");
            req.getSession().setAttribute("msg","删除失败");
            resp.sendRedirect("list");
        }
//        req.setAttribute("manager",new ManagerDao().findByLoginId(id));
//        req.getRequestDispatcher("/WEB-INF/manager/user/edit.jsp").forward(req,resp);
    }
}
