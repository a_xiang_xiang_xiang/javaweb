package com.xiang.manager.user;

import com.xiang.dao.ManagerDao;
import com.xiang.model.Manager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/manager/user/list")
public class UserListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ManagerDao dao = new ManagerDao();
        List<Manager> managers = dao.selectAll();
        req.setAttribute("managers",managers);

        String msg = (String)req.getSession().getAttribute("msg");
        System.out.println("msg: " + msg);

        req.getRequestDispatcher("/WEB-INF/manager/user/list.jsp").forward(req, resp);

    }
}
