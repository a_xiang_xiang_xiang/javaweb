package com.xiang.manager;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = "/manager/*")
public class ManagerLoginFilter implements Filter {
    public void init(FilterConfig config) throws ServletException {
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
//        强转；
        HttpServletRequest request=(HttpServletRequest)req;
        HttpServletResponse response=(HttpServletResponse)resp;

        String url = request.getRequestURI();
        System.out.println("当前访问的是"+ url);

        final HttpSession session = request.getSession();
        if (session.getAttribute("currentUser") == null){ //没有登录
            //进行过滤
            if (url.contains("/manager/login") || url.contains("/manager/checkLogin") || url.contains("/manager/test")){
                //放开
            }else {
                //过滤
                response.sendRedirect("/today/manager/login");
                return;
            }
        }else { //已经登录
            //完全放开
        }

        chain.doFilter(request, response);

    }
}
