package com.xiang.manager;

import com.xiang.dao.ManagerDao;
import com.xiang.model.Manager;
import com.xiang.servlet.MyMd5;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

@WebServlet(urlPatterns = "/manager/checkLogin")
public class CheckLoginServlet  extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String loginId = req.getParameter("loginId");
        String pwd = req.getParameter("pwd");

        System.out.println("用户名："+ loginId);
        System.out.println("密码："+ pwd);

        final boolean res = MyMd5.validateManager(loginId, pwd);
        System.out.println(res);

        if (res){
            final HttpSession session = req.getSession();
            ManagerDao dao = new ManagerDao();
            final Manager manager = dao.findByLoginId(loginId);
//            处理登录 次数
            if (manager.getLogincount() == null){
                manager.setLogincount(1);
            }else {
                manager.setLogincount(manager.getLogincount()+1);
            }

//            登录 时间
            Calendar calendar = Calendar.getInstance();
            Date time = calendar.getTime();
            manager.setLastlogindt(time);
//            保存时间
            dao.updateByPrimaryKey(manager);

            session.setAttribute("currentUser",manager);

//            resp.sendRedirect("home");
//            跳转
            resp.sendRedirect("./home");
        }else {
//            req.getRequestDispatcher("/WEB-INF/manager/login.jsp").forward(req,resp);
            req.getSession().setAttribute("loginMsg","用户名、密码错误");
            resp.sendRedirect("login");
        }

//        if()




//        if (pwd.equals("123456")){
//            System.out.println("true;");
//        绑页面
//            req.getRequestDispatcher("/WEB-INF/manager/main.jsp").forward(req,resp);
//        }
//        else {
//            System.out.println("error");
//
//        }

//        home
//        用户提示，登录 报错
    }


}
