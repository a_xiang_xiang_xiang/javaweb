package com.xiang.manager.user;

import com.xiang.dao.ManagerDao;
import com.xiang.model.Manager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

//@WebServlet(urlPatterns = "/manager/user/update")
public class UserUpdateServlet2 extends HttpServlet {

    //    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        String loginId = req.getParameter("loginid");
//        System.out.println(loginId);
//        req.setAttribute("manager",new ManagerDao().findByLoginId(loginId));
//        req.getRequestDispatcher("/WEB-INF/manager/user/edit.jsp").forward(req,resp);
//
//    }

        @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//                先拿参数
            try {
//            有问题，没有保存页面状态；
                if (!validation(req)) {
                    System.out.println("验证不通过");
                }else {
                    System.out.println("验证通过");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

//        验证
    private boolean validation(HttpServletRequest req) {
        String loginId = req.getParameter("loginId");
        if (loginId == null || loginId.length() < 4 || loginId.length() > 10) {
            System.out.println("登录名不通过");
            System.out.println("登录名"+loginId);
            return false;
        }
        String realName = req.getParameter("realName");
        if (realName == null || realName.length() < 2 || realName.length() > 5) {
            System.out.println("姓名不通过");
            System.out.println("姓名"+realName);
            return false;
        }
//        String pwd1 = req.getParameter("pwd1");
//        if (pwd1 == null || pwd1.length() < 6 || pwd1.length() > 20) {
//            System.out.println("密码不通过");
//            System.out.println(pwd1);
//            return false;
//        }
//        String pwd2 = req.getParameter("pwd2");
//        if (!pwd2.equals(pwd1)) {
//            System.out.println(pwd2);
//            return false;
//        }
        Integer.getInteger(req.getParameter("loginCount"));
        return true;
    }

}

