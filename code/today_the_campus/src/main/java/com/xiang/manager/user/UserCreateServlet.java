package com.xiang.manager.user;

import com.xiang.dao.ManagerDao;
import com.xiang.model.Manager;
import com.xiang.servlet.MyMd5;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(urlPatterns = "/manager/user/create")
public class UserCreateServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Manager manager = new Manager();

        //        先拿参数
        try {
//            有问题，没有保存页面状态；
            if (!validation(req)) {
                resp.sendRedirect("new");
                return;
            }else {
//                req.getRequestDispatcher("/WEB-INF/manager/user/list.jsp").forward(req,resp);
//                resp.sendRedirect("list");
            }
//            String id = req.getParameter("id");
            String loginId = req.getParameter("loginId");
            String realName = req.getParameter("realName");
            String pwd1 = req.getParameter("pwd1");
            String pwd2 = req.getParameter("pwd2");
            Integer loginCount = Integer.parseInt(req.getParameter("loginCount")); //转成整数；
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date lastLoginDt = format.parse(req.getParameter("lastLoginDt").replace("T" ," "));
//            Date lastLoginDt = format.parse(req.getParameter("lastLoginDt"));


            manager.setLoginid(loginId);
            manager.setRealname(realName);
            manager.setPwd(pwd1);
//            manager.setPwd(MyMd5.signPwd(pwd1));
            manager.setLogincount(loginCount);
            manager.setLastlogindt(lastLoginDt);

            ManagerDao dao = new ManagerDao();
            dao.insert(manager);
            System.out.println("insert success");
            req.getSession().setAttribute("msg","新增成功");
            req.getSession().getAttribute("msg");
            resp.sendRedirect("list");
            return;
        } catch (Exception e){
            System.out.println("insert error");
            req.getSession().setAttribute("msg","新增失败");
            req.getSession().setAttribute("manager", manager);
            resp.sendRedirect("new");
        }


    }

    //    验证
    private boolean validation(HttpServletRequest req) {
        String loginId = req.getParameter("loginId");
        if (loginId == null || loginId.length() < 4 || loginId.length() > 10) {
            System.out.println("登录名不通过");
            System.out.println("登录名"+loginId);
            return false;
        }


        String realName = req.getParameter("realName");
        if (realName == null || realName.length() < 2 || realName.length() > 5) {
            System.out.println("姓名不通过");
            System.out.println("姓名"+realName);
            return false;
        }
        String pwd1 = req.getParameter("pwd1");
        if (pwd1 == null || pwd1.length() < 6 || pwd1.length() > 20) {
            System.out.println("密码不通过");
            System.out.println(pwd1);
            return false;
        }
        String pwd2 = req.getParameter("pwd2");
        if (!pwd2.equals(pwd1)) {
            System.out.println(pwd2);
            return false;
        }
        Integer.getInteger(req.getParameter("loginCount"));
        return true;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/manager/user/list.jsp").forward(req,resp);
    }
}

