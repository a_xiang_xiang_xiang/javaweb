package com.xiang.dao;

import com.xiang.mapper.StudentMapper;
import com.xiang.model.Student;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.List;

public class StudentDao {
    public Student selectByPrimaryKey(Integer id){
        SqlSession getsession = MybatisUtil.getsession();
        StudentMapper mapper = getsession.getMapper(StudentMapper.class);
        Student student = mapper.selectByPrimaryKey(id);
        return student;
    }

    public ArrayList selectAll(){
        SqlSession getsession = MybatisUtil.getsession();
        StudentMapper mapper = getsession.getMapper(StudentMapper.class);
        List<Student> students = mapper.selectAll();
        return (ArrayList) students;

    }

    public int insert(Student student){
        SqlSession getsession = MybatisUtil.getsession();
        StudentMapper mapper = getsession.getMapper(StudentMapper.class);
        mapper.insert(student);
//        getsession.commit();
//        getsession.close();
        return 0;
    }

    public int deleteByPrimaryKey(Integer id){
        SqlSession getsession = MybatisUtil.getsession();
        StudentMapper mapper = getsession.getMapper(StudentMapper.class);
        int delete = mapper.deleteByPrimaryKey(id);
        getsession.commit();
        getsession.close();
        return delete;
    }

    public int updateByPrimaryKey(Student student){
        SqlSession getsession = MybatisUtil.getsession();
        StudentMapper mapper = getsession.getMapper(StudentMapper.class);
        mapper.updateByPrimaryKey(student);
        getsession.commit();
        getsession.close();
        return 0;
    }

}
