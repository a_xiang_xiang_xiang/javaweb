package com.xiang.dao;


import com.xiang.mapper.ManagerMapper;
import com.xiang.model.Manager;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class ManagerDao {
//    public Manager selectByPrimaryKey(Integer id){
//        try (SqlSession session = MybatisUtil.getsession()){
//            final ManagerMapper mapper = session.getMapper(ManagerMapper.class);
//            return mapper.selectByPrimaryKey(id);
//
//        }
//    }


    public Manager selectByPrimaryKey(Integer id){
        SqlSession getsession = MybatisUtil.getsession();
        ManagerMapper mapper = getsession.getMapper(ManagerMapper.class);
//        getsession.close();
        return  mapper.selectByPrimaryKey(id);
    }

    public Manager findByLoginId(String loginId){
        SqlSession getsession = MybatisUtil.getsession();
        ManagerMapper mapper = getsession.getMapper(ManagerMapper.class);
//        getsession.close();
        return  mapper.findByLoginId(loginId);
    }

    /*查询id的方法*/
    public Manager findById(Integer id){
        SqlSession getsession = MybatisUtil.getsession();
        ManagerMapper mapper = getsession.getMapper(ManagerMapper.class);
        return mapper.findById(id);
    }



    public List<Manager>  selectAll(){
        SqlSession getsession = MybatisUtil.getsession();
        ManagerMapper mapper = getsession.getMapper(ManagerMapper.class);
//        getsession.close();
        return mapper.selectAll();
    }


//    public int updateByPrimaryKey(String id){
//        SqlSession getsession = MybatisUtil.getsession();
//        ManagerMapper mapper = getsession.getMapper(ManagerMapper.class);
//        final int result =  mapper.updateByPrimaryKey(id);
//        getsession.commit();
////        getsession.close();
//        return result;
//    }

    public int updateByPrimaryKey(Manager manager){
        SqlSession getsession = MybatisUtil.getsession();
        ManagerMapper mapper = getsession.getMapper(ManagerMapper.class);
        final int result =  mapper.updateByPrimaryKey(manager);
        getsession.commit();
//        getsession.close();
        return result;
    }

    public int updateByLoginId(Manager manager){
        SqlSession getsession = MybatisUtil.getsession();
        ManagerMapper mapper = getsession.getMapper(ManagerMapper.class);
        final int result =  mapper.updateByLoginId(manager);
        getsession.commit();
//        getsession.close();
        return result;
    }

    public int deleteByPrimaryKey(Integer id){
        SqlSession getsession = MybatisUtil.getsession();
        ManagerMapper mapper = getsession.getMapper(ManagerMapper.class);
        Integer res =  mapper.deleteByPrimaryKey(id);
//        getsession.close();
        getsession.commit();
        return res;
    }
    public int updateByKey(Integer id){
        SqlSession getsession = MybatisUtil.getsession();
        ManagerMapper mapper = getsession.getMapper(ManagerMapper.class);
        Integer res =  mapper.updateByKey(id);
//        getsession.close();
        getsession.commit();
        return res;
    }

//    public int updateByLoginId(Manager id) {
//        SqlSession getsession = MybatisUtil.getsession();
//        ManagerMapper mapper = getsession.getMapper(ManagerMapper.class);
//        final int result = mapper.updateByLoginId(id);
//        getsession.commit();
////        getsession.close();
//        return result;
//    }
    public Integer insert(Manager manager){
        SqlSession getsession = MybatisUtil.getsession();
        final ManagerMapper mapper = getsession.getMapper(ManagerMapper.class);
        Integer res = mapper.insert(manager);
        getsession.commit();
//        getsession.close();
        return res;
    }

}
