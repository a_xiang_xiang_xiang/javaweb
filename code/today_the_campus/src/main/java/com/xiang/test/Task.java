package com.xiang.test;

public class Task {
    private static int a = 1;

    public static int getA() {

        return a;

    }

    public static void changeA() {

        a++;

    }
}

 class Test{

    public static void main(String[] args) {

        Task s1 = new Task();

        Task s2 = new Task();

        s1.changeA();

        s2.changeA();

        System.out.println(s1.getA()+","+s2.getA());

    }

}
