<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/5/7
  Time: 8:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>今日校园---用户端首页</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<c:url value="/css/manager.css"/>">
    <link rel="stylesheet" href="<c:url value="/webjars/bootstrap/4.6.0/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/webjars/font-awesome/4.7.0/css/font-awesome.min.css"/>">
    <script src="<c:url value="/webjars/jquery/3.6.0/dist/jquery.min.js"/>"></script>
    <script src=" <c:url value="/webjars/bootstrap/4.6.0/js/bootstrap.min.js"/>"></script>
    <%--    <style>--%>
    <%--        /* 表格样式 */--%>

    <%--        .table>tbody>tr>td {--%>
    <%--            border: 0px;--%>
    <%--            text-align: center;--%>
    <%--        }--%>

    <%--        .bootstrap-table .table thead>tr>th {--%>
    <%--            text-align: center;--%>
    <%--        }--%>
    <%--        .bootstrap-table .table tr>td {--%>
    <%--            text-align: center;--%>
    <%--        }--%>

    <%--        .table thead {--%>
    <%--            background: #ebeaea;--%>
    <%--        }--%>
    <%--    </style>--%>

</head>
<body>

<jsp:include page="/WEB-INF/manager/include/nav.jsp"/>

<div class="container">
    <h1>用户管理</h1>
    <c:if test="${sessionScope.msg != null}">
        <div class="alert alert-primary" role="alert">
            <span><c:out value="${sessionScope.msg}"/></span>
            <c:remove var="msg" scope="session"/>
        </div>
    </c:if>
    <table class="table table-hover">
        <tr>
            <th scope="col">编号</th>
            <th scope="col">登录名</th>
            <th scope="col">真实姓名</th>
            <th scope="col">密码</th>
            <th scope="col">登录次数</th>
            <th scope="col">最后登录时间(美国)</th>
            <th scope="col">最后登录时间（中国）</th>
            <th scope="col">操作</th>

        </tr>

        <c:forEach items="${managers}" var="model">
            <tr>
                <td><c:out value="${model.id}"/></td>
                <td><c:out value="${model.loginid}"/></td>
                <td><c:out value="${model.realname}"/></td>
                <td><c:out value="******"/></td>
                <td><c:out value="${model.logincount}"/></td>
                <td><c:out value="${model.lastlogindt}"/></td>
                <td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${model.lastlogindt}"/></td>
                <td>
                    <span>
<%--                        <a href="<c:url value="/manager/user/update"/>?loginid=${model.loginid}">更新</a>--%>
                        <a href="<c:url value="/manager/user/edit"/>?id=${model.id}">更新</a>
                    </span>|
                    <span>
<%--                        confirm 函数 返回布尔--%>
                        <a href="<c:url value="/manager/user/delete"/>?id=${model.id}" onclick="return confirm('确定删除吗？')">删除</a>
<%--                        <a href="<c:url value="/manager/user/delete"/>“  onclick="return confirm('确定删除吗？')">删除</a>--%>
                    </span>
                </td>
            </tr>
        </c:forEach>
        <%--
            private Integer id;

            private String loginid;

            private String realname;

            private String pwd;

            private Integer logincount;

            private Date lastlogindt;--%>

    </table>
</div>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/docs/4.2/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="/docs/4.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-zDnhMsjVZfS3hiP7oCBRmfjkQC4fzxVxFhBx8Hkz2aZX8gEvA/jsP3eXRCvzTofP"
        crossorigin="anonymous"></script>

</body>
</html>

<%--<update id="updateByLoginId" parameterType="com.xiang.model.Manager">--%>
<%--    update manager--%>
<%--    set--%>
<%--    realName = #{realname,jdbcType=VARCHAR},--%>
<%--    pwd = #{pwd,jdbcType=VARCHAR},--%>
<%--    loginCount = #{logincount,jdbcType=INTEGER},--%>
<%--    lastLoginDt = #{lastlogindt,jdbcType=TIMESTAMP}--%>
<%--    where loginid = #{loginid,jdbcType=VARCHAR}--%>
<%--</update>--%>