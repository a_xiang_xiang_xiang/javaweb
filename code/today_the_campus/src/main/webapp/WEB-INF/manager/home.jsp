<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/5/7
  Time: 8:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>今日校园---管理端首页</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<c:url value="/css/manager.css"/>">
    <link rel="stylesheet" href="<c:url value="/webjars/bootstrap/4.6.0/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/webjars/font-awesome/4.7.0/css/font-awesome.min.css"/>">
    <script src="<c:url value="/webjars/jquery/3.6.0/dist/jquery.min.js"/>"></script>
    <script src=" <c:url value="/webjars/bootstrap/4.6.0/js/bootstrap.min.js"/>"></script>
</head>
<body>


<jsp:include page="/WEB-INF/manager/include/nav.jsp"/>

<%--<main role="main" class="container">--%>

<%--    <div class="starter-template">--%>
<%--        <h1>Bootstrap starter template</h1>--%>
<%--        <p class="lead">Use this document as a way to quickly start any new project.<br> All you get is this text and a mostly barebones HTML document.</p>--%>
<%--    </div>--%>

<%--</main><!-- /.container -->--%>
<%--<div class="container">--%>

<%--</div>--%>


<div class="container" >
    <div class="row">
        <div class="col">
            <div class="input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i></span>
                <input class="form-control" type="text" placeholder="您的邮箱地址">
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-key fa-fw" aria-hidden="true"></i></span>
                <input class="form-control" type="password" placeholder="请输入密码">
            </div>
        </div>
        <div class="col-10">
<%--            <div style="width:1000px; height: 600px ">--%>
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="8"></li>

                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="<c:url value="/images/img/1.jpg"/>" alt="/images/img/1.jpg">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<c:url value="/images/img/2.jpg"/>" alt="/images/img/2.jpg">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<c:url value="/images/img/3.jpg"/>" alt="/images/img/3.jpg">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<c:url value="/images/img/4.jpg"/>" alt="/images/img/3.jpg">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<c:url value="/images/img/5.jpg"/>" alt="/images/img/3.jpg">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<c:url value="/images/img/6.jpg"/>" alt="/images/img/3.jpg">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<c:url value="/images/img/7.jpg"/>" alt="/images/img/3.jpg">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<c:url value="/images/img/8.jpg"/>" alt="/images/img/3.jpg">
                        </div>

                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
<%--        </div>--%>
        <div class="col">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
            </div>
        </div>
    </div>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/docs/4.2/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="/docs/4.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-zDnhMsjVZfS3hiP7oCBRmfjkQC4fzxVxFhBx8Hkz2aZX8gEvA/jsP3eXRCvzTofP"
        crossorigin="anonymous"></script>

</body>
</html>
