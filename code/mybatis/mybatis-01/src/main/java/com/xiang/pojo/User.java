package com.xiang.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * @author Xiang
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User {
    private  int id;
    private  String name;
    private  String mobile;
    private Date birth;

}
