package com.xiang.mappet;

import com.xiang.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

public class MyTest {
    @Test
    public  void  myTest(){
        System.out.println("小向");


        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        try (SqlSession session = sqlSessionFactory.openSession()) {
            User user = (User) session.selectOne("com.xiang.mapper.UserMapper.selectUser", 1);
            System.out.println("name:" + user.getName());

        }

    }

    @Test
    public void  test(){
        for (int i = 0; i <= 20; i+=2) {
            System.out.println(i);

        }
    }

    @Test
    public  void  test2(){
        int j=1;
        int res=0;
        do {
            res+=j;
            j++;
        }while (j<=100);
        System.out.println(j);
        System.out.println(res);
    }
}
