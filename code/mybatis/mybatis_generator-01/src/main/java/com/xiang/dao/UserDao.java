package com.xiang.dao;

import com.xiang.mapper.UserMapper;
import com.xiang.model.User;
import org.apache.ibatis.session.SqlSession;

public class UserDao {
    public User selectByPrimaryKey(Integer id){
        SqlSession getsession = MybatisUtil.getsession();
        UserMapper mapper = getsession.getMapper(UserMapper.class);
        User user = mapper.selectByPrimaryKey(id);
        return  user;
    }
//    public int insert(User user){
//        SqlSession getsession = MybatisUtil.getsession();
//        UserMapper mapper = getsession.getMapper(UserMapper.class);
//        int insert = mapper.insert(new User(3, "小不点", "131313131", "20200213"));
//
//    }

    public int deleteByPrimaryKey(Integer id){
        SqlSession getsession = MybatisUtil.getsession();
        UserMapper mapper = getsession.getMapper(UserMapper.class);
        int i = mapper.deleteByPrimaryKey(id);
        return i;
    }

//    public int updateByPrimaryKey(){
//        SqlSession getsession = MybatisUtil.getsession();
//        UserMapper mapper = getsession.getMapper(UserMapper.class);
//        int update = mapper.updateByPrimaryKey(new User(3, "小不点", "131313131", "2020-02-13"));
//        return update;
//    }


}
