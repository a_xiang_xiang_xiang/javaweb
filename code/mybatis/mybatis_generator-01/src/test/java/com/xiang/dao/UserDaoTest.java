package com.xiang.dao;

import com.xiang.mapper.UserMapper;
import com.xiang.model.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class UserDaoTest {

    @Test
    void selectByPrimaryKey() {
        UserDao userDao = new UserDao();
        User user = userDao.selectByPrimaryKey(1);
        System.out.println("name"+user.getName());
    }

    @Test
    void  insert(){
        SqlSession getsession = MybatisUtil.getsession();
        UserMapper mapper = getsession.getMapper(UserMapper.class);
        int insert = mapper.insert(new User(10, "小不点", "123456", Date.from(Instant.ofEpochSecond(2020/02/02))));
        if (insert > 0){
            System.out.println("插入成功");
        }
    }

    @Test
    void  insert2() throws ParseException {
        SqlSession getsession = MybatisUtil.getsession();
        UserMapper mapper = getsession.getMapper(UserMapper.class);
        User user = new User();
        user.setName("aa");
        user.setId(102);
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        Date date = sdf.parse("2000-2-28");

//        Calendar calendar = Calendar.getInstance();
//        Date time = calendar.getTime();

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2021);
        calendar.set(Calendar.MONTH, 3);
        calendar.set(Calendar.DAY_OF_MONTH, 7);
        user.setBirth(calendar.getTime());

        int insert = mapper.insert(user);
        if (insert > 0){
            System.out.println("插入成功");
        }
    }


    @Test
    void  deleteByPrimaryKey(){
        UserDao userDao = new UserDao();
        int res = userDao.deleteByPrimaryKey(2);
        if (res > 0){
            System.out.println("删除成功");
        }
    }

    @Test
    void updateByPrimaryKey(){
        SqlSession getsession = MybatisUtil.getsession();
        UserMapper mapper = getsession.getMapper(UserMapper.class);
//        int update = mapper.updateByPrimaryKey(new User(3, "小不点", "131313131", Date.from(Instant.ofEpochSecond(1580982158))));
//        if (update > 0){
//            System.out.println("更新成功");
//        }

    }
}

