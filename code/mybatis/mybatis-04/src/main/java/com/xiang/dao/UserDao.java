package com.xiang.dao;

import com.xiang.mapper.UserMaper;
import com.xiang.pojo.User;
import org.apache.ibatis.session.SqlSession;

public class UserDao {
    public User findById(Integer id){
        SqlSession getsession = MybatisUtils.getsession();
        UserMaper mapper = getsession.getMapper(UserMaper.class);
        User user = mapper.findById(id);
        return user;
    }
}
