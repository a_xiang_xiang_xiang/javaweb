package com.xiang.mapper;

import com.xiang.pojo.User;

public interface UserMaper {
    User findById(Integer id);
}
