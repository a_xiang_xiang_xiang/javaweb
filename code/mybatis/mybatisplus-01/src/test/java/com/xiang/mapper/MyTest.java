package com.xiang.mapper;

import com.xiang.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class MyTest {
    // 继承了BaseMapper,所有的方法都来自父类
    // 我们也可以编写自己的扩展方法
    @Autowired
    private UserMapper userMapper;

    @Test
    public  void  myTest(){
        if (userMapper == null) {
            System.out.println("null");
        }
        List<User> users=userMapper.selectList(null);
        users.forEach(System.out::println);
    }
}
