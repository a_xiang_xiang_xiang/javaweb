package com.xiang.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiang.pojo.User;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
@Repository

public interface UserMapper extends BaseMapper<User> {

}
