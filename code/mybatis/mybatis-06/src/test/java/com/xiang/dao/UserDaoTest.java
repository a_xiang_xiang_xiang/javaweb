package com.xiang.dao;

import com.xiang.pojo.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserDaoTest {

    @Test
    void findById() {
        UserDao userDao = new UserDao();
        User byId = userDao.findById(1);
        System.out.printf("name", byId.getName());
        System.out.println();
        System.out.println(byId.getName());
        Assertions.assertEquals("小向",byId.getName());
    }
}