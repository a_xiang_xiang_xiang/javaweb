package com.xiang.dao;

import com.xiang.mapper.UserMapper;
import com.xiang.pojo.User;
import org.apache.ibatis.session.SqlSession;

public class UserDao {
    User findById(Integer id){
        SqlSession getsession = MybatisUtil.getsession();
        UserMapper mapper = getsession.getMapper(UserMapper.class);
        User buId = mapper.findById(id);
        return buId;
    }
}
