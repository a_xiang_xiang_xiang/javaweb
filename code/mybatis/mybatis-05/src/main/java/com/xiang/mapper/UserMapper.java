package com.xiang.mapper;

import com.xiang.pojo.User;

public interface UserMapper {
    User findById (Integer id);
}
