package com.xiang.dao;

import com.xiang.mapper.UserMapper;
import com.xiang.pojo.User;
import org.apache.ibatis.session.SqlSession;

public class UserDao {
    public User findById(Integer id){
        SqlSession getsession = MybatisUitl.getsession();
        UserMapper mapper = getsession.getMapper(UserMapper.class);
        User byId = mapper.findById(id);
        return  byId;

    }
}
