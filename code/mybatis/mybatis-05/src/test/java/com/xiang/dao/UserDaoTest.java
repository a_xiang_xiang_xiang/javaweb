package com.xiang.dao;

import com.xiang.pojo.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserDaoTest {

    @Test
    void findById() {
        UserDao userDao = new UserDao();
        User byId = userDao.findById(2);
        System.out.println("Name\t"+ byId.getName());
        Assertions.assertEquals("小哈",byId.getName());
    }
}