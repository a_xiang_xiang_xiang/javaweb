package com.dao;

import com.xiang.mapper.UserMapper;
import com.xiang.model.User;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.List;

public class UserDao {
    public User selectByPrimaryKey(Integer id){
        SqlSession getsession = mybatisUtil.getsession();
        UserMapper mapper = getsession.getMapper(UserMapper.class);
        User user = mapper.selectByPrimaryKey(id);
        return user;

    }

    public  ArrayList  selectAll(){
        SqlSession getsession = mybatisUtil.getsession();
        UserMapper mapper = getsession.getMapper(UserMapper.class);
        List<User> users = mapper.selectAll();
        return (ArrayList) users;
    }

//    public int insert(User record) throws ParseException {
//        SqlSession getsession = mybatisUtil.getsession();
//        UserMapper mapper = getsession.getMapper(UserMapper.class);
//        User user = new  User();
//        user.setId(110);
//        user.setName("小小");
//        user.setMobile("12345689");
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//        Date date = format.parse("2020-03-06");
//        user.setBirth(date);
//        int insert = mapper.insert(user);
//        return  insert;
//    }


    public int deleteByPrimaryKey(Integer id){
        SqlSession getsession = mybatisUtil.getsession();
        UserMapper mapper = getsession.getMapper(UserMapper.class);
        int i = mapper.deleteByPrimaryKey(1);
        return i;
    }
}
