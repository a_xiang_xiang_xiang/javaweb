package com.dao;

import com.xiang.mapper.UserMapper;
import com.xiang.model.User;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class UserDaoTest {

    @Test
    void selectByPrimaryKey() {
        UserDao userDao = new UserDao();
        User user = userDao.selectByPrimaryKey(1);
        System.out.println(user.getName());
        System.out.println(user.getBirth());
        System.out.println("--------------");
    }

    @Test
    void  selectAll(){
//        SqlSession getsession = mybatisUtil.getsession();
//        UserMapper mapper = getsession.getMapper(UserMapper.class);
//        List<User> userList = mapper.selectAll();
//        for (User user:userList){
//            System.out.println(user);
//        }

        UserDao userDao = new UserDao();
        List<User> user = (List<User>) userDao.selectAll();
        for (User users:user){
            System.out.println(users);
        }
    }


    @Test
    void  insert() throws ParseException {
//        UserDao userDao = new UserDao();
//        userDao.insert()


        SqlSession getsession = mybatisUtil.getsession();
        UserMapper mapper = getsession.getMapper(UserMapper.class);
        User user = new  User();
        user.setId(110);
        user.setName("小小");
        user.setMobile("12345689");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse("2020-03-06");
        user.setBirth(date);
        int insert = mapper.insert(user);
        if (insert>0){
            System.out.println("插入成功");
        }
    }

    @Test
    void  updateByPrimaryKey(){
        SqlSession getsession = mybatisUtil.getsession();
        UserMapper mapper = getsession.getMapper(UserMapper.class);
//        mapper.updateByPrimaryKey()
    }


    @Test
    void  deleteByPrimaryKey(){
        UserDao userDao = new UserDao();
        int delete = userDao.deleteByPrimaryKey(1);
        if (delete > 0 ){
            System.out.println("删除成功");
        }
    }

}