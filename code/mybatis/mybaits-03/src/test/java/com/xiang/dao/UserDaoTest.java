package com.xiang.dao;

import com.xiang.pojo.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserDaoTest {

    @Test
    void findById() {
        UserDao userDao=new UserDao();
        User user = userDao.findById(1);
        System.out.println(user.getName());
        Assertions.assertEquals("小向", user.getName());

    }
}