package com.xiang.dao;

import com.xiang.mapper.UserMapper;
import com.xiang.pojo.User;
import org.apache.ibatis.session.SqlSession;

public class UserDao {
    public User findById(Integer id){
        try(SqlSession session = MyBatisutil.getsession()){
            UserMapper mapper = session.getMapper(UserMapper.class);
            User user = mapper.findById(id);
            return  user;
        }

    }
}
