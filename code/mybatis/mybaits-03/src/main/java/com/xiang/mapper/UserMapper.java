package com.xiang.mapper;

import com.xiang.pojo.User;

public interface UserMapper {
    public User findById (Integer id);
}
