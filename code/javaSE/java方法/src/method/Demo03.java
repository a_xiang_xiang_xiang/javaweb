package method;
/*
可变参数；
不定项；‘
 */
public class Demo03 {
    public static void main(String[] args) {
        Demo03 demo03 = new Demo03();
        demo03.test(1,2,3,4,7,5);
    }

//可变长参数，必须要最后面
    public  void  test(int x, int... a){
        System.out.println(a[0]);
        System.out.println(a[1]);
        System.out.println(a[2]);
        System.out.println(a[3]);
        System.out.println(a[4]);
    }



    public  void  method(){}
    public  void  method(int i){}
    public  void  method(int i,int i2){}
    public  void  method(int i,double i2){}
}
