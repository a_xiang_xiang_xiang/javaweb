package method;

public class Demo01 {
    public static void main(String[] args) {
//     实参
        int sum = add(1, 4);
        System.out.println(sum);//5


//        int max = max(10, 50);
//        System.out.println(max);//50
         double max = max(10.56, 50.0);
        System.out.println(max);//50.0

//        重载：
//        方法名一样，参数类型不同、参数个数不同、参数列表不同、

    }

    //    加法
    //形参
    public static int add(int a, int b) {
        return a + b;

    }


    //    比大小
    public static double max(double num1, double num2) {
        double res=0;

        if (num1 > num2) {
            res = num1;
        }else {
            res = num2;
        }

        if(num1==num2){
            System.out.println("num1==num2");
            return  0;
        }
        return res;
    }
    public static int max(int num1, int num2) {
        int res=0;

        if (num1 > num2) {
            res = num1;
        }else {
            res = num2;
        }

        if(num1==num2){
            System.out.println("num1==num2");
            return  0;
        }
        return res;
    }
}
