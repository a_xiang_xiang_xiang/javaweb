package method;

public class Demo04 {
    public static void main(String[] args) {
        Demo04 demo04 = new Demo04();
        demo04.printMax(456,454,4654.4,6.454,6544.65,465.48,4564);

        demo04.printMax(new  double[]{1,2,35,5,8,55,66,33,1122131,});
    }

    public void printMax(double... num) {
        if (num.length == 0) {
            System.out.println("No passed");
            return;
        }


        double res = num[0];

//        排序
        for (int i = 0; i < num.length; i++) {
            if (num[i] > res) {
                res = num[i];
            }

        }
        System.out.println("The Max value is:\t" + res);
    }
}
