package method;

/**
 * 递归
 */
public class Demo05 {
    public static void main(String[] args) {
        Demo05 text = new Demo05();
        text.test();
        /*
        会报错； 栈溢出；
        Exception in thread "main" java.lang.StackOverflowError
         */

    }

    public  void  test(){
//        这里是自己调用自己；
        test();
    }
}
