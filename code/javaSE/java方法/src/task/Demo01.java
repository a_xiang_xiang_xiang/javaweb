package task;

import java.util.Scanner;

/*
实现简单计算器；
 */
public class Demo01 {
    final static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        calc();
    }

    public static void calc() {
        System.out.println("请输入第一个数字：");
        double one = scanner.nextDouble();
        System.out.println("请输入第二个数字：");
        double two = scanner.nextDouble();
        System.out.println("请选择运算符：");
        System.out.println("1：加 2：减 3：乘 4：除");

        double res = scanner.nextDouble();

        while (res == 4 && two < 0) {
            System.out.println("被除数不能为0，请重新输入：");
            two = scanner.nextDouble();
        }

        while (res > 4 || res < 1) {
            System.out.println("运算符不合法，请重新选择运算符");
            System.out.println("1：加 2：减 3：乘 4：除");
            res = scanner.nextDouble();
        }


        double result = 0;
        double remainder = 0;

        switch ((int) res) {
            case 1:
                result = one + two;
                break;
            case 2:
                result = one - two;
                break;
            case 3:
                result = one * two;
                break;
            case 4:
                result = one / two;
                remainder = one % two;
                break;
        }
        if (res == 4) {
            System.out.println("结果为：" + result + "余数为：" + remainder);
        } else {
            System.out.println("结果为：" + result);
            scanner.close();
        }
    }
}
