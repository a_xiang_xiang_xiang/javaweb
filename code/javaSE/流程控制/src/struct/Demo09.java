package struct;

public class Demo09 {
    public static void main(String[] args) {
        text();
    }

    public static void text() {
        int[] nums = {10, 20, 30, 40, 50,65,65,656565,656};

        for (int i = 0; i < nums.length; i++) {
            System.out.print(nums[i]+"\t");//10	20	30	40	50	65	65	656565	656
        }
        System.out.println();
        System.out.println("=================");


        for (int x : nums) {
            System.out.print(x+"\t");//10	20	30	40	50	65	65	656565	656
        }
    }
}
