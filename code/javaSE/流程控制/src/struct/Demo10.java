package struct;

public class Demo10 {
    public static void main(String[] args) {
        text();
        System.out.println();
        System.out.println("=====================================");

        text2();
    }

    public static void text() {
        int i = 0;
        while (i < 100) {
            i++;
            System.out.print(i + "\t");//1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20	21	22	23	24	25	26	27	28	29	30	31	32	33	34	35	36	37	38	39	40
            if (i == 40) {
                break;
            }
        }
    }


    public static void text2() {
        int i = 0;
        while (i < 100) {
            i++;
            if (i % 10 == 0) {
                System.out.println("当前值：" + i);
                System.out.println();
                continue;
            }
            System.out.print(i+"\t");
        }
        /**
         *
         1	2	3	4	5	6	7	8	9	当前值：10

         11	12	13	14	15	16	17	18	19	当前值：20

         21	22	23	24	25	26	27	28	29	当前值：30

         31	32	33	34	35	36	37	38	39	当前值：40

         41	42	43	44	45	46	47	48	49	当前值：50

         51	52	53	54	55	56	57	58	59	当前值：60

         61	62	63	64	65	66	67	68	69	当前值：70

         71	72	73	74	75	76	77	78	79	当前值：80

         81	82	83	84	85	86	87	88	89	当前值：90

         91	92	93	94	95	96	97	98	99	当前值：100
         */
    }
}
