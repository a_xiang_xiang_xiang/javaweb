package struct;

public class Demo04 {
    public static void main(String[] args) {
        text();

        text2();

        text3();
    }

    public static void text() {
        int i = 0;
        int sum = 0;

        do {
            sum += i;
            i++;
        } while (i <= 100);
        System.out.println(sum);//5050
    }


    public static void text2() {
        int a = 10;

        while (a < 10) {
            System.out.println(a);//不进这个循环
            a++;
        }

        System.out.println("-----------------");

        do {
            System.out.println(a);//10
            a++;
        }while (a<10);
    }


    public  static  void  text3(){
        int a=1;
        while (a<100){
            System.out.println(a);
            a+=2;
        }
        System.out.println("while循环结束");

        for (int i = 1; i < 100; i+=2) {
            System.out.println(i);
        }
        System.out.println("for循环结束");


    }
}

