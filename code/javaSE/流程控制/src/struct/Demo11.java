package struct;
/*
质数
101 ---150 的所有质数
 */
public class Demo11 {
    public static void main(String[] args) {

        outer: for (int i = 101; i < 150; i++) {
            for (int j = 2; j < i/2; j++) {
                if (i%j==0){
                    continue outer;
                }
            }
            System.out.print(i+"\t");//101	103	107	109	113	127	131	137	139	149
        }
    }
}
