package struct;


import java.util.Scanner;

public class Demo01 {
    final  static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        text();

        text2();

        text3();

        text4();

        text5();
    }
    /*
    顺序结构
     */
    public static void text() {
        System.out.println("hello1");
        System.out.println("hello2");
        System.out.println("hello3");
        System.out.println("hello4");
        System.out.println("hello5");
    }

    /*
    选择结构
     */

    public  static  void  text2(){

        System.out.println("请输入内容：");
        String line = scanner.nextLine();

        if (line.equals("hello")){
            System.out.println(line);
        }else {
            System.out.println("error");
        }
//        scanner.close();
    }

    public  static  void  text3(){

        System.out.println("请输入成绩：");

        int score = scanner.nextInt();
        if (score>60){
            System.out.println("成绩及格");
        }else {
            System.out.println("成绩不及格");
        }
//        scanner.close();
    }


    public  static  void  text4(){
        System.out.println("请输入你的成绩：");
        int nextInt = scanner.nextInt();

        if (nextInt>=90){
            System.out.println("你的成绩为："+nextInt+"\t等级为：优秀");
        }else  if (nextInt>=80){
            System.out.println("你的成绩为："+nextInt+"\t等级为：良好");
        }else  if (nextInt>=70){
            System.out.println("你的成绩为："+nextInt+"\t等级为：中等");
        }else  if (nextInt>=60){
            System.out.println("你的成绩为："+nextInt+"\t等级为：及格");
        }else {
            System.out.println("你的成绩为："+nextInt+"\t等级为：不及格");
        }

//        scanner.close();
    }


    public  static  void  text5(){
        System.out.println("请输入你的成绩：");
        int nextInt = scanner.nextInt();

        if (nextInt==100){
            System.out.println("你的成绩为："+nextInt+"\t恭喜满分");
        }else  if (nextInt<100&&nextInt>=90){
            System.out.println("你的成绩为："+nextInt+"\t等级为：A");
        }else  if (nextInt<90&&nextInt>=80){
            System.out.println("你的成绩为："+nextInt+"\t等级为：B");
        }else  if (nextInt<80&&nextInt>=70){
            System.out.println("你的成绩为："+nextInt+"\t等级为：C");
        }else  if (nextInt<70&&nextInt>=60){
            System.out.println("你的成绩为："+nextInt+"\t等级为：D");
        }else  if (nextInt<60&&nextInt>=0){
            System.out.println("你的成绩为："+nextInt+"\t等级为：不及格");
        }else {
            System.out.println("你输入的成绩不合法");
        }

        scanner.close();
    }
}
