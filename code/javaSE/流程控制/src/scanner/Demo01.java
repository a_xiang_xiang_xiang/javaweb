package scanner;

import java.util.Scanner;

public class Demo01 {
    public static void main(String[] args) {
        scanner();

        scanner2();

        scanner3();
    }

    public  static  void  scanner(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("next方式接收：");

        if (scanner.hasNext()){
            String str= scanner.next() ;
            System.out.println("输入内容为："+str);
        }
//        scanner.close();
    }

    public  static  void  scanner2(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("nextLine方式接收：");

        if (scanner.hasNextLine()){
            String srt=scanner.nextLine();
            System.out.println("输入内容为："+srt);

        }
//        scanner.close();
    }


    public  static  void  scanner3(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入数据 ：");

        String str = scanner.next();
        System.out.println("next===>输入内容为："+str);
        String res = scanner.nextLine();
        System.out.println("nextLine==>输入内容为："+res);

        scanner.close();

    }

}
