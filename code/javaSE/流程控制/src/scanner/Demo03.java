package scanner;

import java.util.Scanner;

public class Demo03{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入数据 ，如果输入的不是数字 ，则停止计算；");


//        和
        double sum =0;
//        计算输入了多少个 数字
        int count = 0;

//        循环
        while (scanner.hasNextDouble()){
            double aDouble = scanner.nextDouble();

            count++;

            sum=sum+aDouble;

            System.out.println("你输入了第"+count+"个数，当前的sum是"+sum);
            System.out.println("你输入了第"+count+"个数，当前的average是"+(sum/count));
        }

        System.out.println("共输入了"+count+"个数，输入的个数和为："+sum);
        System.out.println("共输入了"+count+"个数，输入的个平均值为："+(sum/count));


        scanner.close();
    }
}
