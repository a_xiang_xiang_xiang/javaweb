package scanner;

import java.util.Scanner;

public class Demo02 {

    final static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        scanner();

    }

    public static void scanner() {
        int i = 0;
        float f = 0.0f;

        System.out.println("请输入整数：");

        if (scanner.hasNextInt()) {
            int nextInt = scanner.nextInt();
            System.out.println("整数数据 " + nextInt);
        } else {
            System.out.println("输入的不是整数数据 ；");
        }

        System.out.println("请输入小数：");

        if (scanner.hasNextFloat()) {
            float nextFloat = scanner.nextFloat();
            System.out.println("小数数据 " + nextFloat);
        } else {
            System.out.println("输入的不是小数数据 ；");
        }

        scanner.close();
    }
}
