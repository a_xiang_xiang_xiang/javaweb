package oop.demo10;

//用户的业务；
//interface 接口  、接口都是需要有实现类的；
public interface UserService {
    //接口中的所有定义的属性都是常量  public static final
    public static final int age=99;

    //接口中的所有定义方法都是抽象的，public  abstract
    public abstract void run();

    void eat();

    void add(String name);

    void del(String name);

    void update(String name);

    void query(String name);


}
