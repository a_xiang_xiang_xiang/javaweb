## 接口
![img.png](img.png)

##接口的作用
    1，约束
    2，定义一些方法，让不同的人实现
    3，所有方法都是 public  abstract
```
    //接口中的所有定义方法都是抽象的，public  abstract
    public abstract void run();
    void eat();
    void add(String name);
```
    4，所有属性都是常量 public static final
```
    //接口中的所有定义的属性都是常量  public static final
    public static final int age=99;
```
    5,接口不能直接被实例化 (接口中没有构造方法),抽象类也不能被实例化，
    6，接口可以实现多个 通过 implements 关键字实现
    7，实现接口，必须要重写接口里边的所有方法