package oop.demo10;

// 抽象类 extends
// 类 可以实现 接口 通过implement 关键字
//  重写里边 的方法；---------实现了接口的类，必须在重写接口里边的方法；

//侧面实现了 多继承 ；implements UserService,TimeService
//接口中只有方法的定义；
public class UserServiceImpl implements UserService,TimeService {
    @Override
    public void run() {

    }

    @Override
    public void eat() {

    }

    @Override
    public void add(String name) {

    }

    @Override
    public void del(String name) {

    }

    @Override
    public void update(String name) {

    }

    @Override
    public void query(String name) {

    }

    @Override
    public void timer() {

    }
}
