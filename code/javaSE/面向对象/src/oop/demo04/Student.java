package oop.demo04;

/*
学生类
 */
public class Student {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
       if (age>120 || age>0){
           System.out.println("你输入的年龄不合法");
       }else{
           this.age = age;
       }
    }

    /**
     * 属性
     * 名字
     * 学号
     * 性别
     * <p>
     * 方法
     * 学习()
     * 睡觉()
     */
//属性私有
    private String name;
    private int id;
    private char sex;
    private int age;

//    get/set
    public  String getName(){
        return  this.name;
    }

    public void  setName(String name){
        this.name=name;
    }
}
