package oop.demo04;

public class Application {
    /*
    封装：
        1，提高程序安全性、保护数据
        2，隐藏代码的实现细节
        3，统一接口
        4，系统可维护性增加了；
     */
    public static void main(String[] args) {
        Student s1 = new Student();
        s1.setName("小向");
        System.out.println("name:"+s1.getName());

        s1.setAge(999);
        System.out.println("age:"+s1.getAge());
    }
}
