package oop;

import oop.demo03.Pet;

public class Application {
    public static void main(String[] args) {
        Pet pig = new Pet();
        pig.name="小朱佩奇";
        pig.age=3;
        pig.shout();

        System.out.println("name:"+pig.name);
        System.out.println("age:"+pig.age);

    /*
    1，类与对象
        类是一个横版：抽象的
        对象是一个具体的实例；
    2，方法
        定义、调用；
    3，对象的引用
        引用类型        基本类型（8种）
        对象是通过引用来操作的，栈----》堆 【地址】
    4，属性、字段Field 成员变量
        默认初始化：
            数字: 0 0.0
            char: u0000
            boolean: false
            引用: null
        属性的定义
            修饰符     属性类型       属性名=属性值；
    5,方法，
        定义、调用
    6，对象的创建和使用
        必须使用new关键字创造对象；还需要有构造器 Person xiang = new Person();
        对象的属性； xiang.name
        对象的方法； xiang.sleep()
    7,类
        静态的属性   属性
        动态的行为   方法


    java的三大特征
        封装
        继承
        多态


     */
    }
}
