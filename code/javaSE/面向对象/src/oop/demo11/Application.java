package oop.demo11;

public class Application {
    public static void main(String[] args) {
        //new
        Outer outer = new Outer();
        outer.out(); //这是一个外部类 方法；

//        通过这个外部内来实例化这个内部类；
        Outer.Inner inner = outer.new Inner();
        inner.in(); //这是一个内部类 方法；

        inner.getTd(); //id:19301041
    }
}
