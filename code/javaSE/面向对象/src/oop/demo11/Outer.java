package oop.demo11;

//外部类；
public class Outer {
    //    private  int id = 19301041;
    private static int id = 19301041;

    public void out() {
        System.out.println("这是一个外部类 方法；");
    }

    //   这是一个内部类
    public class Inner {
        public void in() {
            System.out.println("这是一个内部类 方法；");
        }

        //        内部类，访问外部类的一些私有变量；
//        获得外部类的系有属性；
        public void getTd() {
            System.out.println("id:" + id);
        }
    }

    //    静态内部类
//   这是一个内部类
    public static class Inner2 {
        public void in() {
            System.out.println("这是一个内部类 方法；");
        }

        //        内部类，访问外部类的一些私有变量；
//        获得外部类的系有属性；
        public void getTd() {
            System.out.println("id:" + id);
        }
    }

    //    局部内部类
    public void method() {
        class Inner3 {
            public void in() {

            }
        }
    }

}

//一个Java类 中可以的多个class类 但是 只有一个 public class
//内部类；
class A {
    public static void main(String[] args) {

    }
}
