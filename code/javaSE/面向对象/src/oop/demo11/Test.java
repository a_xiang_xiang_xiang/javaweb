package oop.demo11;

public class Test {
    public static void main(String[] args) {
        Apple apple = new Apple();
        apple.eat(); //eat------------>Apple

        System.out.println("===========");
//        没有名字初始化类、 匿名内部类；匿名对象的使用；  不用蒋实例保存到变量中，
        new Apple().eat(); //eat------------>Apple

        UserService userService =  new UserService() {
            @Override
            public void hell0() {

            }
        };
    }
}

class Apple {
    public void eat() {
        System.out.println("eat------------>Apple");
    }
}

interface UserService {
    void  hell0();
}
