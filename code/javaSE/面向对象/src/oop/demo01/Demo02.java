package oop.demo01;

/*
方法的调用；
 */
public class Demo02 {
    /*
    静态方法：static

    非静态方法：
     */
//    建立 一个student  学生类；

    public static void main(String[] args) {
//不是静态方法：static 需实例化这个类；new
//        对象类型  对名字  =  对象的值  ； 
        Student student = new Student();
        student.say();//同学们开始说话！！！

//实际参数 要和 形式参数 的类型一一对应；
        int add = add(3, 4);
        System.out.println("sum:" + add);// 7

        int a = 1;
        System.out.println("原始的a的值:" + a);//1

//        值传递
        Demo02 demo02 = new Demo02();
        demo02.change(a);
        System.out.println("调用值传递的a的值:" + a);//1
//        因为 我们java是值传递；

//        引用传递
        Perosn perosn = new Perosn();
        System.out.println("perosn.name:" + perosn.name);//null

        chang2(perosn);
        System.out.println("perosn.name:" + perosn.name);//小小
    }

    //    和类一起加载的
    public static void a() {
        b();
//        可以直接调用b 方法；
//        c(); //c 则不行
    }

    public static void b() {
        a();
    }

    //    等实例化之后 才存在
    public void c() {
    }

    //求和方法
    public static int add(int a, int b) {
        return a + b;
    }

    //值 传递方法
//    返回值 为空；
    public void change(int a) {
        a = 10;
    }

    //    引用传递； 传递一个对象； 本质还是一个值传递；
//    定义了一个Perosn类 有一个name 属性；
    static class Perosn {
        String name;
    }

    public static void chang2(Perosn perosn) {
//        这儿的perosn 是一个对象；指向的是person 这个类，Perosn perosn = new Perosn(); 这是一个具体的人，可改变属性；
        perosn.name = "小小";
    }
}
