package oop.demo01;

import java.io.IOException;

//Demo01 类
public class Demo01 {
    //    main 方法；
    public static void main(String[] args) {

    }

    /*
    修饰符     返回值类型     方法名（...）{
            //方法体
            return  返回值；
    }
     */
    public String sayHello() {
        return "hello ";
    }

    public void hello() {
        return;
    }

    public int max(int a, int b) {
        //三元运算
        return a > b ? a : b;
    }

//    IOException 异常；
    public void  readFile(String file) throws IOException{

    }
}
