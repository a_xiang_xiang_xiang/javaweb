package oop.demo06;
//多态
public class Application {
    public static void main(String[] args) {
//一个对象的实际类型是确定了的
        Person p0 = new Person();
        Student s0 = new Student();

//        可以指向的引用类型不确定---
//        父类的引用指向子类的类型
//        父类 new 子类  -------------- 多态

        //student 子类能调用的方法都是自己的或者继承父类的
        Student s1 = new Student();
        //person 父类 可以指向子类，但不能调用子类独有的方法
        Person s2 = new Student();
        Object s3 = new Student();
//        String s4 = new Student(); //报错 类型转换异常 ClassCastException

        s2.run();//(没重写之前 student) run============>Person ///(走了重写之后 )son==========Student
        //子类重写父类的方法，那么执行的是子类的方法；
        s1.run(); //son==========Student

        s1.eat();//eat=============Student
//        强制转换 高----》低
        ((Student) s2).eat();

    }
    /*
    多态注意事项
    1，多态是方法的多态，属性没有多态
    2，父类和子类 有联系
    3，类型转换异常 ClassCastException
    4，存在的条件： 继承关系、 方法需要重写、 父类引用指向子类对象       Father f1 =new Son()；

        不能重写的；
            1，static 方法，属性类，不属性实例
            2，final 常量
            3，private 方法
     */
}
