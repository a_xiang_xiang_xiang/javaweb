package oop.demo07;

/*
instanceof 与 类型转换
    1,父类引用指向子类对象
    2，把子类转换为父类，向上转型
    3，把父类转换为子类，向下转型：强制转换
    4，方便就去的调用，减少重复的代码，

    抽象：封装、继承、多态；
 */
public class Application {
    public static void main(String[] args) {
//        类型之间的转换   父类   子类

//        高               低
        Person s1 = new Student();
//        强制类型转换
        Student s2 = (Student) s1;
        s2.go();//go

        Student s3 = new Student();
        s3.go();//go
        s3.run();//run
        Person s4 = new Student();
        s4.run();//run
        ((Student) s4).go();//go

        System.out.println("===================");

        Object obj = new Student();

//        System.out.println(x instanceof  y); //能不能编译通过

        System.out.println(obj instanceof Student);//true
        System.out.println(obj instanceof Person);//true
        System.out.println(obj instanceof Object);//true
        System.out.println(obj instanceof Teacher);//false
        System.out.println(obj instanceof String);//false

        System.out.println("===============");
        Person person = new Person();
        System.out.println(person instanceof Person);//true
        System.out.println(person instanceof Object);//true
        System.out.println(person instanceof Student);//false
        System.out.println(person instanceof Teacher);//false
//        System.out.println(person instanceof String);//编译报错
        System.out.println("===============");
        Student student = new Student();
        System.out.println(student instanceof Person);//true
        System.out.println(student instanceof Object);//true
        System.out.println(student instanceof Student);//false
//        System.out.println(student instanceof Teacher);//编译报错
//        System.out.println(student instanceof String);//编译报错
    }


}
