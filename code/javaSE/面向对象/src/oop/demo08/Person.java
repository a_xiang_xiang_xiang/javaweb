package oop.demo08;

/*
静态代码块
 */
//public final class Person { ------定义 final 之后 不能被继承
public  class Person {

    {
//        代码块（匿名）
//        跟对象同时产生 --------赋一些初始值
        System.out.println("匿名代码块");
    }

    static { //类加载执行 （永久  只执行一次）
        //静态代码块
        System.out.println("静态代码块");
    }

    public Person() {
        System.out.println("无参构造方法");
    }

    public static void main(String[] args) {
        Person person = new Person();
        System.out.println("==================");
        Person person2 = new Person();
    }
    /*
    依次输出的顺序是：
        静态代码块
        匿名代码块
        无参构造方法
        ==================
        匿名代码块
        无参构造方法
     */
}


