package oop.demo08;
//static 关键字
public class Student {
    private  static  int age;
    private  double score;

    public void  run(){
        //非静态方法可以访问静态方法里边 的所有东西
go();
    }
    public  static  void  go(){//这个东西跟类一块加载
//        静态方法里边不可以调用普通（非静态）方法

    }

    public static void main(String[] args) {
        Student s1 = new Student();
        System.out.println(Student.age);//0
        System.out.println(s1.age);//0
        System.out.println(s1.score);//0.0

//        通过对象点方法调用
        new  Student().run();

        Student.go();
        go();
    }
}
