package oop.demo09;
//抽象类的所有方法，继承了他的子类，都必须要实现它的方法，除非这个子类是抽象类，----》那么有子子类实现
public class A extends Action{
    @Override
    public void doSth() {

    }
}
