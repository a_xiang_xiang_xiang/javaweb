package oop.demo09;

//abstract 抽象类
//类  extends   单继承   （接口可以多继承 ）
public abstract class Action {

    //    abstract 抽象方法 只有方法的名字，没有方法的实现
    public abstract void doSth();

    public void test() {

    }

    /*
    抽象类的特点：
        1，不能new这个抽象类，只能靠子类去实现它，这是一个约束；
        2，抽象类里边可以写普通方法
        3，抽象方法必须在抽象类中
        4，
     */
}
