package oop.demo02;

import java.time.Period;

public class Person {
    String name;

    //    无参构造----------默认构造；
    //        实例化 初始值
//    1、使用new关键字、本质是在调用构造器
//    2、构造器，一般用来初始化值
    public Person() {
        this.name = "小同";
    }

    //    重载；
//    有参构造
//    一旦定义了有参构造，无参构造就必须显示定义
    public Person(String name) {
        this.name = name;
    }

    /**
     构造器：
     1，和类名相同
     2，没有返回值
     作用：
     1，new本质在调用构造方法
     2，初始化对象的值
     注意点：
     1，定义有参构造的时候，如果想使用无参构造，显示的定义一个无参构造即可


     */
}
