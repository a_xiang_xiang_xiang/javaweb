package oop.demo02;

public class Application {
    public static void main(String[] args) {
        /*
        Student类
         */
//类，抽象的，需要实例化
        Student xiaoming = new Student();
        System.out.println("xiaoming.name:\t" + xiaoming.name);//xiaoming.name:	null
        System.out.println("xiaoming.age:\t" + xiaoming.age);//xiaoming.age:	0

        Student xiaoxiao = new Student();
        xiaoxiao.name="小不";
        xiaoxiao.age=18;
        System.out.println("xiaoxiao.name:\t"+xiaoxiao.name);//xiaoxiao.name:	小不
        System.out.println("xiaoxiao.age:\t"+xiaoxiao.age);//

        System.out.println("==================");
        /*
        Person类；
         */
//        new 实例化了一个对象
        Person person = new Person();
        System.out.println("person.name:"+person.name);//person.name:小同
    }
}
