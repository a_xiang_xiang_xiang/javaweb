package oop.demo05;

import javax.swing.plaf.PanelUI;

// 继承B类
public class A extends  B{
    public  static void test(){
        System.out.println("A===>test()");
    }

// Override 重写
    @Override
    public void test2() {
//        super.test2(); //默认调用父类
//        可重写自己的方法
        System.out.println("B===>test2()");
    }
}
