package oop.demo05;
/*
继承
 */
public class Person {
//    无参构造
    public Person() {
        System.out.println("Person 无参执行了");
    }

    public void  say(){
        System.out.println("说了一句话！！");
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    private   int money=10_0000_0000;

    /*
    public 公共的
    private 私有的
    default 默认的
    protected 受保护的
     */


    protected  String name="小小向";

    public void  print(){
        System.out.println("Person");
    }

    //私有和不能被继承
    private void  print2(){
        System.out.println("Person");
    }

}
