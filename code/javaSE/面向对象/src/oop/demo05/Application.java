package oop.demo05;

//继承
public class Application {
    public static void main(String[] args) {

        /*
        先执行 父类的无参、在执行了类的无参
        Person 无参执行了
        Student 无参执行了
         */

        Student s1 = new Student();
        s1.say(); //说了一句话！！
//        System.out.println(s1.money);//1000000000 加了私有 修饰后，这个语句报错

        System.out.println("s1.getMoney():" + s1.getMoney()); //s1.getMoney():1000000000

        Student s2 = new Student();
        s2.Test("向向");

        s2.Test2();
/**
 * 静态方法与非静态方法区别很大
 *      静态方法：方法的调用只与左边定义的数据 类型，有关
 *      非静态方法： 重写；
 */
        System.out.println("============方法的重写============");
        A a = new A();
        a.test(); //A===>test()
        a.test2();//B===>test2()
//        父类的引用指向了子类；B 指向 A
        B b = new A();
        b.test();//B===>test()
        a.test2();//B===>test2() //子类重写父类的方法
    }
}
