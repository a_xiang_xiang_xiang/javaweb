package oop.demo05;

public class Student extends Person {
    public Student() {
//        隐藏代码，调用父类的无参构造
        super();
        System.out.println("Student 无参执行了");
    }

    private String name="小玉";

    public  void Test(String name){
        System.out.println("name:"+name);//向向
        System.out.println("this.name:"+this.name);//小玉
        System.out.println("super.name:"+super.name); // 小小向 ------------调父类；
    }

    public  void  print(){
        System.out.println("Student");
    }

    public  void  Test2(){
        print();//Student
        this.print();//Student
        super.print();//Person

//        super.print2(); //不能调用，私有的
    }


}
