package operator;

public class Demo04 {
    public static void main(String[] args) {
        text();

        text2();
    }

    public static void text() {
        int a = 10;
        int b = 20;

        a += b;//20

        System.out.println(a);//30

        System.out.println(a+b);//50

        System.out.println(""+a+b);//3020

        System.out.println(a+b+"");//50
    }

    public  static  void  text2(){
        //x ? y : z
        int score = 90;
        String type = score <60?"不及格":"及格";
        System.out.println(type);//及格
    }
}

/**
 * 优先级
 * （）
 * 一元运算符
 * 加减乘除
 * 位于、相等
 */
