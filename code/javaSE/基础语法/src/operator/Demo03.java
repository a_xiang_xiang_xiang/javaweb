package operator;

public class Demo03 {
    public static void main(String[] args) {
        text();

        text2();

        text3();
    }

    public  static void  text3(){
        //位运算
        /*
        A = 0011 1100
        B = 0000 1101

        A&B(A与B) =0000 1100
        A|B(A或B) =0011 1101
        A^B(异或) =0011 0001 相同为0 不同为1
        ~B(取反) =1111 0010

        2*8=16

        <<（左移）  *2
        >>（右移）  /2

        0000 0000   0
        0000 0001   1
        0000 0010   2
        0000 0011   3
        0000 0100   4
        0000 1000   8
        0001 0000   16


        * */

        System.out.println("2<<3:==》"+(2<<3));
    }

    public  static  void  text(){
        //短路运算
         int a=5;
         boolean b=(a<4)&&(a++<4);
        System.out.println(a);//5
        System.out.println(b);//false
    }

    public  static  void  text2(){
        boolean a=true;
        boolean b=false;

        System.out.println("a && b:\t"+(a&&b));//false
        System.out.println("a || b:\t"+(a||b));//true
        System.out.println("!a && b:\t"+!(a&&b));//true

    }
}
