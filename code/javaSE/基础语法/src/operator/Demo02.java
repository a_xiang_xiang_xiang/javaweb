package operator;

public class Demo02 {
    public static void main(String[] args) {
        text0();

        text();

        text2();

        text3();
    }


    public  static  void  text3(){
         boolean a=true;
         boolean b=false;

        System.out.println("a && b:\t"+(a&&b));//false
        System.out.println("a || b:\t"+(a||b));//true
        System.out.println("!a && b:\t"+!(a&&b));//true

    }


    public  static  void  text2(){
        int a=3;
        int b=a++;
        int c=++a;

        System.out.println(a);//5
        System.out.println(b);//3
        System.out.println(c);//5


        double pow = Math.pow(2, 3);
        System.out.println(pow);//8.0
    }

    public  static  void  text0(){
        long a = 123132131313131L;
        int b=12345;
        short c=10;
        byte d=88;

        System.out.println(a+b+c+d);//long 123132131325574
        System.out.println(b+c+d);//int 12443
        System.out.println(c+d);//int  98
    }

    public  static   void text(){
        int a=10;
        int b=20;

        int c = 21;

        System.out.println(c%a);//21/10 ===2.....1 ===>>1
        System.out.println(c/a);//21/10 ===2.....1 ===>> 2

        System.out.println(a>b);//false
        System.out.println(a<b);//true
        System.out.println(a==b);//false
        System.out.println(a!=b);//true
    }
}
