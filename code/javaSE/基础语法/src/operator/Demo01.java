package operator;

public class Demo01 {
    public static void main(String[] args) {
        int a=10;
        int b=20;
        int c=30;
        int d=40;

        System.out.println(a+b);//30
        System.out.println(a-b);//-10
        System.out.println(a*b);//200
        System.out.println(a/b);//0  10/20==0.5  这个地方要取整，所以为0 向下取整；
        System.out.println(a/(double)b);//0.5
    }
}
