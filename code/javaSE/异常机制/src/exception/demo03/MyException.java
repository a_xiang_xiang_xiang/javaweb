package exception.demo03;

//自定义 异常类
public class MyException extends Exception {

    //    传递数字 > 10 那么抛出异常
    //接收参数 判断
    private int detail;
    public MyException(int d) {
        this.detail = d;
    }

//    toString
//     异常的打印信息
    @Override
    public String toString() {
        return "MyException{" +
                "detail=" + detail +
                '}';
    }
}
