package exception.demo02;

public class Test {
    public static void main(String[] args) {
        int a = 1;
        int b = 0;
        try { //try 监控区域

            if (b == 0) { //throw  throws
                throw new ArithmeticException();//主动抛出异常
            }
            System.out.println(a / b);
            new Test().b();
//        } catch (ArithmeticException e) { //catch(想要捕获的异常类型) 捕获异常
//        } catch (Throwable e) {
//            System.out.println("程序异常");
        } catch (Error error) {
            System.out.println("error");
        } catch (Exception exception) {
            System.out.println("exception");
        } catch (Throwable throwable) {
            System.out.println("throwable");
        } finally { //finally 处理善后工作
            System.out.println("finally");
        }

//        要捕获多个异常，需从小到大去捕获；

//        System.out.println(a / b);//Exception in thread "main" java.lang.ArithmeticException: / by zero


        //调用test方法
        System.out.println( "----------调用test方法--------");
//        new  Test().test(1,0);


//        在方法上招聘的异常，需捕获，ctrl+alt+t
        System.out.println( "----------调用test方法 在方法上招聘的异常--------");
        try {
            new  Test().test(1,0);
        } catch (ArithmeticException e) {
            e.printStackTrace();
        } finally {
        }
    }

    public void a() {
        b();
    }

    public void b() {
        a();
    }

//假设在这个方法中处理不了这个异常，可以在方法中抛出；
    public  void  test(int a,int b)throws  ArithmeticException{
        if (b == 0) { //throw  throws
            throw new ArithmeticException();//主动抛出异常,一般在方法中使用；
        }
        System.out.println(a / b);
    }

}
