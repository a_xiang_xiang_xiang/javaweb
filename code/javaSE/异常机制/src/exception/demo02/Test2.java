package exception.demo02;

public class Test2 {
    public static void main(String[] args) {
        int a = 10;
        int b = 0;

//      选中内容；如：选中他“System.out.println(a / b);”  ctrl + alt + t =======>自动生成异常代码块

        try {
            System.out.println(a / b);
        } catch (Exception e) {
            System.exit(0); //结束程序
            e.printStackTrace();//打印错误的栈信息
            /*
            java.lang.ArithmeticException: / by zero
	at exception.demo02.Test2.main(Test2.java:11)
             */
        }
    }
}
