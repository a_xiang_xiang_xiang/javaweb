package array;

public class Demo04 {

    final static int[] arrays = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    public static void main(String[] args) {
        printArray(arrays);

        int[] reverse = reverse(arrays);
        System.out.println(reverse);//[I@1b6d3586
        printArray(reverse);//10	9	8	7	6	5	4	3	2	1
    }

    //    打印所有
    public static void printArray(int[] arrays) {
        for (int array : arrays) {
            System.out.print(array + "\t");// 1	   2	3	4	5	6	7	8	9	10
        }
        System.out.println();
    }

    //    反转数组
    public static int[] reverse(int[] arrays) {
        int[] result = new int[arrays.length];

        for (int i = 0, j = result.length - 1; i < arrays.length; i++, j--) {
            result[j] = arrays[i];
        }
        return result;
    }

}
