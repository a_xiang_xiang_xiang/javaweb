package array;

public class Demo01 {
    public static void main(String[] args) {
        test();


        test3();
    }

    public static void test() {
        int[] num1;//定义--------------声明数组
        num1 = new int[10];//这里边可存放10个int类型的数据--------------------创建数组

//        分配 -----------------------给数组元素赋值
        num1[0] = 1;
        num1[1] = 2;
        num1[2] = 3;
        num1[3] = 4;
        num1[4] = 5;
        num1[5] = 6;
        num1[6] = 7;
        num1[7] = 8;
        num1[8] = 9;
        num1[9] = 10;

        System.out.println(num1[9]);//10

//        计算和；
        int sum = 0;
        for (int i = 0; i < num1.length; i++) {
            sum += num1[i];
            System.out.print(i + "\t");//0	1	2	3	4	5	6	7	8	9
        }

        System.out.println();
        System.out.println(sum);//55


    }

    public  static  void  test2(){
        int num2[];
    }

    public  static  void  test3(){
        int[] num3 = new  int[20];

        for (int i = 0; i < num3.length ; i++) {
            System.out.println("第"+i+"个元素是\t"+num3[i]);
        }
    }
}
