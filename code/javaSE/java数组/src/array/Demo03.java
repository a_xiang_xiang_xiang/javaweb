package array;

public class Demo03 {
    public static void main(String[] args) {
        int[] arrays = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
        test();
//打印数组元素
        printArray(arrays);
//反转操作
        int[] reverse = reverse(arrays);
        System.out.println(reverse.toString());//[I@1b6d3586
        printArray(reverse);//14	13	12	11	10	9	8	7	6	5	4	3	2	1
    }

    //    反转数组
    public static int[] reverse(int[] arrays) {
        int[] result = new int[arrays.length];

//反转操作
        for (int i = 0, j = result.length - 1; i < arrays.length; i++, j--) {
            result[j] = arrays[i];
        }
        return result;
    }

    public static void test() {
        int[] arrays = {1, 2, 3, 4, 5, 67, 8, 9, 10, 11, 12, 13, 41};

//增强性 for循环
        for (int array : arrays) {
            System.out.print(array + "\t");//1	2	3	4	5	67	8	9	10	11	12	13	41
        }
        System.out.println();
    }


    //打印数组元素
    public static void printArray(int[] arrays) {
        for (int i = 0; i < arrays.length; i++) {
            System.out.print(arrays[i] + "\t");//1	2	3	4	5	6	7	8	9	10	11	12	13	14
        }
        System.out.println();
    }
}
