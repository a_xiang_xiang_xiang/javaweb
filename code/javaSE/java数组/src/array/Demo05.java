package array;

public class Demo05 {
    //    4行2列
//    [4][2]
    /*
    1,2     array[0]
    2,3     array[1]
    3,4     array[2]
    4,5     array[3]
     */
    final static int[][] array = {{1, 2}, {2, 3}, {3, 4}, {4, 5}};

    public static void main(String[] args) {
        System.out.println(array[0].length);//2
        System.out.println(array[0].toString());//[I@1b6d3586

        System.out.println(array[0][0]);  //1
        System.out.println(array[0][1]);  //2

        System.out.println(array.length); //4


        printArray(array[0]);//1	2
        System.out.println();
        System.out.println("===============");

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j <array[i].length ; j++) {
                System.out.print(array[i][j]+"\t");
                /**
                 * 1	2
                 * 2	3
                 * 3	4
                 * 4	5
                 */
            }
            System.out.println();
        }
    }

//    打印所有
    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]+"\t");
        }
    }


//    循环
    // 这儿有bug   待解决；
    public static  void  test(int[] arrays){
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
//                System.out.print([i][j]+"\t");
            }

        }

    }
}
