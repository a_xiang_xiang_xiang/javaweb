package com.xiang.mapper;

import com.xiang.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

public class MyTest {
    @Test
    public  void  test(){
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        try (SqlSession session = sqlSessionFactory.openSession()) {
            User user = (User) session.selectOne("com.xiang.mapper.UserMapper.selectUser", 2);
            User user2 = (User) session.selectOne("com.xiang.mapper.UserMapper.selectUser", 1);
            System.out.println("name："+ user.getName());
            System.out.println("name:"+ user2.getName());
        }

    }
}
