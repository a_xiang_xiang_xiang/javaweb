package cn.com.scitc.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Xiang
 */
// @WebServlet(urlPatterns = {"/home","/","index"})
@WebServlet(urlPatterns = "/home")
public class HelloServlet  extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.printf("Hello Servlet");



        // req.getRequestDispatcher("index.jsp").forward(req,resp);
        req.getRequestDispatcher("/home.jsp").forward(req,resp);

    }

}
