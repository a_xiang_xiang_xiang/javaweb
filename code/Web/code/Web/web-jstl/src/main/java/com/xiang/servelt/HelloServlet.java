package com.xiang.servelt;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Xiang
 */
@WebServlet(urlPatterns = "/")
public class HelloServlet  extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //传到jsp
        String name="小小";
        Integer age=20;
        req.setAttribute("lastName",name);
        req.setAttribute("lastAge",age);

        req.getRequestDispatcher("index1.jsp").forward(req,resp);
    }
}
