    <%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <!DOCTYPE html>
    <html>
    <head>
        <title>JSP - Hello World</title>
    </head>
    <body>

    <h1><%= "Hello World!" %>
    <%--引入jstl标签库--%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix="a" uri="http://java.sun.com/jsp/jstl/core" %>
        <style>
            span{
                color: red;
                font-size: xx-large;
            }
            #top{margin-bottom: 100px}
        </style>

    <hr>
        <p>
            当前用户：
            <span>
                <c:out value="${lastName}"/>
            </span>，
            年龄：
            <span>
                <c:out value="${lastAge}"/>
            </span>.
        </p>
    <hr>
    </h1>
    <c:set var="name" value="小向"/>
    <p>
        我的名字：
        <span>
    <%--        输出--%>
            <c:out value="${name}"/>
    <%--        删除--%>
            <c:remove var="name"/>
        </span>
        <br>
        <span>
            <c:out value="i name ${name}"/>
        </span>
        <br>
        <span>
            <c:out value="name${name}"/>
        </span>
    </p>
    <hr>
    <%--if--%>
    <c:set var="score" value="90"/>
    <c:if test="${score >=90}">
        优秀
    </c:if>
    <hr>
    <%--choose  otherwise--%>
    <c:choose>
        <c:when test="${score>=90}">优秀</c:when>
        <c:when test="${score>=70}">良好</c:when>
        <c:when test="${score>=60}">及格</c:when>
        <c:otherwise>不及格</c:otherwise>
    </c:choose>
     <hr>
    <c:set var="gender" value="1"/>
    <c:choose>
        <c:when test="${gender==1}">男</c:when>
        <c:when test="${gender==0}">女</c:when>
    </c:choose>
    <hr>

    <c:set var="gender" value="0"/>
<%--    <c:if test="${gender==1}">checked</c:if>--%>
<%--    <c:choose>--%>
<%--        <c:when test="${gender==1}">男</c:when>--%>
<%--        <c:when test="${gender==0}">女</c:when>--%>
<%--    </c:choose>--%>
<%--    <c:out value="checked"/>--%>
    <form>
        <div>
            <label>性别：</label>
            <input type="radio" name="gender" value="${gender==1}"  <c:if test="${gender==1}">checked</c:if> >男
            <input type="radio" name="gender" value="${gender==0}"  <c:if test="${gender==0}">checked</c:if>>女
        </div>
    </form>

    <c:forEach var="i" begin="1" end="5">
        序号：<c:out value="${i}"/>
        <br>
    </c:forEach>

    <table border="1">
        <tr>
            <th>id</th>
            <th>name</th>
            <th>sex</th>
            <th>gender</th>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>

    </table>

    <hr>

<%--    <c:forEach var="i" items="${res}" varStatus="status">--%>
<%--        <c:if test="${status.count%3==1}">--%>
<%--            <tr>--%>

<%--            </tr>--%>
<%--        </c:if>--%>
<%--    </c:forEach>--%>

    <hr>

<%--    //两列的--%>
    <c:forEach var="bean" items="${result}" varStatus="status">
        <c:if test="${status.count%2==1}">
            <tr>
        </c:if>
        <td><b>$...{bean.id}</b>:$...{bean.name}</td>
        <c:if test="${status.count%2==0}">
            </tr>
        </c:if>
        <c:set var ="v_count" value="${status.count}"/>
    </c:forEach>
    <c:if test="${v_count%2==1}">
        <td>您好</td>
        </tr>
    </c:if>
    <hr>
    <%
        int h=3;//行数
        int l=3;//列数
    %>
    <table>
        <%
            for(int i=0;i<h;i++){
        %>
        <tr>
            <%
                for(int j=0;j<l;j++){
            %>
            <td></td>
            <%}%>
        </tr>
        <%}%>
    </table>
    <hr>
    <table>
        <%for(int i=0;i<5;i++){%>
        <tr>
            <%  for(int j=0;j<3;j++){%>
            <td>1</td>
            <%}%>
        </tr>
        <%}%>
    </table>
    <hr>

    <span>f1</span>
    <c:set var="i" value="1"/>
    <c:out value="f${i}"/>


    <hr>
<%--    使用JSTL循环创建一个3列5行的表格--%>
    <table border="1">
        <c:forEach var="i" begin="1" end="5">
            <tr>
                <c:forEach var="j" begin="1" end="3">
                    <td>123</td>
                </c:forEach>
            </tr>
        </c:forEach>
    </table>

    <hr>
<%--    判断性别--%>
    <c:set var="sex" value="1"/>
<%--    <c:if test="${gender==1}">checked</c:if>--%>
    <form>
        <div>
            <label>性别：</label>
            <input type="radio" name="sex" value="1" <c:if test="${sex==1}">checked</c:if>>男
            <input type="radio" name="sex" value="0" <c:if test="${sex==0}">checked</c:if>>女
        </div>
    </form>


<%--    <c:url> 标签--%>
    <c:url value="https://www.baidu.com/"/>链接1
    <br>
    <a href="https://www.baidu.com/">链接2</a>
    <br>
    <a href="<c:url value="https://www.baidu.com/"/> "> &lt;c:url&gt; 标签生成。</a>


    <div id="top"></div>
    <br/>
    <hr>
    <%--使用可选的查询参数来创造一个URL--%>
    <a href="<c:url value="http://www.runoob.com"/>">
        这个链接通过 &lt;c:url&gt; 标签生成。
    </a>
    <hr>
    <%--重定向至一个新的URL.--%>
    <button type="button" class="btn">重定向至一个新的URL
        <script type="text/javascript">
            let btn=document.querySelector(".btn");
            btn.addEventListener('click',function () {
                alert('hello,world')
    <%--            <c:redirect url="http://www.runoob.com"/>--%>
            },false);
            btn.addEventListener('click',function () {
                alert(this.btn)
            },false);

            // document.getElementById("btn").onclick(alert(123));
            // function Fn(){
            //     alert(重定向至一个新的URL)
    <%--            <c:redirect url="http://www.runoob.com"/>--%>
    //         }
        </script>
    <%--<c:redirect url="http://www.runoob.com"/>--%>
    </button>
    <hr>
    <%--用来给包含或重定向的页面传递参数--%>
    <c:url var="URL" value="index1.jsp">
        <c:param name="name" value="Runoob"/>
        <c:param name="url" value="www.runoob.com"/>
    </c:url>
    <a href="/<c:out value="${URL}"/>">使用 &lt;c:param&gt; 为指定URL发送两个参数。</a>
    <hr>
    <%--<c:forEach>, <c:forTokens> 标签--%>
    <c:forEach var="i" begin="1" end="5">
        item<c:out value="${i}"/>
    </c:forEach>
    <br>
    <c:forTokens items="JavaEE,JavaWeb,linux,Html,JavaScript" delims="," var="name">
        <c:out value="${name}"/>
        <p>
    </c:forTokens>
    <hr>
    <%--检索一个绝对或相对 URL，然后将其内容暴露给页面--%>
    <c:import url="https://www.baidu.com/" var="data"/>
    <c:out value="${data}"/>
    <hr>
    <%--<c:choose>, <c:when>, <c:otherwise> 标签--%>
    <c:set value="${2000*2}" scope="session" var="salary"/>
    <span>
        工资为：<c:out value="${salary}"/>
    </span>
    <c:choose>
        <c:when test="${salary<=0}">
            没有工资
        </c:when>
        <c:when test="${salary >=2000}">
            还行，工资够吃饭，
        </c:when>
        <c:otherwise>
            什么也没有
        </c:otherwise>
    </c:choose>
    <hr>
    <%--处理产生错误的异常状况，并且将错误信息储存起来--%>
    <c:catch var="catchE">
        <%int x=5/0;%>
    </c:catch>
    <c:if test="${catchE != null}">
        <span>异常为：${catchE}</span>
        <br>
        <span>发生了异常：${catchE.message}</span>
    </c:if>
    <hr>
    <%--删除数据--%>
    <c:set value="${3*2}" scope="session" var="salary"/>
    <span>
        salary:变量值<c:out value="${salary}"/>
    </span>
    <br>
    <c:remove var="salary"/>
    <span>
        删除salary变量后的值为<c:out value="${salary}"/>
    </span>
    <hr>
    <%--保存数据--%>
    <c:set var="sawe" scope="session" value="${10*20}"/>
    <span>sawe:的值为

        <c:out value="${sawe}"/>
    </span>
    <hr>
    <%--if判断--%>
    <c:set var="salary" scope="session" value="${2000*2}"/>
    <c:if test="${salary>2000}">
        <span>工资为：<c:out value="${salary}"/></span>
    </c:if>
    <hr>
    <%--输出数据    --%>
    <c:out value="&lt要显示的数据对象（未使用转义字符）&gt" escapeXml="true" default="默认值"></c:out><br/>
    <c:out value="&lt要显示的数据对象（使用转义字符）&gt" escapeXml="false" default="默认值"></c:out><br/>
    <c:out value="${null}" escapeXml="false">使用的表达式结果为null，则输出该默认值</c:out><br/>
    <c:out value="${null}" escapeXml="false" default="使用的表达式结果为null"></c:out><br/>
    <%--<c:out value="dd" default="xxxx"/>--%>
    <hr>
    <%--错误写法--%>
    <%--<c:out value="${null}" escapeXml="false" default="使用的表达式结果为null">123</c:out><br/>--%>
    <c:forEach begin="1" end="5" var="1">
        <h1>我是一个中国人</h1>
    </c:forEach>
    </body>
    </html>