package com.xiang.dao;

import com.xiang.model.Manager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ManagerDaoTest {

    @Test
    void selectByPrimaryKey() {
    }

    @Test
    void findByLoginId() {
        ManagerDao dao = new ManagerDao();
        Manager manager = dao.findByLoginId("xiang");
        Assertions.assertEquals("小向",manager.getRealname());
        System.out.println(manager.getRealname());
    }
}