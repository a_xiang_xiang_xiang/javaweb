package com.xiang.dao;

import com.xiang.mapper.ManagerMapper;
import com.xiang.model.Manager;
import org.apache.ibatis.session.SqlSession;

public class ManagerDao {
    public Manager selectByPrimaryKey(Integer id){
        try(SqlSession session = MybatisUtil.getsession();)
        {
            ManagerMapper mapper = session.getMapper(ManagerMapper.class);
            return mapper.selectByPrimaryKey(id);
        }
    }


    public Manager findByLoginId(String loginId){
        try(SqlSession session = MybatisUtil.getsession();)
        {
            ManagerMapper mapper = session.getMapper(ManagerMapper.class);
            return mapper.findByLoginId(loginId);
        }
    }
}
