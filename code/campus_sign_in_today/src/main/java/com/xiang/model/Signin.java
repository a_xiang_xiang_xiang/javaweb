package com.xiang.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Signin implements Serializable {
    private Integer id;

    private String studentid;

    private Date signdate;

    private BigDecimal temperature;

    private Integer working;

    private Integer hadtravel;

    private String address;

    private Integer jkt;

    private String jktcolor;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }

    public Date getSigndate() {
        return signdate;
    }

    public void setSigndate(Date signdate) {
        this.signdate = signdate;
    }

    public BigDecimal getTemperature() {
        return temperature;
    }

    public void setTemperature(BigDecimal temperature) {
        this.temperature = temperature;
    }

    public Integer getWorking() {
        return working;
    }

    public void setWorking(Integer working) {
        this.working = working;
    }

    public Integer getHadtravel() {
        return hadtravel;
    }

    public void setHadtravel(Integer hadtravel) {
        this.hadtravel = hadtravel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getJkt() {
        return jkt;
    }

    public void setJkt(Integer jkt) {
        this.jkt = jkt;
    }

    public String getJktcolor() {
        return jktcolor;
    }

    public void setJktcolor(String jktcolor) {
        this.jktcolor = jktcolor;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", studentid=").append(studentid);
        sb.append(", signdate=").append(signdate);
        sb.append(", temperature=").append(temperature);
        sb.append(", working=").append(working);
        sb.append(", hadtravel=").append(hadtravel);
        sb.append(", address=").append(address);
        sb.append(", jkt=").append(jkt);
        sb.append(", jktcolor=").append(jktcolor);
        sb.append("]");
        return sb.toString();
    }
}