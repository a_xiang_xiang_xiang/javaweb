package com.xiang.mapper;

import com.xiang.model.Manager;
import java.util.List;

public interface ManagerMapper {
//    返回manager
    Manager findByLoginId(String loginId);

    int deleteByPrimaryKey(Integer id);

    int insert(Manager record);

    Manager selectByPrimaryKey(Integer id);

    List<Manager> selectAll();

    int updateByPrimaryKey(Manager record);
}