
<%--
  Created by IntelliJ IDEA.
  User: Xiang
  Date: 2021/4/23
  Time: 11:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="<c:url value="/webjars/bootstrap/4.6.0/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/css/manager.css"/>">
    <script src="<c:url value="/webjars/jquery/3.6.0/dist/jquery.js"/>"></script>
    <script src=" <c:url value="/webjars/bootstrap/4.6.0/css/bootstrap.min.css"/>"> </script>
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
</head>
<body>
    <form class="form-signin needs-validation" novalidate>
        <div class="text-center mb-4">
            <img class="mb-4" src="<c:url value="/images/logo.jpg"/> " alt="" width="72" height="72">
            <h1 class="h3 mb-3 font-weight-normal">今日校园</h1>
            <p>
                每天打卡签到
            </p>
        </div>

        <div class="form-label-group">
            <input type="text" id="loginId" class="form-control" placeholder="登录名" pattern="(\w|\W|\d){4,10}" required autofocus>
            <label for="loginId">登录名</label>
            <div class="valid-feedback">
                登录名验证通过
            </div>
            <div class="invalid-feedback">
                登录名必填，4-10个字符
            </div>
        </div>

        <div class="form-label-group">
            <input type="password" id="password" class="form-control"  placeholder="密码" pattern=".{6,20}" required="">
            <label for="password">密码</label>
            <div class="valid-feedback">
                密码验证通过
            </div>
            <div class="invalid-feedback">
                登录名必填，6-20个字符
            </div>
        </div>

<%--        <div class="checkbox mb-3">--%>
<%--            <label>--%>
<%--                <input type="checkbox" value="remember-me"> Remember me--%>
<%--            </label>--%>
<%--        </div>--%>
        <button class="btn btn-lg btn-primary btn-block" type="submit">登录</button>
        <p class="mt-5 mb-3 text-muted text-center">© 2021-2021</p>
    </form>

    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
</body>
</html>
