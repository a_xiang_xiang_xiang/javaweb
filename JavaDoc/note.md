# 生成 java 帮助文档

## 1，编写代码

```java
public class Hello{
	/**
	*@author xiang
	*@param args  null
	*@since 1.0
	*@throws  null
	*/
	public static void main(String[] args){
		System.out.println("Hello,world");
	}
}
```

## 2,打开 cmd 编译代码  

```properties
C:\Users\Xiang\Desktop\JavaDoc>javac Hello.java

C:\Users\Xiang\Desktop\JavaDoc>java Hello
Hello,world
```

## 3,生成 JavaDoc 帮助文档

```properties
C:\Users\Xiang\Desktop\JavaDoc>javadoc -encoding UTF-8 -charset UTF-8 Hello.java
正在加载源文件Hello.java...
正在构造 Javadoc 信息...
标准 Doclet 版本 1.8.0_281
正在构建所有程序包和类的树...
正在生成.\Hello.html...
Hello.java:6: 错误: 意外的文本
        *@throws  null
         ^
正在生成.\package-frame.html...
正在生成.\package-summary.html...
正在生成.\package-tree.html...
正在生成.\constant-values.html...
正在构建所有程序包和类的索引...
正在生成.\overview-tree.html...
正在生成.\index-all.html...
正在生成.\deprecated-list.html...
正在构建所有类的索引...
正在生成.\allclasses-frame.html...
正在生成.\allclasses-noframe.html...
正在生成.\index.html...
正在生成.\help-doc.html...
1 个错误

C:\Users\Xiang\Desktop\JavaDoc>
```

## 4,生成文件

![image-20210804090600878](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210804090600878.png)

![image-20210804090648867](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210804090648867.png)





# 命令行传参

## 1，编写代码

```java
public class Hello{
	/**
	*@author xiang
	*@param args  null
	*@since 1.0
	*@throws  null
	*/
	public static void main(String[] args){
		for(int i = 0;i < args.length;i++){
			System.out.println(args[i]);
		}
		//System.out.println("Hello,world");
	}
}
```



## 2，运行结果 

![image-20210804125352205](C:\Users\Xiang\AppData\Roaming\Typora\typora-user-images\image-20210804125352205.png)
