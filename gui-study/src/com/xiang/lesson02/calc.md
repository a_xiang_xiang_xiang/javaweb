# GUI 实现简易计算器
## 面向过程写法 有 bug
```java
package com.xiang.lesson02;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

//简易 计算器
public class TestCalc {
    public static void main(String[] args) {
        new Calculator();
    }
}

//计算器 类
class Calculator extends Frame {
    public Calculator() {

//        3个文本框
        TextField num1 = new TextField(10); //字符数 最大能10个字符
        TextField num2 = new TextField(10); //字符数 最大能10个字符
        TextField num3 = new TextField(20); //字符数 最大能10个字符
//        一个按钮
        Button button = new Button("=");
        button.addActionListener(new MyCalculatorListener(num1, num2, num3));
//        一个标签
        Label label = new Label("+");
//        布局
        setLayout(new FlowLayout());

        add(num1);
        add(label);
        add(num2);
        add(button);
        add(num3);

        pack();
        setVisible(true);
        setBackground(Color.yellow);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

    }
}

//监听器 类
class MyCalculatorListener implements ActionListener {
    //    需获取 3个变量
    private TextField num1, num2, num3;

    public MyCalculatorListener(TextField num1, TextField num2, TextField num3) {
        this.num1 = num1;
        this.num2 = num3;
        this.num3 = num3;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
//        1,获取加数和被加数
        int n1 = Integer.parseInt(num1.getText());
        int n2 = Integer.parseInt(num2.getText());
//        2, 加法运算后，放入第三个框
//        Integer.parseInt()
//        num3.setText(String.valueOf(n1+n2));
//        num3.setText(String.valueOf(n2 + n1));
//        int n3=Integer.parseInt(num3.setText(String.valueOf(n1+n2)));
        String n3 = String.valueOf(Integer.parseInt(String.valueOf(n1+n2)));
        num3.setText(n3);
//        3，清除前两个框
        num1.setText("");
        num2.setText("");
    }
}
```
## 改造为面向对象写法
```java
package com.xiang.lesson02;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

//简易 计算器
public class TestCalc {
    public static void main(String[] args) {
        new Calculator().loadFrame();
    }
}

//计算器 类
class Calculator extends Frame {

    //属性
    TextField num1, num2, num3;

    //方法
    public void loadFrame() {
//        3 个文本框
        num1 = new TextField(10); //字符数 最大能10个字符
        num2 = new TextField(10); //字符数 最大能10个字符
        num3 = new TextField(20); //字符数 最大能10个字符
//        一个按钮
        Button button = new Button("=");
        button.addActionListener(new MyCalculatorListener(this));
//        一个标签
        Label label = new Label("+");
//        布局
        setLayout(new FlowLayout());

        add(num1);
        add(label);
        add(num2);
        add(button);
        add(num3);

        pack();
        setVisible(true);
        setBackground(Color.yellow);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }


}

//监听器 类
class MyCalculatorListener implements ActionListener {
    //    需获取计算器这个对象,在一个类中组合另外一个类
    Calculator calculator = null;

    public MyCalculatorListener(Calculator calculator) {
        this.calculator = calculator;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
//        1,获取加数和被加数
//        2, 加法运算后，放入第三个框
        int n1 = Integer.parseInt(calculator.num1.getText());
        int n2 = Integer.parseInt(calculator.num2.getText());
        calculator.num3.setText("" + (n1 + n2));
        //        3，清除前两个框
//        calculator.num1.setText("");
//        calculator.num2.setText("");

    }
}
```
## 使用内部类
```java
package com.xiang.lesson02;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

//简易 计算器
public class TestCalc {
    public static void main(String[] args) {
        new Calculator().loadFrame();
    }
}

//计算器 类
class Calculator extends Frame {

    //属性
    TextField num1, num2, num3;

    //方法
    public void loadFrame() {
//        3 个文本框
        num1 = new TextField(10); //字符数 最大能10个字符
        num2 = new TextField(10); //字符数 最大能10个字符
        num3 = new TextField(20); //字符数 最大能10个字符
//        一个按钮
        Button button = new Button("=");
        button.addActionListener(new MyCalculatorListener());
//        一个标签
        Label label = new Label("+");
//        布局
        setLayout(new FlowLayout());

        add(num1);
        add(label);
        add(num2);
        add(button);
        add(num3);

        pack();
        setVisible(true);
        setBackground(Color.yellow);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

    //监听器 类
//    重构
//    内部类 畅通无阻的访问外部类；
    private class MyCalculatorListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
//        1,获取加数和被加数
//        2, 加法运算后，放入第三个框
            int n1 = Integer.parseInt(num1.getText());
            int n2 = Integer.parseInt(num2.getText());
            num3.setText("" + (n1 + n2));
            //        3，清除前两个框
//        calculator.num1.setText("");
//        calculator.num2.setText("");

        }
    }

}


```