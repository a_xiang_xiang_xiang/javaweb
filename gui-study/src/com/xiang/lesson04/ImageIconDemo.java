package com.xiang.lesson04;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

public class ImageIconDemo extends JFrame {
    public ImageIconDemo() {
        JLabel label = new JLabel("ImageIconDemo");
//        获取图片地址
//        这个类获取 当前类路径下的东西
        URL url = ImageIconDemo.class.getResource("wallpaper.jpg");

        ImageIcon icon = new ImageIcon(url);
        label.setIcon(icon);
        label.setHorizontalAlignment(SwingConstants.CENTER);

        Container container = getContentPane();
        container.add(label);

        setVisible(true);
        setBounds(50,50,1000,800);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    public static void main(String[] args) {
        new ImageIconDemo();
    }
}
