package com.xiang.lesson04;

import javax.swing.*;
import java.awt.*;

//icon 图标是一个接口，需要实现类，Frame 继承
public class IconDemo extends JFrame implements Icon {
    private int width;
    private int height;

    //    无参
    public IconDemo() {
    }

    //    有参
    public IconDemo(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void init() {
        IconDemo iconDemo = new IconDemo(12, 12);
        JLabel label = new JLabel("iconTest", iconDemo , SwingConstants.CENTER);
        Container container = getContentPane();
        container.add(label);

        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
    }

    public static void main(String[] args) {
        new IconDemo().init();

    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
//画图标
        g.fillOval(x, y, width, height);
    }

    @Override
    public int getIconWidth() {
        return this.width;
    }

    @Override
    public int getIconHeight() {
        return this.height;
    }
}
