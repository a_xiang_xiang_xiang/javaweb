package com.xiang.lesson04;

import javax.swing.*;
import java.awt.*;

//开始学习 GUI ---Swing
public class JFrameDemo {
    //   init() 初始化
    private void init() {
        JFrame frame = new JFrame("这是一个jFrame 窗口");
        frame.setVisible(true);
        frame.setBackground(Color.cyan);
        frame.setBounds(200, 200, 400, 400);
//设置文字
        JLabel jLabel = new JLabel("欢迎来到 GUI -Swing的学习");
        frame.add(jLabel);


//        关闭事件
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new JFrameDemo().init();
    }
}
