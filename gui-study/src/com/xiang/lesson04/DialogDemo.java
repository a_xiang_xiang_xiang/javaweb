package com.xiang.lesson04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//JDialog  弹窗
public class DialogDemo extends JFrame {
    public DialogDemo(String title) {
        super(title);
        setVisible(true);
        setBackground(Color.yellow);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(200, 200, 300, 300);

//        JFrame 放东西，容器
        Container container = getContentPane();
//        绝对布局
        container.setLayout(null);
//        按钮
        JButton button = new JButton("点击弹出对话框");
        button.setBounds(50, 50, 180, 60);
        button.setBackground(Color.red);

//        点击 这个按钮，弹出弹窗 监听事件
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                弹窗
                new MyDialogDemo();
            }
        });

//        把这个东西加到容器里边
        container.add(button);
    }

    public static void main(String[] args) {
        new DialogDemo("弹窗");
    }
}

//弹出窗口
class MyDialogDemo extends JDialog {
//    JDialog  弹窗 默认有关闭事件
    public MyDialogDemo() {
        setVisible(true);
        setBackground(Color.magenta);
//        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(600, 500, 300, 200);

        Container container = getContentPane();
        container.setLayout(null);

        container.add(new Label("study ---------java"));
    }
}
