package com.xiang.lesson04;

import javax.swing.*;
import java.awt.*;

public class JFrameDemo02 {
    public static void main(String[] args) {
        new MyJFrame().init("MyJFrame");
    }
}

class MyJFrame extends JFrame {
    public void init(String title) {
        this.setTitle("MyJFrame");
        this.setVisible(true);
        this.setBounds(200, 200, 400, 400);
//获取 一个容器
        Container pane = this.getContentPane();
        pane.setBackground(Color.yellow);

        JLabel jLabel = new JLabel("欢迎来到 GUI -Swing的学习");
        this.add(jLabel);
//      文本居中，设置文本水平对齐方式 --setHorizontalAlignment
        jLabel.setHorizontalAlignment(SwingConstants.CENTER);
    }
}
