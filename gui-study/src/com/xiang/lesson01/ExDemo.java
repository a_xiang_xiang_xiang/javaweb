package com.xiang.lesson01;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

//练习
public class ExDemo {
    public static void main(String[] args) {
        Frame frame = new Frame("ExDemo");
//        2行1列
        frame.setLayout(new GridLayout(2,1));

        Panel panel1 = new Panel(new BorderLayout());
        Panel panel2 = new Panel(new GridLayout(2,1));
        Panel panel3 = new Panel(new BorderLayout());
        Panel panel4 = new Panel(new GridLayout(2,2));

        panel1.add(new Button("East-1"),BorderLayout.EAST);
        panel1.add(new Button("West-1"),BorderLayout.WEST);
        panel2.add(new Button("panel2-btn-1"));
        panel2.add(new Button("panel2-btn-2"));
        panel1.add(panel2,BorderLayout.CENTER);

        panel3.add(new Button("East-2"),BorderLayout.EAST);
        panel3.add(new Button("West-2"),BorderLayout.WEST);
        for (int i = 0; i < 4; i++) {
            panel4.add(new Button("for-"+i));
        }
        panel3.add(panel4,BorderLayout.CENTER);


        frame.add(panel3);
        frame.add(panel1);

        frame.setVisible(true);
        frame.setBounds(200,200,580,600);
        frame.setBackground(Color.magenta);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }
}
