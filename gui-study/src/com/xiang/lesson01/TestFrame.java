package com.xiang.lesson01;

import java.awt.*;

//GUI 第一个界面
public class TestFrame {
    public static void main(String[] args) {
//         frame
        Frame frame = new Frame("我的第一个java图形界面窗口");
//        设置可见性
        frame.setVisible(true);
//        窗口大小
        frame.setSize(400,400);
//        设置背景颜色
        frame.setBackground(Color.pink);
//        弹出的初始位置
        frame.setLocation(200,200);
//        设置大小固定
        frame.setResizable(false);
    }
}
