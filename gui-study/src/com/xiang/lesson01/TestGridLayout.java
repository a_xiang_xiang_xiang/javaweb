package com.xiang.lesson01;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

//表格布局
public class TestGridLayout {
    public static void main(String[] args) {
        Frame frame = new Frame("TestGridLayout");

        Button but01 = new Button("but01");
        Button but02 = new Button("but02");
        Button but03 = new Button("but03");
        Button but04 = new Button("but04");
        Button but05 = new Button("but05");
        Button but06 = new Button("but06");

        frame.add(but06);
        frame.add(but05);
        frame.add(but04);
        frame.add(but03);
        frame.add(but03);
        frame.add(but02);
        frame.add(but01);

        frame.setLayout(new GridLayout(4, 4, 5, 5));
        frame.pack();//java 函数 ；
        frame.setBounds(400, 400, 400, 400);
        frame.setVisible(true);

        frame.setBackground(Color.green);

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }
}
