package com.xiang.lesson01;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

//流式布局
public class TestFlowLayout {
    public static void main(String[] args) {
        Frame frame = new Frame();

//        按钮 组件
        Button button1 = new Button("but01");
        Button button2 = new Button("but02");
        Button button3 = new Button("but03");
        Button button4 = new Button("but04");
//设置为流式布局
//        frame.setLayout(new FlowLayout());//居中
//        frame.setLayout(new FlowLayout(FlowLayout.LEFT));//居左
        frame.setLayout(new FlowLayout(FlowLayout.RIGHT));//居左

        frame.setBounds(400,400,400,400);
//        添加按钮
        frame.add(button1);
        frame.add(button2);
        frame.add(button3);
        frame.add(button4);

        frame.setVisible(true);

        frame.addWindowListener(new WindowAdapter() {
            //窗口关闭要作的事情
            @Override
            public void windowClosing(WindowEvent e) {
//                结束程序
                System.exit(0);
            }
        });
    }

}
