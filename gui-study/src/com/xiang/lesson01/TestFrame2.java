package com.xiang.lesson01;

import java.awt.*;

public class TestFrame2 {
    public static void main(String[] args) {
        MyFrame myFrame1 = new MyFrame(100, 100, 400, 400, Color.yellow);
        MyFrame myFrame2 = new MyFrame(500, 100, 400, 400, Color.red);
        MyFrame myFrame3 = new MyFrame(100, 500, 400, 400, Color.pink);
        MyFrame myFrame4 = new MyFrame(500, 500, 400, 400, Color.green);
    }
}

class MyFrame extends  Frame{
    static  int id=0; //弹出 多个窗口

    public  MyFrame(int x,int y,int w,int h,Color color){
        super("MyFrame"+(++id));
        setVisible(true);
        setBounds(x,y,w,h);
        setBackground(color);

    }
}
