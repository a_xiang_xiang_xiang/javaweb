package com.xiang.lesson06;

import javax.swing.*;
import java.awt.*;

public class TestJTextFieldDemo extends JFrame {
    public  void  init (String title){
        super.setTitle("文本框");
    }
    public TestJTextFieldDemo() {
        Container container = getContentPane();
        container.setLayout(new GridLayout(2,2));
        JTextField field = new JTextField("hello");
        JTextField field2 = new JTextField("world",50);

        container.add(field);
        container.add(field2);


        setVisible(true);
        setBounds(100, 100, 300, 200);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new TestJTextFieldDemo().init("");
    }
}
