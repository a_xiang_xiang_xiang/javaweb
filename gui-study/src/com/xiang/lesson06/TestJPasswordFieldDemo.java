package com.xiang.lesson06;

import org.w3c.dom.Node;

import javax.swing.*;
import java.awt.*;

//密码框
public class TestJPasswordFieldDemo extends JFrame {
    public void init(String title) {
        super.setTitle("密码框");
    }

    public TestJPasswordFieldDemo() {
        Container container = getContentPane();

//        密码框
        JPasswordField field = new JPasswordField();
        field.setEchoChar('*');

        container.add(field);

        setVisible(true);
        setBounds(100, 100, 300, 200);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new TestJPasswordFieldDemo().init("");
    }
}
