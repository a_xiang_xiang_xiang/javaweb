package com.xiang.lesson06;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

//new JList();
//列表
public class TestJComboBoxDemo extends JFrame {
    public TestJComboBoxDemo() {
        Container container = getContentPane();

//        生成列表内容
//        String[] contents = {"1", "2", "3", "4", "5", "6", "7", "8", "9"}; //存放静态变量
//列表中需要放入内容，放在构造器里边
//        JList list = new JList(contents);
//        添加列表

        Vector contents = new Vector();
        JList list = new JList(contents);

//        动态添加；
        contents.add("用户名");
        contents.add("李四");
        contents.add("王五");
        contents.add("张三");
        contents.add("王二");

        container.add(list);

        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(100, 100, 200, 300);
    }

    public static void main(String[] args) {
        new TestJComboBoxDemo();
    }
}
