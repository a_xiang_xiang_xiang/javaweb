package com.xiang.lesson06;

import javax.swing.*;
import java.awt.*;

//下拉 JComboBox
public class TestDropDownDemo extends JFrame {
    public TestDropDownDemo() {
        Container container = getContentPane();

        JComboBox status = new JComboBox();
        status.addItem("请选择");
        status.addItem("即将上映");
        status.addItem("热映影片");
        status.addItem("新热预告");


        container.add(status);

        setVisible(true);
        setBounds(100, 100, 150, 130);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JLabel study = new JLabel("study");
        study.setHorizontalAlignment(SwingConstants.CENTER);
//        container.add(study);
    }

    public static void main(String[] args) {
        new TestDropDownDemo();
    }
}
