package com.xiang.lesson05;

import javax.swing.*;
import java.awt.*;

public class JButtonDemo03 extends JFrame {
    public JButtonDemo03() {
        Container container = getContentPane();
//多选框
        JCheckBox jCheckBox01 = new JCheckBox("JCheckBox01");
        JCheckBox jCheckBox02 = new JCheckBox("JCheckBox02");



        container.add(jCheckBox01,BorderLayout.WEST);
        container.add(jCheckBox02,BorderLayout.EAST);


        setVisible(true);
        setBounds(100, 100, 300, 200);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new JButtonDemo03();
    }
}



