package com.xiang.lesson05;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

//图片按钮
public class JButtonDemo01 extends JFrame {
    public JButtonDemo01() {
        Container container = getContentPane();
//将一个图片变为图标
        URL url = JButtonDemo01.class.getResource("wallpaper.jpg");
        Icon icon = new ImageIcon(url);

//        把这个图标放在按钮上
        JButton jButton = new JButton();
        jButton.setIcon(icon);
        jButton.setToolTipText("这是一个图片按钮");

//        把这个按钮加到容器中
        container.add(jButton);

        setVisible(true);
        setBounds(100, 100, 500, 400);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new JButtonDemo01();
    }
}
