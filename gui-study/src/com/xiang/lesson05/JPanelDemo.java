package com.xiang.lesson05;

import javax.swing.*;
import java.awt.*;

// JPanel面板
public class JPanelDemo extends JFrame {
    public JPanelDemo() {
        Container container = getContentPane();
//        表格布局
        container.setLayout(new GridLayout(2, 1, 10, 10));

        JPanel p1 = new JPanel(new GridLayout(1, 3));
        JPanel p2 = new JPanel(new GridLayout(2, 1));
        JPanel p3 = new JPanel(new GridLayout(5, 5));
        JPanel p4 = new JPanel(new GridLayout(3, 1));

//        p1.add(new JButton("1"));
//        p1.add(new JButton("2"));
//        p1.add(new JButton("3"));

        for (int i = 0; i < 3; i++) {
            p1.add(new JButton("3"));
        }

        p2.add(new JButton("2"));
        p2.add(new JButton("2"));

//        p3.add(new JButton("3"));
//        p3.add(new JButton("3"));
//        p3.add(new JButton("3"));
//        p3.add(new JButton("3"));
//        p3.add(new JButton("3"));
//        p3.add(new JButton("3"));


        for (int i = 0; i < 25; i++) {
            p3.add(new JButton("3"));
        }

//        p4.add(new JButton("4"));
//        p4.add(new JButton("4"));
//        p4.add(new JButton("4"));

        for (int i = 0; i < 3; i++) {
            p4.add(new JButton("4"));
        }

        container.add(p1);
        container.add(p2);
        container.add(p3);
        container.add(p4);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
        setBounds(100, 100, 500, 500);
        setBackground(Color.pink);

    }

    public static void main(String[] args) {
        new JPanelDemo();
    }
}
