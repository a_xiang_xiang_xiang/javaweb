package com.xiang.lesson05;

import javax.swing.*;
import java.awt.*;

//滚动条 JScrollPane 面板
public class JScrollDemo extends JFrame {
    public JScrollDemo() {
        Container container = getContentPane();

//        文本域
        JTextArea textArea = new JTextArea(20, 10);
        textArea.setText("\tMachine reading comprehension (MRC) aims to teach machines to read and comprehend human\n" +
                "languages, which is a long-standing goal of natural language processing (NLP). With the burst of\n" +
                "deep neural networks and the evolution of contextualized language models (CLMs), the research\n" +
                "of MRC has experienced two significant breakthroughs. MRC and CLM, as a phenomenon,\n" +
                "have a great impact on the NLP community. In this survey, we provide a comprehensive and\n" +
                "comparative review on MRC covering overall research topics about 1) the origin and development\n" +
                "of MRC and CLM, with particular focus on the role of CLMs; 2) the impact of MRC and CLM\n" +
                "to the NLP community; 3) the definition, datasets, and evaluation of MRC; 4) general MRC\n" +
                "architecture and technical methods in the view of two-stage Encoder-Decoder solving archi\u0002tecture from the insights of the cognitive process of humans; 5) previous highlights, emerging\n" +
                "topics, and our empirical analysis, among which we especially focus on what works in different\n" +
                "periods of MRC researches. We propose a full-view categorization and new taxonomies on these\n" +
                "topics. The primary views we have arrived at are that 1) MRC boosts the progress from language\n" +
                "processing to understanding; 2) the rapid improvement of MRC systems greatly benefits from\n" +
                "the development of CLMs; 3) the theme of MRC is gradually moving from shallow text matching\n" +
                "to cognitive reasoning.");

//        JScrollPane 面板
        JScrollPane scrollPane = new JScrollPane(textArea);


        container.add(scrollPane);

        setVisible(true);
        setBounds(100, 100, 400, 200);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    public static void main(String[] args) {
        new JScrollDemo();
    }
}
