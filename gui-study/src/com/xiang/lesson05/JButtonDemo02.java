package com.xiang.lesson05;

import org.w3c.dom.Node;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

public class JButtonDemo02 extends JFrame {
    public JButtonDemo02() {
        Container container = getContentPane();

//        单选框
        JRadioButton button01 = new JRadioButton("JRadioButton01");
        JRadioButton button02 = new JRadioButton("JRadioButton02");
        JRadioButton button03 = new JRadioButton("JRadioButton03");
        JRadioButton button04 = new JRadioButton("JRadioButton04");

//        分组,一个组中只能选择一个
        ButtonGroup group = new ButtonGroup();
        group.add(button01);
        group.add(button02);
        group.add(button03);
        group.add(button04);

        container.add(button01,BorderLayout.NORTH);
        container.add(button03,BorderLayout.SOUTH);
        container.add(button02,BorderLayout.CENTER);
//        container.add(button04,BorderLayout.);


        setVisible(true);
        setBounds(100, 100, 400, 300);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new JButtonDemo02();
    }

}
