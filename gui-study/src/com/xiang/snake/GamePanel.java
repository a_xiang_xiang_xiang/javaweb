package com.xiang.snake;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Date;

//游戏面板
public class GamePanel extends JPanel implements KeyListener {
    //定义蛇的数据结构
    int length;//定义蛇的长度
    int[] snakeX = new int[600];
    int[] snakeY = new int[500];
    //    初始方向---向右
    String fx;
    //        游戏当前状态 开始，停止
    Boolean isStart = false;//默认不开始

    //    构造器 调用init() 访求
    public GamePanel() {
        init();
//        获得焦点与键盘事件
        this.setFocusable(true);//获取焦点事件
        this.addKeyListener(this);//获取键盘事件

    }

    //    初始化方法
    public void init() {
        length = 3;
//        脑袋的坐标
        snakeX[0] = 100;
        snakeY[0] = 100;
//第一个身体坐标
        snakeX[1] = 75;
        snakeY[1] = 100;
//        第二个身体坐标
        snakeX[2] = 50;
        snakeY[2] = 100;
//        方向
        fx = "R";
    }

    //绘制面板
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g); //清屏
        setBackground(Color.WHITE);

//        绘制静态面板
        Data.header.paintIcon(this, g, 25, 11);//头部广告栏
//        画矩形
        g.fillRect(25, 75, 850, 600); //默认的游戏界面

//        把小蛇画上去
        if (fx.equals("R")) {
            Data.right.paintIcon(this, g, snakeX[0], snakeY[0]);//蛇头初始化向右,通过方向判断
        } else if (fx.equals("L")) {
            Data.left.paintIcon(this, g, snakeX[0], snakeY[0]);//蛇头初始化向右,通过方向判断
        } else if (fx.equals("U")) {
            Data.up.paintIcon(this, g, snakeX[0], snakeY[0]);//蛇头初始化向右,通过方向判断
        } else if (fx.equals("D")) {
            Data.down.paintIcon(this, g, snakeX[0], snakeY[0]);//蛇头初始化向右,通过方向判断
        }

        for (int i = 1; i < length; i++) {
            Data.body.paintIcon(this, g, snakeX[i], snakeY[i]);//第一个身体坐标
        }

//        Data.body.paintIcon(this, g, snakeX[2], snakeY[2]);//第二个身体坐标

//        游戏状态
        if (isStart == false) {
//            游戏没有开始的时候 ，画一行文字
//            颜色
            g.setColor(Color.WHITE);
//            字体
            g.setFont(new Font("微软雅黑", Font.BOLD, 40));
//            画字
            g.drawString("按下空格开始游戏", 300, 300);

        }

    }

    //键盘监听器；
    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println("keyPressed");
//获得键盘按键是那一个
        int keyCode = e.getKeyCode();
        System.out.println("keyCode" + keyCode);
        //监听空格
        if (keyCode == KeyEvent.VK_SPACE) {
//           取反
            isStart =!isStart;
//            重新绘
            repaint();
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {
        System.out.println("keyTyped");

    }

    @Override
    public void keyReleased(KeyEvent e) {
        System.out.println("keyReleased");

    }
}



