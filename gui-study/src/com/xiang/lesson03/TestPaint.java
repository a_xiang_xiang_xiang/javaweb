package com.xiang.lesson03;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

//画笔
public class TestPaint {
    public static void main(String[] args) {
        new MyPaint().LoadFrame();
    }
}

class MyPaint extends Frame {
    public void LoadFrame() {
        setVisible(true);
        setBounds(200, 200, 600, 500);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

    //画笔
    @Override
    public void paint(Graphics g) {
//       颜色
        g.setColor(Color.RED);
//        画画
        g.drawOval(100, 100, 100, 100);
        g.setColor(Color.PINK);
        g.fillOval(220, 100, 100, 100); //实心的圆

        g.setColor(Color.GREEN);
        g.fillRect(110, 220, 200, 200);
    }
}
