<%--
  Created by IntelliJ IDEA.
  User: 17790405750
  Date: 2021/4/27
  Time: 8:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@include file="hd.jsp"%>--%>
<%--<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<%--<head>--%>
<%--    <title>学生信息管理系统</title>--%>
<%--    --%>
<%--</head>--%>

<head>
    <title></title>
    <!-- 新Bootstrap 核心CSS文件-->
    <link href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- iquery文件，务必在bootstarp.min.js之前引入-->
    <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>
    <!--最新的Bootstarp核心JavaScript 文件-->
<%--    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>
<%--    <link href="/Web8/bootstrap-4.6.0-dist/css/signin.css"rel="stylesheet">--%>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
    <a class="navbar-brand" href="#">学生信息管理系统</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">用户管理 <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">课程管理</a>
            </li>
<%--            <li class="nav-item">--%>
<%--                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">成绩管理</a>--%>
<%--            </li>--%>
            <li class="nav-item">
                <a class="nav-link" href="#">成绩管理</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">学生管理</a>
            </li>
        </ul>
        <p class="nav-item nav-link" style="color:#ffffff;align:right">欢迎你 ${uname}</p>
        <form class="form-inline mt-2 mt-md-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>
<main role="main">
    <div class="jumbotron" style="width:100%">
        <h1>用户管理</h1>
        <p class="lead">该模块可以完成对用户的操作，如添加、修改、删除等各种操作。</p>
        <a class="btn btn-lg btn-primary" href="/docs/components/navbar/" role="button">添加用户 &raquo;</a>
    </div>
</main >
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">序号</th>
        <th scope="col">用户名</th>
        <th scope="col">密码</th>
        <th scope="col">操作</th>
    </tr>
    </thead>
    <tbody>
   //循环
   <c:forEach var="u" items="${lis}" varStatus="id">
       <tr>
           <th scope="row">${id.index+1}</th>
           <td>${u.uname}</td>
           <td>${u.upsw}</td>
           <td>删除</td>
       </tr>

   </c:forEach>

    </tbody>
</table>
</body>
</html>
