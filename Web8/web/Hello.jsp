<%--
  Created by IntelliJ IDEA.
  User: 17790405750
  Date: 2021/4/1
  Time: 8:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" import="java.util.Date" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
你好，今天是：
<%
    Date today=new Date();
%>
<%=today.getDate()%>号，星期<%=today.getDay()%>
<hr>

<%! int i = 0;%>
<%
    //java注释
    i++;
%>
<%--服务端的注释--%>
<p>你是第<%=i%>个访问本站的用户！</p>
</body>
</html>
