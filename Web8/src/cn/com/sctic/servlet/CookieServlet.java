package cn.com.sctic.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "CookieServlet",urlPatterns = "/cookie")
public class CookieServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    //设置中文格式输出的doget
        response.setContentType("text/html;charset=utf-8");
        //输出对象
        PrintWriter out=response.getWriter();
        //创建Cookie
        Cookie c1=new Cookie("uname","admin");

        c1.setMaxAge(100);
        c1.setPath("d:/");
        response.addCookie(c1);
        Cookie[] cookies=request.getCookies();

        for(int i= 0;i<cookies.length;i++){
            if(cookies[i].getName().equals("uname")){
               out.println(cookies[i].getValue());
            }
        }



//        out.println(c1.getName());
//        out.println(c1.getValue());
//        out.println(c1.getMaxAge());
//        out.println(c1.getPath());
    }
}
