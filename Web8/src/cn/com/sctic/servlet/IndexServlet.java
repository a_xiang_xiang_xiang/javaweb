package cn.com.sctic.servlet;

import dao.UserDao;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "IndexServlet",urlPatterns = "/Index")
public class IndexServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession s=request.getSession();
        String userName=s.getAttribute("uname").toString();
        request.setAttribute("userName",userName);
        //查询所有用户
        UserDao dao =new UserDao();
        List<User> lis=dao.findAll();
        //将lis集合绑定到welcome.jsp页面
        request.setAttribute("lis",lis);

        request.getRequestDispatcher("welcome.jsp").forward(request,response);

    }
}
