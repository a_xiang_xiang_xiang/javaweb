package cn.com.sctic.servlet;

import cn.com.scitc.conn.SQLHpler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

@WebServlet(name = "MySqlServlet",urlPatterns = "/mysql")
public class MysqlServlet extends HttpServlet {

//    String driver="com.mysql.cj.jdbc.Driver";
//    String url="jdbc:mysql://localhost:3306/mytest?serverTimezone=GMT%2B8";
//    String user="root";
//    String password="123456";
    //定义连接对象
    Connection conn=null;



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("welcome.jsp").forward(request,response);

        SQLHpler s=new SQLHpler();
        conn=s.getConn();
        try{
        //1.加载驱动程序
//        try {
//            Class.forName(driver);
//            //2.创建数据库链接
//            conn=DriverManager.getConnection(url,user,password);
//            System.out.println("连接成功");
            //获取文本框中输入的用户名和密码
           String uname= request.getParameter("userName");
           String upawd= request.getParameter("passWord");
           //vSystem.out.println(uname+","+upawd);
           //sql语句
            String sql="select * from user where uname='"+ uname+"' and upawd='"+upawd+"'";
            //执行sql语句
            Statement st=conn.createStatement();
            ResultSet rs= st.executeQuery(sql);

            HttpSession session =request.getSession();
            
            if(rs.next()){
                session.removeAttribute("msg");
                session.setAttribute("uname",uname);
                response.sendRedirect("/Web8/Index");

            }else{

                session.setAttribute("msg","用户名或密码不正确!");

                response.sendRedirect("header.jsp");
            }
        } catch (Exception e) {
            e.printStackTrace();
           // System.out.println("连接失败");
        }
    }
}
