package cn.com.scitc.conn;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class SQLHpler {
    //定义变量
    String driver,url,user,password;
    //连接对象
    Connection conn=null;
    //连接方法
    public Connection getConn(){
        Properties p=new Properties();
        InputStream is=SQLHpler.class.getClassLoader().getResourceAsStream("conf.properties");
        try {
            p.load(is);
            //赋值
            driver=p.getProperty("driver");
            url=p.getProperty("url");
            user=p.getProperty("user");
            password=p.getProperty("password");
            //1.加载驱动程序
            Class.forName(driver);
            //2.建立连接对象
            conn= DriverManager.getConnection(url,user,password);
            System.out.println("成功");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("失败");
        }
        return conn;
    }
}
