package dao;

import cn.com.scitc.conn.SQLHpler;
import model.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
    //完成对myuser表中的数据的增、删、改、查询
    Connection c=null;
    //构造方法
    public UserDao(){
        SQLHpler s=new SQLHpler();
        c=s.getConn();
    }
    //查询所有用户
    public List<User> findAll() {
        //定义集合对象
        List<User> lis = new ArrayList<>();
        //sql命令
        String sql = "select * from user";
        //
        try {
            Statement st = c.createStatement();
            //执行sql
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                User u=new User();
                u.setUname(rs.getNString(1));
                u.setUpawd(rs.getNString(2));
                lis.add(u);

            }
            //关闭对象
            rs.close();
            st.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lis;
    }
    public static void main(String[] args) {
        UserDao dao=new UserDao();
        List<User> users=dao.findAll();
        for (User u :  users  ){
            System.out.println(u.getUname()+"\t"+u.getUpawd());
        }
    }
}
