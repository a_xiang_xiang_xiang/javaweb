package model;

public class User {
//定义成员变量
    String uname,upawd;

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUpawd() {
        return upawd;
    }

    public void setUpawd(String upawd) {
        this.upawd = upawd;
    }
}
