<%--
  Created by IntelliJ IDEA.
  User: 17790405750
  Date: 2021/4/6
  Time: 8:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    response.setHeader("Cache-Control","no-store");
    response.setDateHeader("Expires",0);
    response.setHeader("Pragma","no-cache");
%>
<html>
<head>
    <title></title>
    <!-- 新Bootstrap 核心CSS文件-->
    <link href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- iquery文件，务必在bootstarp.min.js之前引入-->
    <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>
    <!--最新的Bootstarp核心JavaScript 文件-->
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="/Web8/bootstrap-4.6.0-dist/css/signin.css"rel="stylesheet">
</head>



<body class="text-center">

<form class="form-signin"method="get" action="/Web8/mysql">
    <img class="mb-4" src="/Web8/images/bootstrap-solid.svg" alt="" width="72" height="72">
    <h1 class="h3 mb-3 font-weight-normal">欢迎登录本系统</h1>
    <label for="inputEmail" class="sr-only">用户名</label>
    <input type="text" name="userName" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" name="passWord" id="inputPassword" class="form-control" placeholder="Password" required>
    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" value="remember-me"> 记住密码
        </label>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">登录</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2017-2021</p>
</form>

</body>
    <script>
        var a='${msg}';
        if(a!=''){
            alert(a);
            a='';
        }

    </script>

</html>
