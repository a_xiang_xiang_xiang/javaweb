<%--
  Created by IntelliJ IDEA.
  User: 17790405750
  Date: 2021/4/7
  Time: 15:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>bootstrap</title>
    <link href="/Web8/bootstrap-4.6.0-dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="/Web8/jquery-3.6.0.min.js"></script>
    <script src="/Web8/bootstrap-4.6.0-dist/js/bootstrap.min.js"></script>
    <link href="/Web8/bootstrap-4.6.0-dist/css/bootstrap.sig.css" rel="stylesheet">
</head>
<body class="text-center">

<form class="form-signin">
    <img class="mb-4" src="/Web8/img/bootstrap-solid.svg" alt="" width="72" height="72">
    <h1 class="h3 mb-3 font-weight-normal">欢迎登录</h1>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" value="remember-me"> 记住密码
        </label>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">登录</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2017-2021</p>
</form>

</body>
</html>
