<%--
  Created by IntelliJ IDEA.
  User: 17790405750
  Date: 2021/4/8
  Time: 9:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<c:set var="salary" value="800"></c:set>
<%--你的工资是：<c:out value="${salary}"></c:out>--%>
<%--<c:remove var="salary"></c:remove>--%>
<%--删除后的工资是<c:out value="${salary}"/>--%>
<%--<c:if test="${salary>2000}">--%>
<%--    你的工资是：<c:out value="${salary}"></c:out>--%>
<%--</c:if>--%>
<c:choose>
    <c:when test="${salary>10000}">
        <c:out value="优秀,继续加油"></c:out>
    </c:when>
    <c:when test="${salary>5000}">
        <c:out value="还行，还要努力"></c:out>
    </c:when>
    <c:when test="${salary>2000}">
        <c:out value="勉强够用，多多搬砖"></c:out>
    </c:when>
    <c:otherwise>
        <c:out value="快要饿死了,快快加油"></c:out>
    </c:otherwise>
</c:choose>
</body>
</html>
